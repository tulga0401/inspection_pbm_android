package mn.fullstream.premium_hse;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public abstract class BaseActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Window window = getWindow();
		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			window.setStatusBarColor(ContextCompat.getColor(BaseActivity.this,R.color.colorPrimaryWhite));
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	protected void setToolbar(String title) {
		setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setTitle("");

		TextView titleTextView = findViewById(R.id.title);
		titleTextView.setText(title);


		((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main)).setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.enter, R.anim.leave);
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.enter, R.anim.leave);
	}

	@Override
	protected void onResume() {
		super.onResume();
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
	}

}
