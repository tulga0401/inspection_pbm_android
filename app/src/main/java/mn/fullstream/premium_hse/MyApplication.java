package mn.fullstream.premium_hse;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.Log;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import lv.chi.photopicker.ChiliPhotoPicker;
import mn.fullstream.premium_hse.loaders.GlideImageLoader;
import mn.fullstream.premium_hse.model.Config;

/**
 * Created by Tulga on 10/30/17.
 */

public class MyApplication extends Application {
    public static RealmConfiguration realmConfig;
    public static Realm realm;
    public static boolean inMessenger = true;
    private static Context context;
    private static Locale mLocale;
    private AlarmManager alarmMgr; //TO CALL OUT THE CLASS OF THE ALARM SERVICE
    private PendingIntent alarmIntent; // FOR TARGET FUNCTION TO PERFORM WITH BROADCASTRECEIVER
    private BroadcastReceiver br;
    public static boolean isChangedLang = false;

    public static Activity activeActivity = null;

    public static void setLocale(String lang, Context context) {
        try {
            SharedPreferences.Editor editor;

            SharedPreferences sharedPreferences;
            sharedPreferences = context.getSharedPreferences("ContactList", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();

            editor.putString("lang", lang);
            editor.apply();

            Configuration config = context.getResources().getConfiguration();

            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocales(new LocaleList(locale));
            } else {
                config.locale = locale;
            }
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkAndRecreateActivity(Activity activity) {
        SharedPreferences sharedPreferences;
        sharedPreferences = activity.getSharedPreferences("ContactList", Context.MODE_PRIVATE);
        String sharedLang = sharedPreferences.getString("lang", "");
        Resources res = activity.getResources();
        Configuration conf = res.getConfiguration();

        if (!conf.locale.getLanguage().equals(sharedLang)) {
            activity.finish();
            activity.startActivity(new Intent(activity, activity.getClass()));
        }
    }

    public static String checkLocale(Context context) {

        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences("ContactList", Context.MODE_PRIVATE);

        Resources res = context.getResources();
        Configuration conf = res.getConfiguration();

        String sharedLang = sharedPreferences.getString("lang", "");

        Log.e("locale", conf.locale.getLanguage());

        if (!conf.locale.getLanguage().equals(sharedLang)) {

            Configuration config = context.getResources().getConfiguration();

            Locale locale = new Locale(sharedLang);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocales(new LocaleList(locale));
            } else {
                config.locale = locale;
            }
            context.getResources()
                    .updateConfiguration(config, context.getResources().getDisplayMetrics());

            Log.e("changed locale", conf.locale.getLanguage());
        }

        return sharedLang;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {

            context = getApplicationContext();
//            MultiDex.install(this);


            FirebaseApp.initializeApp(context);


            Realm.init(MyApplication.this);
            if (realmConfig == null) {
                realmConfig = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
            }

            Realm.setDefaultConfiguration(realmConfig);
            realm = Realm.getInstance(realmConfig);


            ChiliPhotoPicker.INSTANCE.init(new GlideImageLoader(), "mn.iotech.inspection.fileprovider");


            Realm realm = Realm.getInstance(MyApplication.realmConfig);
            Config config = realm.where(Config.class).findFirst();


            if (config == null) {
                realm.beginTransaction();
                config = realm.createObject(Config.class);
                config.setSavedFingerPrint(false);
                config.setFingerPrintCardNumber("");
                config.setFingerPrintPinCode("");
                realm.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
