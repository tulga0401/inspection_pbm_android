package mn.fullstream.premium_hse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.dialogs.LoadingDialogFragment;
import mn.fullstream.premium_hse.model.HseImage;
import mn.fullstream.premium_hse.model.Issue;
import mn.fullstream.premium_hse.util.PhotoCaptureHelper;
import mn.fullstream.premium_hse.util.RequestController;

public class OrderResultActivity extends AppCompatActivity {


    RecyclerView hseImageRecyclerView, issuesRecyclerView;

    ArrayList<HseImage> hseImages = new ArrayList<>();

    ArrayList<Issue> issues = new ArrayList<>();
    ArrayList<Issue> filteredIssues = new ArrayList<>();

    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerIssueViewAdapter recyclerIssueViewAdapter;

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    FrameLayout selectHseButton;

    FrameLayout backButton,cancelButton;

    CheckBox addAdviceCheck;
    EditText addAdviceEditText;

    TextView endDateDescText;

    TextView issueTitleText,descNotFoundText,issuesNotFoundText,imagesNotFoundText,adviceNotFoundText,endDateEmptyText;

    ArrayList<File> files = new ArrayList<>();

    String orderId = "";

    TextView userNameText,positionText,phonenumberText,descText,issuesText,adviceText,endText;

    NestedScrollView container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_result);

        hseImageRecyclerView = findViewById(R.id.hseImageRecyclerView);
        issuesRecyclerView = findViewById(R.id.issuesRecyclerView);
        selectHseButton = findViewById(R.id.selectHseButton);
        addAdviceCheck = findViewById(R.id.addAdviceCheck);
        addAdviceEditText = findViewById(R.id.addAdviceEditText);
        cancelButton = findViewById(R.id.cancelButton);
        issueTitleText = findViewById(R.id.issueTitleText);
        adviceNotFoundText = findViewById(R.id.adviceNotFoundText);
        adviceText = findViewById(R.id.adviceText);
        endDateDescText = findViewById(R.id.endDateDescText);
        endDateEmptyText = findViewById(R.id.endDateEmptyText);
        endText = findViewById(R.id.endText);

        userNameText = findViewById(R.id.userNameText);
        positionText = findViewById(R.id.positionText);
        phonenumberText = findViewById(R.id.phonenumberText);
        descText = findViewById(R.id.descText);
        issuesText = findViewById(R.id.issuesText);
        descNotFoundText = findViewById(R.id.descNotFoundText);
        issuesNotFoundText = findViewById(R.id.issuesNotFoundText);
        imagesNotFoundText = findViewById(R.id.imagesNotFoundText);
        container = findViewById(R.id.container);





        backButton = findViewById(R.id.backButton);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderResultActivity.this.finish();
            }
        });

        addAdviceCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    addAdviceEditText.setVisibility(View.VISIBLE);
                } else {
                    addAdviceEditText.setVisibility(View.GONE);
                }
            }
        });


        hseImages.add(new HseImage(null, true));
        hseImages.add(new HseImage(null, true));
        hseImages.add(new HseImage(null, true));
        hseImages.add(new HseImage(null, true));
        hseImages.add(new HseImage(null, true));
        hseImages.add(new HseImage(null, true));

        orderId = getIntent().getStringExtra(Constants.INTENT_ORDER_ID);
        String advice = getIntent().getStringExtra(Constants.INTENT_ORDER_ADVICE);
        String approveAt = getIntent().getStringExtra(Constants.INTENT_ORDER_APPROVEDAT);
        String canceledAt = getIntent().getStringExtra(Constants.INTENT_ORDER_CANCALEDAT);


        if (!approveAt.equals("null")){

            endDateDescText.setText("БАТАЛСАН ОГНОО");

            try {
                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


                Date newDate = spf.parse(approveAt);
                spf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                approveAt = spf.format(newDate);


                endText.setText(approveAt);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (!canceledAt.equals("null")){

            endDateDescText.setText("ЦУЦАЛСАН ОГНОО");

            try {
                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


                Date newDate = spf.parse(canceledAt);
                spf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                canceledAt = spf.format(newDate);


                endText.setText(canceledAt);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            endText.setVisibility(View.GONE);
            endDateEmptyText.setVisibility(View.VISIBLE);
        }

        if (advice.isEmpty()){
            adviceNotFoundText.setVisibility(View.VISIBLE);
            adviceText.setVisibility(View.GONE);
        }else {
            adviceText.setText(advice);
        }




        new LoadIssuesTask("Үл тохирол татаж байна...").execute();

    }


    class LoadIssuesTask extends AsyncTask<String, String, String> {

        LoadingDialogFragment loadingDialogFragment;


        String loadingTitle = "";

        boolean isSuccess = false;
        String responseMessage = "";


        String issue="";
        String userName="";
        String position="";
        String phone="";
        String desc="";

        ArrayList<String> images = new ArrayList<>();


        public LoadIssuesTask(String loadingTitle) {
            this.loadingTitle = loadingTitle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            loadingDialogFragment = new LoadingDialogFragment(loadingTitle);
            loadingDialogFragment.show(getSupportFragmentManager(), "");

        }

        @Override
        protected String doInBackground(String... strings) {


            RequestController requestController = new RequestController();

            String response = requestController.getHttpRequest(Constants.ORDERS_API_HSE_ISSUE.replace("{id}",orderId));

            Log.e("response", response);


            try {
                JSONObject resultJson = new JSONObject(response);



                if (resultJson != null) {
                    try {
                        String status = "";
                        status = resultJson.getString("status");

                        JSONObject resultJsonObject = resultJson.getJSONObject("result");

                        if (status.equals("success")) {
                            isSuccess = true;


                            JSONArray jsonArray = resultJsonObject.getJSONArray("issueList");

                            if (jsonArray.length()>0){


                                JSONObject issueJsonObject = jsonArray.getJSONObject(0);

                                desc = issueJsonObject.getString("desc");
                                issue = issueJsonObject.getJSONObject("issue").getString("name");
                                userName = issueJsonObject.getJSONObject("rejectedUser").getString("lastName")+" "+issueJsonObject.getJSONObject("rejectedUser").getString("firstName");
                                position = issueJsonObject.getJSONObject("rejectedUser").getString("position");
                                phone = issueJsonObject.getJSONObject("rejectedUser").getString("phoneNumber");

                            }


                            JSONArray imagesJsonArray = resultJsonObject.getJSONArray("images");
                            if (imagesJsonArray.length()>0){


                                for (int i=0;i<imagesJsonArray.length();i++) {

                                    JSONObject pathJsonObject = imagesJsonArray.getJSONObject(i);

                                    images.add(pathJsonObject.getString("path"));

                                }
                            }

                        }


                        String message = resultJson.getString("message");

                        responseMessage = message;

                    } catch (Exception e) {
                        e.printStackTrace();

                        isSuccess = false;

                        responseMessage = e.getMessage();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            loadingDialogFragment.dismiss();


            if (isSuccess) {


                userNameText.setText(userName);
                positionText.setText(position);
                phonenumberText.setText(phone);
                descText.setText(desc);
                issuesText.setText(issue);


                if (issue.isEmpty()){

                    issuesNotFoundText.setVisibility(View.VISIBLE);
                }


                if (desc.isEmpty()){
                    descNotFoundText.setVisibility(View.VISIBLE);
                }

                if(images.size()==0){
                    imagesNotFoundText.setVisibility(View.VISIBLE);
                }


                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderResultActivity.this, LinearLayoutManager.HORIZONTAL, false);
                hseImageRecyclerView.setLayoutManager(layoutManager);

                recyclerViewAdapter = new RecyclerViewAdapter(OrderResultActivity.this, images);

                hseImageRecyclerView.setAdapter(recyclerViewAdapter);

                container.setVisibility(View.VISIBLE);


            } else {

            }
        }
    }

    private static final int PERMISSION_REQUEST_CODE = 200;


    private void pickFromGallery(int position) {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, position);
    }

    private String cameraFilePath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //This is the directory in which the file will be created. This is the default location of Camera photos
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for using again
        cameraFilePath = "file://" + image.getAbsolutePath();
        return image;
    }

    PhotoCaptureHelper photoCaptureHelper;

    private void captureFromCamera(int position) {
        try {

            try {


                photoCaptureHelper = new PhotoCaptureHelper(this);
                photoCaptureHelper.dispatchTakePictureIntent(position + 1000);

            } catch (Exception e) {
                e.printStackTrace();

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        private Context context;
        private ArrayList<String> hseImages;

        public RecyclerViewAdapter(Context context, ArrayList<String> hseImages) {
            this.hseImages = hseImages;
            this.context = context;
        }

        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_image, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

            final String data = hseImages.get(position);

            if (data != null) {
                try {


                    holder.closeButton.setVisibility(View.GONE);


                    Log.e("data",data);

                    Picasso.get().load(data).into(holder.hseImage);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {

            return hseImages.size();
        }


    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        FrameLayout addImageButton, closeButton;
        ImageView hseImage;


        public RecyclerViewHolders(View itemView) {
            super(itemView);

            closeButton = itemView.findViewById(R.id.closeButton);
            addImageButton = itemView.findViewById(R.id.addImageButton);
            hseImage = itemView.findViewById(R.id.hseImage);


        }

        @Override
        public void onClick(View view) {

            Log.e("click", "click");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {

                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    class RecyclerIssueViewAdapter extends RecyclerView.Adapter<RecyclerIssueViewHolders> {

        private Context context;

        public RecyclerIssueViewAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public RecyclerIssueViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_issue, parent, false);
            RecyclerIssueViewHolders rcv = new RecyclerIssueViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerIssueViewHolders holder, @SuppressLint("RecyclerView") final int position) {

            Issue data = filteredIssues.get(position);


            if (data != null) {


                holder.title.setText(data.getName());
                holder.checkCircle.setVisibility(View.GONE);


            }


        }

        @Override
        public int getItemCount() {

            return filteredIssues.size();
        }


    }

    public class RecyclerIssueViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, editText, descText;
        ImageView circleImageView;

        FrameLayout checkButton, checkCircle;

        FrameLayout addDescButton;

        public RecyclerIssueViewHolders(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            circleImageView = itemView.findViewById(R.id.circleImageView);
            checkButton = itemView.findViewById(R.id.checkButton);
            editText = itemView.findViewById(R.id.editText);
            descText = itemView.findViewById(R.id.descText);


        }

        @Override
        public void onClick(View view) {

            Log.e("click", "click");
        }
    }
}
