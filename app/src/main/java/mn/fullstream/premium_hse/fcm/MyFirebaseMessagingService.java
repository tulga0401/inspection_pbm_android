package mn.fullstream.premium_hse.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import org.json.JSONObject;

import java.util.Random;

import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MainActivity;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.model.Config;
import mn.fullstream.premium_hse.model.NotificationPush;
import mn.fullstream.premium_hse.notification.NotificationDialogActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        try {
            Log.e("token", s);
            Realm realm = Realm.getInstance(MyApplication.realmConfig);
            Config config = realm.where(Config.class).findFirst();
            realm.beginTransaction();
            config.setFcmToken(s);
            realm.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("remoteMessage", remoteMessage.getData().toString());
//        Toast.makeText(getApplicationContext(), remoteMessage.getData().toString(), Toast.LENGTH_SHORT).show();

        String title = remoteMessage.getData().get("contentTitle");
        String message = remoteMessage.getData().get("message");
        String actionCode = remoteMessage.getData().get("action");
        String actionKey = remoteMessage.getData().get("actionKey");
        String data = remoteMessage.getData().get("data");

//        Log.e("message", message);

        if (title == null) {
            title = "";
        }

        if (message == null) {
            message = "";
        }

        if (actionCode == null) {
            actionCode = "";
        }

        if (actionKey == null) {
            actionKey = "";
        }

        if (data == null) {
            data = "";
        }


        sendNotification(title, message, actionCode, actionKey, data);


//        sendNotification(notificationPush);

    }
    // [END receive_message]

    private void sendNotification(String title, String message, String actionCode, String actionKey, String data) {
        try {
            Intent intent;
            PendingIntent pendingIntent;

            Log.e("actionKey", actionKey + " k");
            Log.e("message", message + " m");
            Log.e("data", data + " d");

            intent = new Intent(getApplicationContext(), MainActivity.class);

            String CHANNEL_ID = "my_channel_01";// The id of the channel.

            pendingIntent =
                    PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder;
            int smallIcon;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                smallIcon = R.mipmap.ic_app_icon_notify;
            } else {
                smallIcon = R.mipmap.ic_launcher;
            }
            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();

            notificationBuilder = new NotificationCompat.Builder(this).setSmallIcon(smallIcon)
                    .setLargeIcon(Bitmap.createBitmap(
                            BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_foreground)))
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(CHANNEL_ID)
                    .setStyle(bigText)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


                NotificationChannel channel = new NotificationChannel("my_channel_01",
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }

            Random rand = new Random();

            int id = rand.nextInt(50) + 1;
            notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
//     */
//    private void sendNotification(NotificationPush notificationPush) {
//        try {
//            Intent intent = new Intent(getApplicationContext(), NotificationDialogActivity.class);
//            intent.putExtra(Constants.INTENT_PUSH_NOTIFICATION, notificationPush);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
