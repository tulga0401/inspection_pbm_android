package mn.fullstream.premium_hse.loaders;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import lv.chi.photopicker.loader.ImageLoader;

public class GlideImageLoader implements ImageLoader {


	@Override
	public void loadImage(@NotNull Context context, @NotNull ImageView imageView, @NotNull Uri uri) {

		Glide.with(context).load(uri).asBitmap().centerCrop().into(imageView);

	}
}
