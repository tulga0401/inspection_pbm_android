package mn.fullstream.premium_hse.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.model.NotificationPush;

public class NotificationDialogActivity extends Activity {

	ImageView notificationImage;
	FrameLayout closeButton,okButton;
	TextView title,desc;


	NotificationPush notification;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_notification_dialog);

		notification = getIntent().getParcelableExtra(Constants.INTENT_PUSH_NOTIFICATION);

		notificationImage = findViewById(R.id.notificationImage);
		closeButton = findViewById(R.id.closeButton);
		title = findViewById(R.id.title);
		desc = findViewById(R.id.desc);
		okButton = findViewById(R.id.okButton);


		title.setText(notification.getTitle());
		desc.setText(notification.getDesc());


		try {
			Picasso.get().load(notification.getImageUrl()).into(notificationImage);
		} catch (Exception e){e.printStackTrace();}


		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					NotificationDialogActivity.this.finish();

					Intent intent = new Intent();

					intent.setClassName(NotificationDialogActivity.this, notification.getAndroidPageId());

					intent.putExtra(Constants.INTENT_PARAM_INSPECT_ID,notification.getInspectId());
					intent.putExtra(Constants.INTENT_PARAM_INSPECT_NAME,notification.getInspectName());
					intent.putExtra(Constants.INTENT_PARAM_INSPECT_CODE,notification.getInspectCode());

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NotificationDialogActivity.this.finish();
			}
		});

	}
}
