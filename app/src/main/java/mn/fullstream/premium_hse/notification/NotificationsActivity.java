package mn.fullstream.premium_hse.notification;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.LinearLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.BaseActivity;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.CustomResultDialog;
import mn.fullstream.premium_hse.model.Attribute;
import mn.fullstream.premium_hse.model.Notification;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.EndlessRecyclerViewLinearScrollListener;
import mn.fullstream.premium_hse.util.RequestController;

public class NotificationsActivity extends BaseActivity {

	RecyclerView notificationRecyclerView;

	LottieAnimationView lottieAnimationView;

	ArrayList<Notification> notifications = new ArrayList<>();
	EndlessRecyclerViewLinearScrollListener scrollListener;


	RecyclerViewAdapter recyclerViewAdapter;


	int pageCount=1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);

		setToolbar("Мэдэгдэл");

		notificationRecyclerView = findViewById(R.id.notificationRecyclerView);
		lottieAnimationView = findViewById(R.id.lottieAnimationView);



		notificationRecyclerView.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this));
		recyclerViewAdapter = new RecyclerViewAdapter(NotificationsActivity.this);
		notificationRecyclerView.setAdapter(recyclerViewAdapter);


		new LoadNotificationsTask().execute();


		LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationsActivity.this);
		notificationRecyclerView.setLayoutManager(layoutManager);
		scrollListener = new EndlessRecyclerViewLinearScrollListener(layoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {

				pageCount = page + 1;

				new LoadNotificationsTask().execute();

			}


		};

		notificationRecyclerView.addOnScrollListener(scrollListener);


	}

	class LoadNotificationsTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		ArrayList<Attribute> attributes = new ArrayList<>();

		public LoadNotificationsTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if (pageCount==1) {

				notificationRecyclerView.setVisibility(View.GONE);
				lottieAnimationView.setVisibility(View.VISIBLE);
			}

		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.API_NOTIFICATIONS.replace("{userId}",user.getId()).replace("{page}",pageCount+""));

				Log.e("response",response);

				try {

					JSONObject jsonObject = new JSONObject(response);

					JSONArray notificationsJsonArray = jsonObject.getJSONArray("notifications");

					for (int i=0;i<notificationsJsonArray.length();i++){
						Notification notification = new Notification();
						JSONObject notificationJsonObject = notificationsJsonArray.getJSONObject(i);

						notification.setTitle(notificationJsonObject.has("title")?notificationJsonObject.getString("title"):"");
						notification.setDesc(notificationJsonObject.has("body")?notificationJsonObject.getString("body"):"");

						if (notificationJsonObject.has("createdAt")) {
							notification.setDate(notificationJsonObject.getJSONObject("createdAt").getString("date"));
						}

						notifications.add(notification);
					}



				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {


				if (pageCount==1) {

					notificationRecyclerView.setVisibility(View.VISIBLE);
					lottieAnimationView.setVisibility(View.GONE);
				}

				recyclerViewAdapter.notifyDataSetChanged();



			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;



		public RecyclerViewAdapter(Context context) {
			this.context = context;
		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_notification, parent, false);



			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {

			final Notification data = notifications.get(position);

			if (data != null) {

				holder.dateText.setText(data.getDate().replace(".000000",""));
				holder.descText.setText(data.getDesc());
				holder.titleText.setText(data.getTitle());


				holder.selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						CustomResultDialog customResultDialog = new CustomResultDialog(CustomResultDialog.DIALOG_SUCCESS_,data.getTitle(),data.getDesc());

						customResultDialog.show(getSupportFragmentManager(),"");
					}
				});
			}
		}

		@Override
		public int getItemCount() {

			return notifications.size();
		}


	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {


		TextView titleText,descText,dateText;

		LinearLayout selectButton;


		public RecyclerViewHolders(View itemView) {
			super(itemView);


			titleText = itemView.findViewById(R.id.titleText);
			descText = itemView.findViewById(R.id.descText);
			dateText = itemView.findViewById(R.id.dateText);
			selectButton = itemView.findViewById(R.id.selectButton);
		}


		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}
}
