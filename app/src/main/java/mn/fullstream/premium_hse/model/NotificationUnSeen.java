package mn.fullstream.premium_hse.model;

import io.realm.RealmObject;

public class NotificationUnSeen extends RealmObject {

	String unseenCount;


	public String getUnseenCount() {
		return unseenCount;
	}

	public void setUnseenCount(String unseenCount) {
		this.unseenCount = unseenCount;
	}
}
