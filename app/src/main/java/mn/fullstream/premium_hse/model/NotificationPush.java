package mn.fullstream.premium_hse.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationPush implements Parcelable {

	String attachment;
	String androidPageId;
	String imageUrl;
	String title;
	String desc;
	String pageArgument;

	String inspectId;
	String inspectName;
	String inspectCode;


	public String getInspectId() {
		return inspectId;
	}

	public void setInspectId(String inspectId) {
		this.inspectId = inspectId;
	}

	public String getInspectName() {
		return inspectName;
	}

	public void setInspectName(String inspectName) {
		this.inspectName = inspectName;
	}

	public String getInspectCode() {
		return inspectCode;
	}

	public void setInspectCode(String inspectCode) {
		this.inspectCode = inspectCode;
	}

	public String getPageArgument() {
		return pageArgument;
	}

	public void setPageArgument(String pageArgument) {
		this.pageArgument = pageArgument;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAndroidPageId() {
		return androidPageId;
	}

	public void setAndroidPageId(String androidPageId) {
		this.androidPageId = androidPageId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.attachment);
		dest.writeString(this.androidPageId);
		dest.writeString(this.imageUrl);
		dest.writeString(this.title);
		dest.writeString(this.desc);
		dest.writeString(this.pageArgument);
		dest.writeString(this.inspectId);
		dest.writeString(this.inspectName);
		dest.writeString(this.inspectCode);
	}

	public NotificationPush() {
	}

	protected NotificationPush(Parcel in) {
		this.attachment = in.readString();
		this.androidPageId = in.readString();
		this.imageUrl = in.readString();
		this.title = in.readString();
		this.desc = in.readString();
		this.pageArgument = in.readString();
		this.inspectId = in.readString();
		this.inspectName = in.readString();
		this.inspectCode = in.readString();
	}

	public static final Creator<NotificationPush> CREATOR = new Creator<NotificationPush>() {
		@Override
		public NotificationPush createFromParcel(Parcel source) {
			return new NotificationPush(source);
		}

		@Override
		public NotificationPush[] newArray(int size) {
			return new NotificationPush[size];
		}
	};
}
