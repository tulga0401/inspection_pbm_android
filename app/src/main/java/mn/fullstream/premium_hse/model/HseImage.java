package mn.fullstream.premium_hse.model;

import android.graphics.Bitmap;

import java.io.File;

public class HseImage {

    Bitmap photo;
    File photoFile;

    String fileName;
    boolean isAdd;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public HseImage(Bitmap photo, boolean isAdd) {
        this.photo = photo;
        this.isAdd = isAdd;
    }

    public File getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(File photoFile) {
        this.photoFile = photoFile;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }
}
