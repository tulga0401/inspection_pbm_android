package mn.fullstream.premium_hse.model;

import java.util.ArrayList;

public class Notification {
	String title;
	String desc;
	String date;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	public static ArrayList<Notification> getNotifications(){

		ArrayList<Notification> notifications = new ArrayList<>();


		Notification notification = new Notification();
		notification.setTitle("Дэлгүүрийн зѳвлѳх");
		notification.setDesc("Энэ Checklist-ийг бөглөнө үү?");
		notification.setDate("Jan 7, 2019");

		notifications.add(notification);

		Notification notification1 = new Notification();
		notification1.setTitle("QC-Tuyaa");
		notification1.setDesc("Таны хариуцсан салбар дээр шинэ Task хүлээгдэж байна.");
		notification1.setDate("Jan 7, 2019");

		notifications.add(notification1);

		Notification notification2 = new Notification();
		notification2.setTitle("IT");
		notification2.setDesc("Таны Task-ийг хэрэгжүүллээ");
		notification2.setDate("Jan 7, 2019");

		notifications.add(notification2);
		return notifications;

	}
}
