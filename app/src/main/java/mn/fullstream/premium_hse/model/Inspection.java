package mn.fullstream.premium_hse.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Inspection implements Parcelable {

    String id;
    String groupId;
    String name;
    String nonConflict;
    String code;
    String inspectedData;
    String inspectedDate;
    String createdAt;
    String desc;
    String orderCompanyName;
    String quantity;
    String site;
    String carRegNumber;
    String siteNumber;
    String productName;
    String plantName;
    String quantityMeter;
    String templateId;
    String status;
    boolean isGroup;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getQuantityMeter() {
        return quantityMeter;
    }

    public void setQuantityMeter(String quantityMeter) {
        this.quantityMeter = quantityMeter;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(String siteNumber) {
        this.siteNumber = siteNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOrderCompanyName() {
        return orderCompanyName;
    }

    public void setOrderCompanyName(String orderCompanyName) {
        this.orderCompanyName = orderCompanyName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCarRegNumber() {
        return carRegNumber;
    }

    public void setCarRegNumber(String carRegNumber) {
        this.carRegNumber = carRegNumber;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNonConflict() {
        return nonConflict;
    }

    public void setNonConflict(String nonConflict) {
        this.nonConflict = nonConflict;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInspectedData() {
        return inspectedData;
    }

    public void setInspectedData(String inspectedData) {
        this.inspectedData = inspectedData;
    }

    public String getInspectedDate() {
        return inspectedDate;
    }

    public void setInspectedDate(String inspectedDate) {
        this.inspectedDate = inspectedDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.nonConflict);
        dest.writeString(this.code);
        dest.writeString(this.inspectedData);
        dest.writeString(this.inspectedDate);
        dest.writeString(this.createdAt);
        dest.writeString(this.desc);
        dest.writeString(this.orderCompanyName);
        dest.writeString(this.quantity);
        dest.writeString(this.site);
        dest.writeString(this.carRegNumber);
        dest.writeString(this.siteNumber);
        dest.writeString(this.productName);
        dest.writeString(this.templateId);
        dest.writeString(this.plantName);
        dest.writeString(this.groupId);
        dest.writeString(this.quantityMeter);
        dest.writeString(this.status);
    }

    public Inspection() {
    }

    protected Inspection(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.nonConflict = in.readString();
        this.code = in.readString();
        this.inspectedData = in.readString();
        this.inspectedDate = in.readString();
        this.createdAt = in.readString();
        this.desc = in.readString();
        this.orderCompanyName = in.readString();
        this.quantity = in.readString();
        this.site = in.readString();
        this.carRegNumber = in.readString();
        this.siteNumber = in.readString();
        this.productName = in.readString();
        this.templateId = in.readString();
        this.plantName = in.readString();
        this.groupId = in.readString();
        this.quantityMeter = in.readString();
        this.status = in.readString();
    }

    public static final Creator<Inspection> CREATOR = new Creator<Inspection>() {
        @Override
        public Inspection createFromParcel(Parcel source) {
            return new Inspection(source);
        }

        @Override
        public Inspection[] newArray(int size) {
            return new Inspection[size];
        }
    };
}
