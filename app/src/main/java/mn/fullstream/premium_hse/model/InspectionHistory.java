package mn.fullstream.premium_hse.model;

import java.util.ArrayList;

public class InspectionHistory {

	String createdAt;

	ArrayList<Inspection> inspections;

	public ArrayList<Inspection> getInspections() {
		return inspections;
	}

	public void setInspections(ArrayList<Inspection> inspections) {
		this.inspections = inspections;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
