package mn.fullstream.premium_hse.model;

import java.util.ArrayList;

public class Attribute {


	public static String FORM_CHECKBOX = "checkbox";
	public static String FORM_IMAGE = "image";
	public static String FORM_TEXT = "text";
	public static String FORM_SELECT = "select";
	public static String FORM_EMAIL = "email";
	public static String FORM_TEXTAREA = "textarea";
	public static String FORM_CURRENT_NUMBER = "number";
	public static String FORM_MULTI_SELECT = "multi_select";
	public static String FORM_TIME_PICK = "time_pick";
	public static String FORM_CALENDAR = "calendar";

	String id;
	String name;
	String isRequired;
	String grade;
	String formType;
	ArrayList<Option> options;
	ArrayList<String> images;

	public ArrayList<String> getImages() {
		return images;
	}

	public void setImages(ArrayList<String> images) {
		this.images = images;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public ArrayList<Option> getOptions() {
		return options;
	}

	public void setOptions(ArrayList<Option> options) {
		this.options = options;
	}
}
