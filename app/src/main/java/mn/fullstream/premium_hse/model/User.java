package mn.fullstream.premium_hse.model;

import io.realm.RealmObject;

public class User extends RealmObject {

    String token;
    String id;
    String userName;
    String firstName;
    String lastName;
    String position;
    String phoneNumber;
    String company;
    String email;
    String templateCode;
    String isWorley;

	public String getIsWorley() {
		return isWorley;
	}

	public void setIsWorley(String isWorley) {
		this.isWorley = isWorley;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

	@Override
	public String toString() {
		return "User{" +
				"token='" + token + '\'' +
				", id='" + id + '\'' +
				", userName='" + userName + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", position='" + position + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", company='" + company + '\'' +
				", email='" + email + '\'' +
				", templateCode='" + templateCode + '\'' +
				", isWorley='" + isWorley + '\'' +
				'}';
	}
}
