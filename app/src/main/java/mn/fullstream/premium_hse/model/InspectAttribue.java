package mn.fullstream.premium_hse.model;

import java.util.ArrayList;

public class InspectAttribue {
	String attribueId;
	String value;
	String formType;
	ArrayList<String> values;

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public ArrayList<String> getValues() {
		return values;
	}

	public void setValues(ArrayList<String> values) {
		this.values = values;
	}

	public String getAttribueId() {
		return attribueId;
	}

	public void setAttribueId(String attribueId) {
		this.attribueId = attribueId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
