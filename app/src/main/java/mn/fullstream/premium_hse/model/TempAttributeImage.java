package mn.fullstream.premium_hse.model;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import mn.fullstream.premium_hse.main.task.InspectActivity;

public class TempAttributeImage {

	String attributeId;
	RecyclerView recyclerView;
	InspectActivity.RecyclerViewAdapter recyclerViewAdapter;
	ArrayList<String> images = new ArrayList<>();

	public ArrayList<String> getImages() {
		return images;
	}

	public void setImages(ArrayList<String> images) {
		this.images = images;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public RecyclerView getRecyclerView() {
		return recyclerView;
	}

	public void setRecyclerView(RecyclerView recyclerView) {
		this.recyclerView = recyclerView;
	}

	public InspectActivity.RecyclerViewAdapter getRecyclerViewAdapter() {
		return recyclerViewAdapter;
	}

	public void setRecyclerViewAdapter(InspectActivity.RecyclerViewAdapter recyclerViewAdapter) {
		this.recyclerViewAdapter = recyclerViewAdapter;
	}
}
