package mn.fullstream.premium_hse.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ActiveOrder implements Parcelable {

    String id;
    String orderName;
    String orderNumber;
    String castingLocation;
    String createdDate;
    String status;
    int statusId;

    int deliveredQuantity;
    int undeliverQuantity;
    String totalQuantity;


    String siteLat;
    String siteLon;



    String clientName;
    String nameWhoGives;
    String statusWhoGives;
    String contactName;
    String contactNumber;
    String projectName;
    String nameOfCastingLocation;
    String structureName;
    String floor;
    String siteEngineer;

    String siteId;
    String mixDesignId;

    String dateOfCasting;
    String startTime;
    String orderQualityOfConcrete;
    String pouringSpeech;
    String concreteClass;
    String morter;
    boolean fieldTestRequirment;
    String numberOfSpecimen;
    String sampleForm;
    String slump;
    String flow;
    String airContent;
    String locationOfTest;
    String daysOfStrengthTest;
    String castingMethod;
    String additionalInfo;
    String canceledReason;
    String hseAdvice;

    String canceledAt;
    String approvedAt;

    public String getCanceledAt() {
        return canceledAt;
    }

    public void setCanceledAt(String canceledAt) {
        this.canceledAt = canceledAt;
    }

    public String getApprovedAt() {
        return approvedAt;
    }

    public void setApprovedAt(String approvedAt) {
        this.approvedAt = approvedAt;
    }

    public String getHseAdvice() {
        return hseAdvice;
    }

    public void setHseAdvice(String hseAdvice) {
        this.hseAdvice = hseAdvice;
    }

    public String getCanceledReason() {
        return canceledReason;
    }

    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    public String getSiteLat() {
        return siteLat;
    }

    public void setSiteLat(String siteLat) {
        this.siteLat = siteLat;
    }

    public String getSiteLon() {
        return siteLon;
    }

    public void setSiteLon(String siteLon) {
        this.siteLon = siteLon;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getNameWhoGives() {
        return nameWhoGives;
    }

    public void setNameWhoGives(String nameWhoGives) {
        this.nameWhoGives = nameWhoGives;
    }

    public String getStatusWhoGives() {
        return statusWhoGives;
    }

    public void setStatusWhoGives(String statusWhoGives) {
        this.statusWhoGives = statusWhoGives;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getNameOfCastingLocation() {
        return nameOfCastingLocation;
    }

    public void setNameOfCastingLocation(String nameOfCastingLocation) {
        this.nameOfCastingLocation = nameOfCastingLocation;
    }

    public String getStructureName() {
        return structureName;
    }

    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getSiteEngineer() {
        return siteEngineer;
    }

    public void setSiteEngineer(String siteEngineer) {
        this.siteEngineer = siteEngineer;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getMixDesignId() {
        return mixDesignId;
    }

    public void setMixDesignId(String mixDesignId) {
        this.mixDesignId = mixDesignId;
    }

    public String getDateOfCasting() {
        return dateOfCasting;
    }

    public void setDateOfCasting(String dateOfCasting) {
        this.dateOfCasting = dateOfCasting;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getOrderQualityOfConcrete() {
        return orderQualityOfConcrete;
    }

    public void setOrderQualityOfConcrete(String orderQualityOfConcrete) {
        this.orderQualityOfConcrete = orderQualityOfConcrete;
    }

    public String getPouringSpeech() {
        return pouringSpeech;
    }

    public void setPouringSpeech(String pouringSpeech) {
        this.pouringSpeech = pouringSpeech;
    }

    public String getConcreteClass() {
        return concreteClass;
    }

    public void setConcreteClass(String concreteClass) {
        this.concreteClass = concreteClass;
    }

    public String getMorter() {
        return morter;
    }

    public void setMorter(String morter) {
        this.morter = morter;
    }

    public boolean isFieldTestRequirment() {
        return fieldTestRequirment;
    }

    public void setFieldTestRequirment(boolean fieldTestRequirment) {
        this.fieldTestRequirment = fieldTestRequirment;
    }

    public String getNumberOfSpecimen() {
        return numberOfSpecimen;
    }

    public void setNumberOfSpecimen(String numberOfSpecimen) {
        this.numberOfSpecimen = numberOfSpecimen;
    }

    public String getSampleForm() {
        return sampleForm;
    }

    public void setSampleForm(String sampleForm) {
        this.sampleForm = sampleForm;
    }

    public String getSlump() {
        return slump;
    }

    public void setSlump(String slump) {
        this.slump = slump;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getAirContent() {
        return airContent;
    }

    public void setAirContent(String airContent) {
        this.airContent = airContent;
    }

    public String getLocationOfTest() {
        return locationOfTest;
    }

    public void setLocationOfTest(String locationOfTest) {
        this.locationOfTest = locationOfTest;
    }

    public String getDaysOfStrengthTest() {
        return daysOfStrengthTest;
    }

    public void setDaysOfStrengthTest(String daysOfStrengthTest) {
        this.daysOfStrengthTest = daysOfStrengthTest;
    }

    public String getCastingMethod() {
        return castingMethod;
    }

    public void setCastingMethod(String castingMethod) {
        this.castingMethod = castingMethod;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public int getDeliveredQuantity() {
        return deliveredQuantity;
    }

    public void setDeliveredQuantity(int deliveredQuantity) {
        this.deliveredQuantity = deliveredQuantity;
    }

    public int getUndeliverQuantity() {
        return undeliverQuantity;
    }

    public void setUndeliverQuantity(int undeliverQuantity) {
        this.undeliverQuantity = undeliverQuantity;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCastingLocation() {
        return castingLocation;
    }

    public void setCastingLocation(String castingLocation) {
        this.castingLocation = castingLocation;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.orderName);
        dest.writeString(this.orderNumber);
        dest.writeString(this.castingLocation);
        dest.writeString(this.createdDate);
        dest.writeString(this.status);
        dest.writeInt(this.statusId);
        dest.writeInt(this.deliveredQuantity);
        dest.writeInt(this.undeliverQuantity);
        dest.writeString(this.totalQuantity);
        dest.writeString(this.siteLat);
        dest.writeString(this.siteLon);
        dest.writeString(this.clientName);
        dest.writeString(this.nameWhoGives);
        dest.writeString(this.statusWhoGives);
        dest.writeString(this.contactName);
        dest.writeString(this.contactNumber);
        dest.writeString(this.projectName);
        dest.writeString(this.nameOfCastingLocation);
        dest.writeString(this.structureName);
        dest.writeString(this.floor);
        dest.writeString(this.siteEngineer);
        dest.writeString(this.siteId);
        dest.writeString(this.mixDesignId);
        dest.writeString(this.dateOfCasting);
        dest.writeString(this.startTime);
        dest.writeString(this.orderQualityOfConcrete);
        dest.writeString(this.pouringSpeech);
        dest.writeString(this.concreteClass);
        dest.writeString(this.morter);
        dest.writeByte(this.fieldTestRequirment ? (byte) 1 : (byte) 0);
        dest.writeString(this.numberOfSpecimen);
        dest.writeString(this.sampleForm);
        dest.writeString(this.slump);
        dest.writeString(this.flow);
        dest.writeString(this.airContent);
        dest.writeString(this.locationOfTest);
        dest.writeString(this.daysOfStrengthTest);
        dest.writeString(this.castingMethod);
        dest.writeString(this.additionalInfo);
        dest.writeString(this.canceledReason);
    }

    public ActiveOrder() {
    }

    protected ActiveOrder(Parcel in) {
        this.id = in.readString();
        this.orderName = in.readString();
        this.orderNumber = in.readString();
        this.castingLocation = in.readString();
        this.createdDate = in.readString();
        this.status = in.readString();
        this.statusId = in.readInt();
        this.deliveredQuantity = in.readInt();
        this.undeliverQuantity = in.readInt();
        this.totalQuantity = in.readString();
        this.siteLat = in.readString();
        this.siteLon = in.readString();
        this.clientName = in.readString();
        this.nameWhoGives = in.readString();
        this.statusWhoGives = in.readString();
        this.contactName = in.readString();
        this.contactNumber = in.readString();
        this.projectName = in.readString();
        this.nameOfCastingLocation = in.readString();
        this.structureName = in.readString();
        this.floor = in.readString();
        this.siteEngineer = in.readString();
        this.siteId = in.readString();
        this.mixDesignId = in.readString();
        this.dateOfCasting = in.readString();
        this.startTime = in.readString();
        this.orderQualityOfConcrete = in.readString();
        this.pouringSpeech = in.readString();
        this.concreteClass = in.readString();
        this.morter = in.readString();
        this.fieldTestRequirment = in.readByte() != 0;
        this.numberOfSpecimen = in.readString();
        this.sampleForm = in.readString();
        this.slump = in.readString();
        this.flow = in.readString();
        this.airContent = in.readString();
        this.locationOfTest = in.readString();
        this.daysOfStrengthTest = in.readString();
        this.castingMethod = in.readString();
        this.additionalInfo = in.readString();
        this.canceledReason = in.readString();
    }

    public static final Creator<ActiveOrder> CREATOR = new Creator<ActiveOrder>() {
        @Override
        public ActiveOrder createFromParcel(Parcel source) {
            return new ActiveOrder(source);
        }

        @Override
        public ActiveOrder[] newArray(int size) {
            return new ActiveOrder[size];
        }
    };
}
