package mn.fullstream.premium_hse.model;

import io.realm.RealmObject;

public class Config extends RealmObject {

	boolean isSavedFingerPrint;

	String fcmToken;
	String fingerPrintCardNumber;
	String fingerPrintPinCode;
	String rememberPhoneNumber;

	public boolean isSavedFingerPrint() {
		return isSavedFingerPrint;
	}

	public void setSavedFingerPrint(boolean savedFingerPrint) {
		isSavedFingerPrint = savedFingerPrint;
	}

	public String getRememberPhoneNumber() {
		return rememberPhoneNumber;
	}

	public void setRememberPhoneNumber(String rememberPhoneNumber) {
		this.rememberPhoneNumber = rememberPhoneNumber;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public String getFingerPrintCardNumber() {
		return fingerPrintCardNumber;
	}

	public void setFingerPrintCardNumber(String fingerPrintCardNumber) {
		this.fingerPrintCardNumber = fingerPrintCardNumber;
	}

	public String getFingerPrintPinCode() {
		return fingerPrintPinCode;
	}

	public void setFingerPrintPinCode(String fingerPrintPinCode) {
		this.fingerPrintPinCode = fingerPrintPinCode;
	}
}
