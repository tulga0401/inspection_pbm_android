package mn.fullstream.premium_hse.model;

public class Issue {

    String id;
    String name;
    boolean isChild;
    boolean isChecked;

    String desc;


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChild() {
        return isChild;
    }

    public void setChild(boolean child) {
        isChild = child;
    }


    @Override
    public String toString() {
        return "Issue{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", isChild=" + isChild +
                ", isChecked=" + isChecked +
                '}';
    }
}
