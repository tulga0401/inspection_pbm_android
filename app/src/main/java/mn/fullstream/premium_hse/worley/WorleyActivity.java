package mn.fullstream.premium_hse.worley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.CustomPermissionDialog;
import mn.fullstream.premium_hse.interfaces.OnAgree;
import mn.fullstream.premium_hse.interfaces.ParentRequestInterface;
import mn.fullstream.premium_hse.login.LoginActivity;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.widget.CustomViewPager;
import mn.fullstream.premium_hse.worley.fragments.WorleyMenusFragment;
import mn.fullstream.premium_hse.worley.fragments.WorleyReportFragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class WorleyActivity extends AppCompatActivity implements OnAgree , ParentRequestInterface {



	FrameLayout logoutButton;
	FrameLayout dashboardButton,menuButton;

	ImageView dashboardImage;
	TextView dashboardText;

	ImageView menuImage;
	TextView menuText;

	CustomViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_worley);





		logoutButton = findViewById(R.id.logoutButton);
		dashboardButton = findViewById(R.id.dashboardButton);
		menuButton = findViewById(R.id.menuButton);

		dashboardImage = findViewById(R.id.dashboardImage);
		dashboardText = findViewById(R.id.dashboardText);

		menuImage = findViewById(R.id.menuImage);
		menuText = findViewById(R.id.menuText);

		viewPager = findViewById(R.id.viewPager);

		logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				logoutButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						CustomPermissionDialog customPermissionDialog = new CustomPermissionDialog(CustomPermissionDialog.DIALOG_WARNING,"Анхааруулга","Та Central Inspection Апп-аас гарах гэж байна.", WorleyActivity.this);

						customPermissionDialog.show(getSupportFragmentManager(),"");
					}
				});
			}
		});


		SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(sectionsPagerAdapter);

		viewPager.setScrollbarFadingEnabled(true);

		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {

				selectPage(position);
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});


		dashboardButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(0);
			}
		});

		menuButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(1);
			}
		});


    }


    void selectPage(int pageCount){
    	if (pageCount==0){

			dashboardButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
			dashboardImage.setColorFilter(ContextCompat.getColor(WorleyActivity.this,
					R.color.colorPrimaryWhite));
			dashboardText.setTextColor(getResources().getColor(R.color.colorPrimaryWhite));



			menuButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryWhite));
			menuImage.setColorFilter(ContextCompat.getColor(WorleyActivity.this,
					R.color.colorPrimary));
			menuText.setTextColor(getResources().getColor(R.color.colorPrimary));
		}else {

			menuButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
			menuImage.setColorFilter(ContextCompat.getColor(WorleyActivity.this,
					R.color.colorPrimaryWhite));
			menuText.setTextColor(getResources().getColor(R.color.colorPrimaryWhite));



			dashboardButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryWhite));
			dashboardImage.setColorFilter(ContextCompat.getColor(WorleyActivity.this,
					R.color.colorPrimary));
			dashboardText.setTextColor(getResources().getColor(R.color.colorPrimary));
		}
	}


	@Override
	public void onAgree() {
		Realm realm = Realm.getInstance(MyApplication.realmConfig);
		realm.executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm bgRealm) {
				bgRealm.where(User.class).findAll().deleteAllFromRealm();
			}
		}, new Realm.Transaction.OnSuccess() {
			@Override
			public void onSuccess() {
				try {
					WorleyActivity.this.finish();

					Intent intent = new Intent(WorleyActivity.this, LoginActivity
							.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void setViewPagerStatus(Boolean b) {
		viewPager.setPagingEnabled(b);
	}

	public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {


			if (position == 0){
				return new WorleyReportFragment();
			}else {
				return new WorleyMenusFragment();
			}


		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {

			return "";

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {

		}
	}


}
