package mn.fullstream.premium_hse.worley.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.LinearLayout;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.worley.WebViewActivity;
import mn.fullstream.premium_hse.worley.model.WorleyMenu;


public class WorleyMenusFragment extends Fragment {

	RecyclerView worleyRecyclerView;

    public WorleyMenusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_worley_menus, container, false);
    }

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		worleyRecyclerView = view.findViewById(R.id.worleyRecyclerView);

		worleyRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));

		worleyRecyclerView.setAdapter(new RecyclerViewAdapter(getActivity(), WorleyMenu.getWorleyMenus()));


	}

	class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;
		ArrayList<WorleyMenu> worleyMenus;

		public RecyclerViewAdapter(Context context, ArrayList<WorleyMenu> worleyMenus) {
			this.context = context;
			this.worleyMenus = worleyMenus;

		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_worley, parent, false);
			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);

			Log.e("RecyclerViewHolders", "RecyclerViewHolders");

			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {


			WorleyMenu worleyMenu = worleyMenus.get(position);
			holder.nameText.setText(worleyMenu.getTitle());
			holder.image.setImageDrawable(getDrawable(worleyMenu.getImage(),getActivity()));

			holder.selectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					Intent intent = new Intent(getActivity(), WebViewActivity.class);

					intent.putExtra(Constants.INTENT_WORLEY_MENU,worleyMenu);

					startActivity(intent);
				}
			});
		}

		@Override
		public int getItemCount() {

			return worleyMenus.size();
		}
	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
		LinearLayout selectButton;
		TextView nameText;
		ImageView image;

		public RecyclerViewHolders(View itemView) {
			super(itemView);
			selectButton = itemView.findViewById(R.id.selectButton);
			nameText = itemView.findViewById(R.id.nameText);
			image = itemView.findViewById(R.id.image);

		}

		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}


	public static Drawable getDrawable(String name, Context context) {

		try {
			int resourceId =
					context.getResources().getIdentifier(name, "mipmap", context.getPackageName());
			return context.getResources().getDrawable(resourceId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
