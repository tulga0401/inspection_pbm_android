package mn.fullstream.premium_hse.worley.fragments;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.airbnb.lottie.LottieAnimationView;

import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.widget.CustomViewPager;
import mn.fullstream.premium_hse.widget.CustomWebView;
import mn.fullstream.premium_hse.worley.WorleyActivity;


public class WorleyReportFragment extends Fragment {

	CustomWebView webView;
	LottieAnimationView mkLoader;

	CustomViewPager viewpager;
	WorleyActivity activity;
	WorleyActivity parentActivity;

	public void setActivity(WorleyActivity activity) {
		this.activity = activity;
	}

	public WorleyReportFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_worley_report, container, false);
	}

	public void setPager(CustomViewPager viewpager) {
		this.viewpager = viewpager;
	}
	public void setViewPager(boolean b) {
		parentActivity.setViewPagerStatus(b);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		webView = view.findViewById(R.id.webView);
		mkLoader = view.findViewById(R.id.lottieAnimationView);
		parentActivity = (WorleyActivity) getActivity();

		loadWebView();
	}

	void loadWebView(){

		webView.setFragment(WorleyReportFragment.this);
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setDisplayZoomControls(false);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultTextEncodingName("utf-8");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}

		webView.setInitialScale(120);
		webView.setScrollContainer(true);
		webView.bringToFront();
		webView.setScrollbarFadingEnabled(true);
		webView.setVerticalScrollBarEnabled(true);
		webView.setHorizontalScrollBarEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setUseWideViewPort(true);

		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
				Log.d("MyApplication", consoleMessage.message() + " -- From line "
						+ consoleMessage.lineNumber() + " of "
						+ consoleMessage.sourceId());
				return super.onConsoleMessage(consoleMessage);
			}
		});

		mkLoader.setVisibility(View.VISIBLE);
		webView.setVisibility(View.GONE);

		webView.loadUrl(Constants.BASE_CENTRALS_API+"/worley/report/mos?isMobile=1");
		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {


				if (url.contains(".pdf")) {
					webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
				} else {
					view.loadUrl(url);
				}

				return false;

			}


			@Override
			public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

				mkLoader.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);

				super.onReceivedError(view, request, error);


			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);


				Log.e("onPageStarted", url);


				mkLoader.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);


			}


			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				mkLoader.setVisibility(View.GONE);
				webView.setVisibility(View.VISIBLE);
			}
		});

	}
}
