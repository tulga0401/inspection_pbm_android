package mn.fullstream.premium_hse.worley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.worley.model.WorleyMenu;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

public class WebViewActivity extends AppCompatActivity {

	WorleyMenu worleyMenu;
	WebView webView;
	LottieAnimationView mkLoader;

	@SuppressLint("WrongViewCast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);

		webView = findViewById(R.id.webView);
		mkLoader = findViewById(R.id.lottieAnimationView);

		worleyMenu = getIntent().getParcelableExtra(Constants.INTENT_WORLEY_MENU);

		setToolbar(worleyMenu.getTitle());

		loadWebView();


	}

	void loadWebView(){
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setDisplayZoomControls(false);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultTextEncodingName("utf-8");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		}
		webSettings.setPluginState(WebSettings.PluginState.ON);


		mkLoader.setVisibility(View.VISIBLE);
		webView.setVisibility(View.GONE);


		webView.loadUrl(worleyMenu.getUrl());

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {


				if (url.contains(".pdf")) {
					webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);


				} else {

					view.loadUrl(url);
				}

				return false;
			}


			@Override
			public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {

				mkLoader.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);

				super.onReceivedError(view, request, error);


			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);


				Log.e("onPageStarted", url);


				mkLoader.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);


			}


			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				mkLoader.setVisibility(View.GONE);
				webView.setVisibility(View.VISIBLE);
			}
		});




		((Toolbar) findViewById(R.id.toolbar_main)).setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (webView.canGoBack()) {
					webView.goBack();
				}else {
					WebViewActivity.this.finish();
				}
			}
		});
	}

	private ValueCallback<Uri[]> mFilePathCallback;



	protected void setToolbar(String title) {
		setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		TextView titleTextView = findViewById(R.id.title);
		titleTextView.setText(title);


		((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main)).setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
