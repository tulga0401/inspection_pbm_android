package mn.fullstream.premium_hse.worley.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class WorleyMenu implements Parcelable {
	String title;
	String url;
	String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public static ArrayList<WorleyMenu> getWorleyMenus(){
		ArrayList<WorleyMenu> worleyMenus = new ArrayList<>();

		WorleyMenu worleyMenu = new WorleyMenu();
		worleyMenu.setTitle("Агуулах");
		worleyMenu.setUrl("http://test2.centrals.mn/worley/warehouse/balance?isMobile=1");
		worleyMenu.setImage("ic_warehouse");

		worleyMenus.add(worleyMenu);

		WorleyMenu worleyMenu1 = new WorleyMenu();
		worleyMenu1.setTitle("Захиалга");
		worleyMenu1.setUrl("http://test2.centrals.mn/worley/order/page?isMobile=1");
		worleyMenu1.setImage("ic_order");

		worleyMenus.add(worleyMenu1);


		WorleyMenu worleyMenu2 = new WorleyMenu();
		worleyMenu2.setTitle("Readiness");
		worleyMenu2.setUrl("http://test2.centrals.mn/worley/readiness/plant/report?isMobile=1");
		worleyMenu2.setImage("ic_readiness");

		worleyMenus.add(worleyMenu2);

		WorleyMenu worleyMenu3 = new WorleyMenu();
		worleyMenu3.setTitle("Map / Tracking");
		worleyMenu3.setUrl("http://test2.centrals.mn/worley/track/?isMobile=1");
		worleyMenu3.setImage("ic_tracking");

		worleyMenus.add(worleyMenu3);

		return worleyMenus;


	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.title);
		dest.writeString(this.url);
		dest.writeString(this.image);
	}

	public WorleyMenu() {
	}

	protected WorleyMenu(Parcel in) {
		this.title = in.readString();
		this.url = in.readString();
		this.image = in.readString();
	}

	public static final Parcelable.Creator<WorleyMenu> CREATOR = new Parcelable.Creator<WorleyMenu>() {
		@Override
		public WorleyMenu createFromParcel(Parcel source) {
			return new WorleyMenu(source);
		}

		@Override
		public WorleyMenu[] newArray(int size) {
			return new WorleyMenu[size];
		}
	};
}
