package mn.fullstream.premium_hse.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import carbon.widget.LinearLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MainActivity;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.LoadingDialogFragment;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;
import mn.fullstream.premium_hse.worley.WorleyActivity;


public class LoginActivity extends AppCompatActivity {

    TextView userNameText, passwordText;
    Button loginButton;

    LinearLayout contactInfoButton;
    ImageView languageImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameText = findViewById(R.id.userNameText);
        passwordText = findViewById(R.id.passwordText);


        loginButton = findViewById(R.id.loginButton);



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {
                    String userName = userNameText.getText().toString();
                    String password = passwordText.getText().toString();


                    new LoginTask(getString(R.string.to_login), userName, password).execute();

                }
            }
        });

    }

	LoadingDialogFragment loadingDialogFragment;

    class LoginTask extends AsyncTask<String, String, String> {

        String loadingTitle = "";
        String userName = "";
        String password = "";

        boolean isSuccess = false;
        String responseMessage = "";

        public LoginTask(String loadingTitle, String userName, String password) {
            this.loadingTitle = loadingTitle;
            this.userName = userName;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            loadingDialogFragment = new LoadingDialogFragment(loadingTitle);
            loadingDialogFragment.show(getSupportFragmentManager(), "");

        }

        @Override
        protected String doInBackground(String... strings) {


            RequestController requestController = new RequestController();

            String response = requestController.loginHttpRequest(MyApplication.checkLocale(LoginActivity.this), userName, password);


            try {

                Log.e("response", response);

                JSONObject resultJson = new JSONObject(response);

                if (resultJson != null) {

                    try {
                        String status = "";
                        status = resultJson.getString("status");

                        if (status.equals("success")) {


                            JSONObject resultJsonObject = resultJson.getJSONObject("result");


                            final Realm realm = Realm.getInstance(MyApplication.realmConfig);


                            realm.beginTransaction();
                            User userDB = realm.createObject(User.class);

                            userDB.setToken(resultJsonObject.has("userToken") ? resultJsonObject.getString("userToken") : "");



                            if (resultJsonObject.has("templateCodes")){

								JSONArray templateCodeJsonArray = resultJsonObject.getJSONArray("templateCodes");


								if (templateCodeJsonArray.length()>0){


									Log.e("code",templateCodeJsonArray.getJSONObject(0).getString("code")+" s");

									userDB.setTemplateCode(templateCodeJsonArray.getJSONObject(0).getString("code"));
								}
							}


                            realm.commitTransaction();
                            realm.refresh();
                            isSuccess = true;

                        }


                        String message = resultJson.getString("message");

                        responseMessage = message;

                    } catch (Exception e) {
                        e.printStackTrace();

                        isSuccess = false;

                        responseMessage = e.getMessage();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          	new LoadUserInfoTask().execute();
        }
    }

	class LoadUserInfoTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		public LoadUserInfoTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				realm.refresh();

				User user = realm.where(User.class).findFirst();
				response = requestController.getHttpRequest(Constants.USER_INFO + "?apiKey=" + user.getToken());

				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray.length() > 0) {


						isSuccess = true;

						JSONObject jsonObject = jsonArray.getJSONObject(0);

						realm.beginTransaction();

						user.setId(jsonObject.has("id") ? jsonObject.getString("id") : "");
						user.setUserName(jsonObject.has("username") ? jsonObject.getString("username") : "");
						user.setEmail(jsonObject.has("email") ? jsonObject.getString("email") : "");
						user.setPosition(jsonObject.has("position") ? jsonObject.getString("position") : "");
						user.setPhoneNumber(jsonObject.has("phone") ? jsonObject.getString("phone") : "");
						user.setFirstName(jsonObject.has("firstName") ? jsonObject.getString("firstName") : "");
						user.setLastName(jsonObject.has("lastName") ? jsonObject.getString("lastName") : "");
						user.setIsWorley(jsonObject.has("isWorley") ? jsonObject.getString("isWorley") : "");


						if (jsonObject.has("company")) {

							user.setCompany(jsonObject.getJSONObject("company").getString("nameMn"));
						}


						realm.commitTransaction();


					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);



			loadingDialogFragment.dismiss();


			if (isSuccess) {

				LoginActivity.this.finish();

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				realm.refresh();
				User user = realm.where(User.class).findFirst();

				if(user.getIsWorley()!=null && user.getIsWorley().equals("true")) {
					startActivity(new Intent(LoginActivity.this, WorleyActivity.class));
				}else {
					startActivity(new Intent(LoginActivity.this, MainActivity.class));
				}

			} else {
				AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Нэвтрэх", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
				alertDialogFragment.show(getSupportFragmentManager(), "");
			}
		}

	}

    boolean isValid() {
        boolean isValid = true;

        String userName = userNameText.getText().toString();
        String password = passwordText.getText().toString();

        if (userName.isEmpty()) {
            isValid = false;

            userNameText.setError(getString(R.string.empty_username));
            userNameText.setFocusable(true);
        } else if (password.isEmpty()) {
            isValid = false;

            passwordText.setError(getString(R.string.empty_password));
            passwordText.setFocusable(true);
        }

        return isValid;

    }



}
