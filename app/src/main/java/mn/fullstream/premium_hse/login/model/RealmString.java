package mn.fullstream.premium_hse.login.model;

import io.realm.RealmObject;

public class RealmString extends RealmObject {
    public String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
