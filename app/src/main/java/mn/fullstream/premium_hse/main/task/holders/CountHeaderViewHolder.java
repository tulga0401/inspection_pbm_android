/*
 * Copyright (C) 2015 Tomás Ruiz-López.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mn.fullstream.premium_hse.main.task.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.model.InspectionHistory;

public class CountHeaderViewHolder extends RecyclerView.ViewHolder {

	TextView dateText;

	public CountHeaderViewHolder(View itemView) {
		super(itemView);

		dateText = itemView.findViewById(R.id.dateText);

	}

	public void render(InspectionHistory inspectionHistory){
		dateText.setText(inspectionHistory.getCreatedAt());
	}




}
