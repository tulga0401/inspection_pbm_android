package mn.fullstream.premium_hse.main.home;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.main.interfaces.OnBottomChangeListener;
import mn.fullstream.premium_hse.model.InspectionMainReport;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;
import mn.fullstream.premium_hse.util.Util;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.r
 */
public class HomeFragment extends Fragment implements SlyCalendarDialog.Callback {

	RecyclerView infoRecyclerView;
	FrameLayout calendarButton;

	LottieAnimationView lottieAnimationView;
	LinearLayout container;

	AnyChartView anyChartView;

	TextView dateText;

	SwipeRefreshLayout swipeRefreshLayout;

	Pie pie;

	public HomeFragment() {
		// Required empty public constructor
	}

	OnBottomChangeListener onBottomChangeListener;


	public HomeFragment(OnBottomChangeListener onBottomChangeListener) {
		this.onBottomChangeListener = onBottomChangeListener;
	}


	public static HomeFragment newInstance() {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_home, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		infoRecyclerView = view.findViewById(R.id.infoRecyclerView);
		calendarButton = view.findViewById(R.id.calendarButton);
		lottieAnimationView = view.findViewById(R.id.lottieAnimationView);
		container = view.findViewById(R.id.container);
		anyChartView = view.findViewById(R.id.any_chart_view);
		dateText = view.findViewById(R.id.dateText);
		swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);


		anyChartView.setProgressBar(view.findViewById(R.id.progress_bar));

		Calendar startCalendar = Calendar.getInstance();
		startCalendar.add(Calendar.DAY_OF_MONTH, -7);
		Date startDate = startCalendar.getTime();

		long start = startDate.getTime();
		Calendar endCalendar = Calendar.getInstance();
		Date endDate = endCalendar.getTime();
		long end = endDate.getTime();

		dateText.setText(Util.getDateFromUnix(start + "") + " - " + Util.getDateFromUnix(end + ""));
		new LoadInfoOrdersTask(Util.getDateFromUnix(start + ""), Util.getDateFromUnix(end + "")).execute();

		Realm realm = Realm.getInstance(MyApplication.realmConfig);

		User user = realm.where(User.class).findFirst();

		if (user == null || user.getUserName() == null || user.getUserName().isEmpty()) {
			new LoadUserInfoTask().execute();
		}

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				swipeRefreshLayout.setRefreshing(false);
				Calendar startCalendar = Calendar.getInstance();
				startCalendar.add(Calendar.DAY_OF_MONTH, -7);
				Date startDate = startCalendar.getTime();

				long start = startDate.getTime();
				Calendar endCalendar = Calendar.getInstance();
				Date endDate = endCalendar.getTime();
				long end = endDate.getTime();

				dateText.setText(Util.getDateFromUnix(start + "") + " - " + Util.getDateFromUnix(end + ""));
				new LoadInfoOrdersTask(Util.getDateFromUnix(start + ""), Util.getDateFromUnix(end + "")).execute();

			}
		});

 		pie = AnyChart.pie();
		anyChartView.setChart(pie);
		calendarButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SlyCalendarDialog slyCalendarDialog = new SlyCalendarDialog()
						.setSingle(false);

				slyCalendarDialog.setCallback(HomeFragment.this);
				slyCalendarDialog.show(getChildFragmentManager(), "");

			}
		});
	}

	@Override
	public void onCancelled() {}

	@Override
	public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
		try {
			Date startDate = firstDate.getTime();
			Date endDate = secondDate.getTime();

			dateText.setText(Util.getDateFromUnix(startDate.getTime() + "") + " - " + Util.getDateFromUnix(endDate.getTime() + ""));
			new LoadInfoOrdersTask(Util.getDateFromUnix(startDate.getTime() + ""), Util.getDateFromUnix(endDate.getTime() + "")).execute();

		} catch (Exception e) {
			AlertDialogFragment customResultDialog = new AlertDialogFragment(getString(R.string.warning), getString(R.string.wrong_date_selection), AlertDialogFragment.ALERT_TYPE_FAIL);
			customResultDialog.show(getChildFragmentManager(), "");
		}
	}

	class LoadInfoOrdersTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		String beginDate;
		String endDate;

		ArrayList<InspectionMainReport> inspectionMainReports = new ArrayList<>();

		public LoadInfoOrdersTask(String beginDate, String endDate) {

			this.beginDate = beginDate;
			this.endDate = endDate;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			container.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);

				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.USER_INFO_MAIN_REPORT + "/" + beginDate + "/" + endDate);

				try {

					isSuccess = true;


					JSONObject jsonObject = new JSONObject(response);

					String allInspectedCount = jsonObject.getString("allInspectedCount");
					String allInspectionsCount = jsonObject.getString("allInspectionCount");
					String allNonConflictedCountObject = jsonObject.getString("allInconsistencyCount");


					InspectionMainReport inspectionMainReport = new InspectionMainReport();

//					if(user.getTemplateCode().equals("10002")) {
//						inspectionMainReport.setName("Баталсан");
//					}else {
						inspectionMainReport.setName("Шалгасан");
//					}

					inspectionMainReport.setCount(allInspectedCount);
					inspectionMainReports.add(inspectionMainReport);

					InspectionMainReport inspectionMainReport1 = new InspectionMainReport();

//					if(user.getTemplateCode().equals("10002")) {
//						inspectionMainReport1.setName("Батлагдаагүй");
//					}else {
						inspectionMainReport1.setName("Шалгалт хийгдээгүй");
//					}

					int allUninspectionsCount = Integer.parseInt(allInspectionsCount) - Integer.parseInt(allInspectedCount);
					inspectionMainReport1.setCount(allUninspectionsCount + "");
					inspectionMainReports.add(inspectionMainReport1);



//					if(user.getTemplateCode().equals("10002")) {
						InspectionMainReport inspectionMainReport2 = new InspectionMainReport();
						inspectionMainReport2.setName("Үл тохирол үүссэн");
						inspectionMainReport2.setCount(allNonConflictedCountObject);
						inspectionMainReports.add(inspectionMainReport2);
//					}

					InspectionMainReport inspectionMainReport3 = new InspectionMainReport();
					inspectionMainReport3.setName("Нийт");
					inspectionMainReport3.setCount(allInspectionsCount);
					inspectionMainReports.add(inspectionMainReport3);


				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				container.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {
					infoRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
					infoRecyclerView.setAdapter(new RecyclerViewAdapter(getActivity(), inspectionMainReports));

					loadGraph(inspectionMainReports);

				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;
		private ArrayList<InspectionMainReport> inspectionMainReports;

		public RecyclerViewAdapter(Context context, ArrayList<InspectionMainReport> inspectionMainReports) {
			this.inspectionMainReports = inspectionMainReports;
			this.context = context;
		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_main_report, parent, false);
			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {

			final InspectionMainReport data = inspectionMainReports.get(position);


			if (data != null) {

				holder.nameText.setText(data.getName());
				holder.countText.setText(data.getCount());
				holder.selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						if (data.getName().equals("Баталсан") || data.getName().equals("Шалгасан")) {
							onBottomChangeListener.onChangePage(1);
						} else if (data.getName().equals("Батлагдаагүй") || data.getName().equals("Шалгалт хийгдээгүй")) {
							onBottomChangeListener.onChangePage(2);
						}
					}
				});
			}
		}

		@Override
		public int getItemCount() {

			return inspectionMainReports.size();
		}


	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

		TextView nameText, countText;
		carbon.widget.LinearLayout selectButton;

		public RecyclerViewHolders(View itemView) {
			super(itemView);
			nameText = itemView.findViewById(R.id.nameText);
			countText = itemView.findViewById(R.id.countText);
			selectButton = itemView.findViewById(R.id.selectButton);

		}

		@Override
		public void onClick(View view) {
		}
	}

	void loadGraph(ArrayList<InspectionMainReport> inspectionMainReports) {


		Log.e("loadGraph","loadGraph");




		pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
			@Override
			public void onClick(Event event) {
				Toast.makeText(getActivity(), event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show();
			}
		});


		final int delayMillis = 500;
		final Handler handler = new Handler();
		final Runnable runnable = new Runnable() {
			public void run() {
				List<DataEntry> data = new ArrayList<>();

				for (InspectionMainReport inspectionMainReport : inspectionMainReports) {

					Log.e("name", inspectionMainReport.getCount());

					if (!inspectionMainReport.getName().equals("Нийт")) {
						data.add(new ValueDataEntry(inspectionMainReport.getName(), Integer.parseInt(inspectionMainReport.getCount())));
					}
				}


				pie.data(data);

			}
		};
		handler.postDelayed(runnable, delayMillis);


		pie.title("Үзүүлэлт");
		pie.labels().position("outside");

		pie.legend().title().enabled(true);
		pie.legend().title()
				.text("Шалгалтын график")
				.padding(0d, 0d, 10d, 0d);

		pie.legend()
				.position("center-bottom")
				.itemsLayout(LegendLayout.HORIZONTAL)
				.align(Align.CENTER);




	}

	class LoadUserInfoTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		public LoadUserInfoTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			container.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);

				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.USER_INFO + "?apiKey=" + user.getToken());

				Log.e("response",response);

				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray.length() > 0) {


						isSuccess = true;

						JSONObject jsonObject = jsonArray.getJSONObject(0);

						realm.beginTransaction();

						user.setId(jsonObject.has("id") ? jsonObject.getString("id") : "");
						user.setUserName(jsonObject.has("username") ? jsonObject.getString("username") : "");
						user.setEmail(jsonObject.has("email") ? jsonObject.getString("email") : "");
						user.setPosition(jsonObject.has("position") ? jsonObject.getString("position") : "");
						user.setPhoneNumber(jsonObject.has("phone") ? jsonObject.getString("phone") : "");
						user.setFirstName(jsonObject.has("firstName") ? jsonObject.getString("firstName") : "");
						user.setLastName(jsonObject.has("lastName") ? jsonObject.getString("lastName") : "");

						if (jsonObject.has("company")) {

							user.setCompany(jsonObject.getJSONObject("company").getString("nameMn"));
						}

						realm.commitTransaction();


					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				container.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {


				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


}
