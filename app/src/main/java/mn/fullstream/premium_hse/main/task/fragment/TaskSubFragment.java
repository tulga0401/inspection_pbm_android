package mn.fullstream.premium_hse.main.task.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.CustomPermissionDialog;
import mn.fullstream.premium_hse.dialogs.SearchTaskDialogFragment;
import mn.fullstream.premium_hse.interfaces.OnAgree;
import mn.fullstream.premium_hse.main.task.GroupInspectActivity;
import mn.fullstream.premium_hse.main.task.InspectActivity;
import mn.fullstream.premium_hse.main.task.holders.CountFooterViewHolder;
import mn.fullstream.premium_hse.main.task.holders.CountHeaderViewHolder;
import mn.fullstream.premium_hse.model.Inspection;
import mn.fullstream.premium_hse.model.InspectionHistory;
import mn.fullstream.premium_hse.model.Template;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;

public class TaskSubFragment extends Fragment implements OnAgree {


	RecyclerView recyclerView;

	ArrayList<Inspection> inspections = new ArrayList<>();

	CountSectionAdapter countSectionAdapter;

	LottieAnimationView lottieAnimationView;

    public TaskSubFragment() {
        // Required empty public constructor
    }


    FrameLayout searchButton;

	Template template;

	String currentStartDate = "";
	String currentEndDate = "";


	public TaskSubFragment(Template template,String currentStartDate,String currentEndDate) {
		this.template = template;
		this.currentStartDate = currentStartDate;
		this.currentEndDate = currentEndDate;
	}




	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_sub, container, false);
    }

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);


		recyclerView = view.findViewById(R.id.recyclerView);
		searchButton = view.findViewById(R.id.searchButton);
		lottieAnimationView = view.findViewById(R.id.lottieAnimationView);
		countSectionAdapter = new CountSectionAdapter(getActivity());
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		recyclerView.setAdapter(countSectionAdapter);

		if(template.getName().toLowerCase().equals("лаб")){
			searchButton.setVisibility(View.VISIBLE);

			searchButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					SearchTaskDialogFragment searchTaskDialogFragment = new SearchTaskDialogFragment();

					searchTaskDialogFragment.show(getChildFragmentManager(),"");

					searchTaskDialogFragment.setOnSearchTaskListener(new SearchTaskDialogFragment.OnSearchTaskListener() {
						@Override
						public void onSearch(String searchValue) {
							new LoadSearchTasks(searchValue).execute();
						}
					});

				}
			});

		}else {
			searchButton.setVisibility(View.GONE);
		}

		new LoadTasks().execute();
	}


	Inspection selectedInspection;

	class LoadTasks extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		boolean isGroup = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;


		public LoadTasks() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			isGroup = false;
			inspections.clear();
			inspectionHistories.clear();
			recyclerView.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				User user = realm.where(User.class).findFirst();

//				response = requestController.getHttpRequest(Constants.USER_UNINSPECTED_API.replace("{beginDate}",currentStartDate).replace("{endDate}",currentEndDate).replace("{code}",template.getCode()));


				response = requestController.getHttpRequest(Constants.USER_UNINSPECTED_API_2.replace("{beginDate}",currentStartDate).replace("{endDate}",currentEndDate).replace("{code}",template.getCode()).replace("{groupId}","0"));

				Log.e("report",response);

				try {

					isSuccess = true;

					JSONObject jsonObject = new JSONObject(response);

					isGroup = jsonObject.getString("list_type").equals("group");

					JSONArray jsonArray = jsonObject.getJSONArray("list");


					for (int i=0;i<jsonArray.length();i++){
						JSONObject inspectionJsonObject = jsonArray.getJSONObject(i);


						Inspection inspection = new Inspection();

						inspection.setGroup(isGroup);


						inspection.setGroupId(inspectionJsonObject.has("_groupId")?inspectionJsonObject.getString("_groupId"):"");
						inspection.setId(inspectionJsonObject.getString("id"));
						inspection.setOrderCompanyName(inspectionJsonObject.has("orderCompanyName")?inspectionJsonObject.getString("orderCompanyName"):"");
						inspection.setQuantity(inspectionJsonObject.has("quantity")?inspectionJsonObject.getString("quantity"):"");
						inspection.setCarRegNumber(inspectionJsonObject.has("carRegNumber")?inspectionJsonObject.getString("carRegNumber"):"");
						inspection.setSite(inspectionJsonObject.has("site")?inspectionJsonObject.getString("site"):"");
						inspection.setSiteNumber(inspectionJsonObject.has("siteNumber")?inspectionJsonObject.getString("siteNumber"):"");
						inspection.setProductName(inspectionJsonObject.has("productName")?inspectionJsonObject.getString("productName"):"");
						inspection.setQuantityMeter(inspectionJsonObject.has("quantityMeter")?inspectionJsonObject.getString("quantityMeter"):"");
						inspection.setPlantName(inspectionJsonObject.has("plantName")?inspectionJsonObject.getString("plantName"):"");

						try {
							inspection.setCreatedAt(inspectionJsonObject.getJSONObject("createdAt").getString("date").substring(0,10));
						}catch (Exception e){
							e.printStackTrace();
						}

						inspection.setName(inspectionJsonObject.getString("generatedName"));
//						inspection.setDesc(inspectionJsonObject.getString("description"));

						inspections.add(inspection);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				recyclerView.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {

					ArrayList<InspectionHistory> tempInspectionHistories = new ArrayList<>();

					String date = "";

					for (Inspection inspection:inspections){

						Log.e("date",date+"");

						if(!date.equals(inspection.getCreatedAt())){
							InspectionHistory inspectionHistory = new InspectionHistory();
							inspectionHistory.setCreatedAt(inspection.getCreatedAt());

							tempInspectionHistories.add(inspectionHistory);
							date = inspection.getCreatedAt();

						}

					}


					for (InspectionHistory inspectionHistory:tempInspectionHistories){


						ArrayList<Inspection> tempInspections = new ArrayList<>();

						for (Inspection inspection:inspections){

							if (inspectionHistory.getCreatedAt().equals(inspection.getCreatedAt())){

								tempInspections.add(inspection);
							}

						}

						inspectionHistory.setInspections(tempInspections);
					}


					inspectionHistories.addAll(tempInspectionHistories);

					if (inspectionHistories.size()>0) {
						countSectionAdapter.notifyDataSetChanged();
					}else {

					}
				} else {
//					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
//					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	class LoadSearchTasks extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		String orderNumber = "";

		RequestController requestController = new RequestController();
		String response;


		public LoadSearchTasks(String orderNumber) {

			this.orderNumber = orderNumber;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			inspections.clear();
			inspectionHistories.clear();
			recyclerView.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.USER_SEARCH_INSPECSTION_API.replace("{orderNumber}",orderNumber));

				Log.e("report",response);

				try {

					isSuccess = true;

					JSONObject jsonObject = new JSONObject(response);

					JSONArray jsonArray = jsonObject.getJSONArray("inspections");


					for (int i=0;i<jsonArray.length();i++){
						JSONObject inspectionJsonObject = jsonArray.getJSONObject(i);


						Inspection inspection = new Inspection();

						inspection.setId(inspectionJsonObject.getString("id"));
						inspection.setOrderCompanyName(inspectionJsonObject.has("orderCompanyName")?inspectionJsonObject.getString("orderCompanyName"):"");
						inspection.setQuantity(inspectionJsonObject.has("quantity")?inspectionJsonObject.getString("quantity"):"");
						inspection.setCarRegNumber(inspectionJsonObject.has("carRegNumber")?inspectionJsonObject.getString("carRegNumber"):"");
						inspection.setSite(inspectionJsonObject.has("site")?inspectionJsonObject.getString("site"):"");
						inspection.setSiteNumber(inspectionJsonObject.has("siteNumber")?inspectionJsonObject.getString("siteNumber"):"");
						inspection.setProductName(inspectionJsonObject.has("productName")?inspectionJsonObject.getString("productName"):"");
						inspection.setQuantityMeter(inspectionJsonObject.has("quantityMeter")?inspectionJsonObject.getString("quantityMeter"):"");
						inspection.setPlantName(inspectionJsonObject.has("plantName")?inspectionJsonObject.getString("plantName"):"");


						try {
							inspection.setCreatedAt(inspectionJsonObject.getJSONObject("createdAt").getString("date").substring(0,10));
						}catch (Exception e){
							e.printStackTrace();
						}

						inspection.setName(inspectionJsonObject.getString("generatedName"));
//						inspection.setDesc(inspectionJsonObject.getString("description"));

						inspections.add(inspection);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				recyclerView.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {

					ArrayList<InspectionHistory> tempInspectionHistories = new ArrayList<>();

					String date = "";

					for (Inspection inspection:inspections){


						Log.e("date",date+"");

						if(!date.equals(inspection.getCreatedAt())){
							InspectionHistory inspectionHistory = new InspectionHistory();
							inspectionHistory.setCreatedAt(inspection.getCreatedAt());

							tempInspectionHistories.add(inspectionHistory);
							date = inspection.getCreatedAt();

						}

					}


					for (InspectionHistory inspectionHistory:tempInspectionHistories){


						ArrayList<Inspection> tempInspections = new ArrayList<>();

						for (Inspection inspection:inspections){

							if (inspectionHistory.getCreatedAt().equals(inspection.getCreatedAt())){

								tempInspections.add(inspection);
							}

						}

						inspectionHistory.setInspections(tempInspections);
					}


					inspectionHistories.addAll(tempInspectionHistories);

					if (inspectionHistories.size()>0) {
						countSectionAdapter.notifyDataSetChanged();
					}else {

					}
				} else {
//					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
//					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	ArrayList<InspectionHistory> inspectionHistories = new ArrayList<>();

	public class CountSectionAdapter extends SectionedRecyclerViewAdapter<CountHeaderViewHolder,
			CountItemViewHolder,
			CountFooterViewHolder> {

		protected Context context = null;


		public CountSectionAdapter(Context context) {
			this.context = context;
		}

		@Override
		protected int getItemCountForSection(int section) {

			return inspectionHistories.get(section).getInspections().size();
		}

		@Override
		protected int getSectionCount() {
			return inspectionHistories.size();
		}

		@Override
		protected boolean hasFooterInSection(int section) {
			return true;
		}

		protected LayoutInflater getLayoutInflater(){
			return LayoutInflater.from(context);
		}

		@Override
		protected CountHeaderViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_parent_header, parent, false);
			return new CountHeaderViewHolder(view);
		}

		@Override
		protected CountFooterViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_empty, parent, false);
			return new CountFooterViewHolder(view);
		}

		@Override
		protected CountItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_uninspected, parent, false);
			return new CountItemViewHolder(view);
		}

		@Override
		protected void onBindSectionHeaderViewHolder(CountHeaderViewHolder holder, int section) {

			InspectionHistory inspectionHistory = inspectionHistories.get(section);
			holder.render(inspectionHistory);
		}

		@Override
		protected void onBindSectionFooterViewHolder(CountFooterViewHolder holder, int section) {

		}

		@Override
		protected void onBindItemViewHolder(CountItemViewHolder holder, int section, int position) {

			InspectionHistory notificationHistory = inspectionHistories.get(section);

			Inspection inspection = notificationHistory.getInspections().get(position);

			holder.render(inspection,context);
		}
	}

	public class CountItemViewHolder extends RecyclerView.ViewHolder {

		TextView title,descText,moreText;
		ImageView circleImageView;

		FrameLayout checkButton, checkCircle;

		carbon.widget.LinearLayout container;

		TextView locationText,companyText,carNumberText,deliveryQuantity,productNameText,siteNumberText,quantityMeterText,plantText;
		LinearLayout siteContainer,companyContainer,carContainer,quantityContainer,productContainer,siteNumberContainer,quantityMeterContainer,plantContainer;




		FrameLayout expandButton;
		ImageView arrowImage;
		LinearLayout descriptionContainer;

		public CountItemViewHolder(View itemView) {
			super(itemView);

			title = (TextView) itemView.findViewById(R.id.title);
			circleImageView = itemView.findViewById(R.id.circleImageView);
			checkButton = itemView.findViewById(R.id.checkButton);
			container = itemView.findViewById(R.id.container);
			descText = itemView.findViewById(R.id.descText);
			expandButton = itemView.findViewById(R.id.expandButton);
			arrowImage = itemView.findViewById(R.id.arrowImage);
			descriptionContainer = itemView.findViewById(R.id.descriptionContainer);
			productNameText = itemView.findViewById(R.id.productNameText);
			siteNumberText = itemView.findViewById(R.id.siteNumberText);
			productContainer = itemView.findViewById(R.id.productContainer);
			siteNumberContainer = itemView.findViewById(R.id.siteNumberContainer);

			siteContainer = itemView.findViewById(R.id.siteContainer);
			companyContainer = itemView.findViewById(R.id.companyContainer);
			carContainer = itemView.findViewById(R.id.carContainer);
			quantityContainer = itemView.findViewById(R.id.quantityContainer);
			quantityMeterContainer = itemView.findViewById(R.id.quantityMeterContainer);
			plantContainer = itemView.findViewById(R.id.plantContainer);

			locationText = itemView.findViewById(R.id.locationText);
			quantityMeterText = itemView.findViewById(R.id.quantityMeterText);
			plantText = itemView.findViewById(R.id.plantText);
			companyText = itemView.findViewById(R.id.companyText);
			carNumberText = itemView.findViewById(R.id.carNumberText);
			deliveryQuantity = itemView.findViewById(R.id.deliveryQuantity);
			moreText = itemView.findViewById(R.id.moreText);
		}


		public void render(Inspection inspection, Context context) {
			try {

				final Inspection data = inspection;


				if (data != null) {

					title.setText("#"+data.getName());
//					descText.setText(data.getDesc());


					checkButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {


							CustomPermissionDialog customPermissionDialog = new CustomPermissionDialog(CustomPermissionDialog.DIALOG_WARNING,"Анхааруулга","Та шалгалтыг шууд хадгалах гэж байна.",TaskSubFragment.this::onAgree);
							customPermissionDialog.show(getChildFragmentManager(),"");

							selectedInspection = data;
						}
					});

					container.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {



							if(inspection.isGroup()){

								Intent intent = new Intent(getActivity(), GroupInspectActivity.class);

								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_CODE, template.getCode());
								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_NAME, template.getName());
								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_GENERATED_NAME, inspection.getName());
								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_BEGINDATE, currentStartDate);
								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_ENDDATE, currentEndDate);
								intent.putExtra(Constants.INTENT_GROUP_INSPECTION_GROUPID, inspection.getGroupId());

								startActivity(intent);
							}else {

								Intent intent = new Intent(getActivity(), InspectActivity.class);

								intent.putExtra(Constants.INTENT_UNINSPECTION, inspection);
								startActivity(intent);
							}
						}
					});

					if (data.getSite()!=null && !data.getSite().isEmpty() && !data.getSite().equals("null")){
						siteContainer.setVisibility(View.VISIBLE);
						locationText.setText(data.getSite());
					}else {
						siteContainer.setVisibility(View.GONE);
					}

					if (data.getCarRegNumber()!=null && !data.getCarRegNumber().isEmpty() && !data.getCarRegNumber().equals("null")){
						carContainer.setVisibility(View.VISIBLE);
						carNumberText.setText(data.getCarRegNumber());
					}else {
						carContainer.setVisibility(View.GONE);
					}

					if (data.getOrderCompanyName()!=null && !data.getOrderCompanyName().isEmpty() && !data.getOrderCompanyName().equals("null")){
						companyContainer.setVisibility(View.VISIBLE);
						companyText.setText(data.getOrderCompanyName());
					}else {
						companyContainer.setVisibility(View.GONE);
					}

					if (data.getQuantity()!=null && !data.getQuantity().isEmpty() && !data.getQuantity().equals("null")){
						quantityContainer.setVisibility(View.VISIBLE);
						deliveryQuantity.setText(data.getQuantity()+" М3");
					}else {
						quantityContainer.setVisibility(View.GONE);
					}

					if (data.getSiteNumber()!=null && !data.getSiteNumber().isEmpty() && !data.getSiteNumber().equals("null")){
						siteNumberContainer.setVisibility(View.VISIBLE);
						siteNumberText.setText(data.getSiteNumber());
					}else {
						siteNumberContainer.setVisibility(View.GONE);
					}

					if (data.getProductName()!=null && !data.getProductName().isEmpty() && !data.getProductName().equals("null")){
						productContainer.setVisibility(View.VISIBLE);
						productNameText.setText(data.getProductName());
					}else {
						productContainer.setVisibility(View.GONE);
					}


					if (data.getQuantityMeter()!=null && !data.getQuantityMeter().isEmpty() && !data.getQuantityMeter().equals("null")){
						quantityMeterContainer.setVisibility(View.VISIBLE);
						quantityMeterText.setText("Хүргэлтийн хэмжээ "+data.getQuantityMeter()+"М3");
					}else {
						quantityMeterContainer.setVisibility(View.GONE);
					}

					if (data.getPlantName()!=null && !data.getPlantName().isEmpty() && !data.getPlantName().equals("null")){
						plantContainer.setVisibility(View.VISIBLE);
						plantText.setText("Үйлдвэр "+data.getPlantName());
					}else {
						plantContainer.setVisibility(View.GONE);
					}

					if (descriptionContainer.getVisibility() == View.VISIBLE){
						arrowImage.setRotation(180f);
						moreText.setText("Хураах");
					}else {
						arrowImage.setRotation(0f);
						moreText.setText("Сунгах");
					}

					expandButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							if (descriptionContainer.getVisibility() == View.VISIBLE){
								arrowImage.setRotation(0f);
								descriptionContainer.setVisibility(View.GONE);
								moreText.setText("Сунгах");
							}else {
								arrowImage.setRotation(180f);
								descriptionContainer.setVisibility(View.VISIBLE);
								moreText.setText("Хураах");
							}
						}
					});
				}
//
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class LoadInspectTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;
		String info = "";

		Inspection inspection;

		public LoadInspectTask(Inspection inspection) {

			this.inspection = inspection;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			recyclerView.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				JSONObject paramJsonObject = new JSONObject();

				paramJsonObject.put("attribute",new JSONArray());


				response = requestController.postHttpJsonParamRequest(Constants.USER_INSPECT_API+"/"+inspection.getId(),paramJsonObject);

				try {

					JSONObject jsonObject = new JSONObject(response);


					if (jsonObject.getString("code").equals("0")){
						isSuccess = true;
						info = jsonObject.getString("message");
					}else {
						info = jsonObject.getString("message");
					}



					Log.e("LoadInspectTask",response);


				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				recyclerView.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {

					inspectionHistories.clear();
					inspections.clear();

					countSectionAdapter.notifyDataSetChanged();

					new LoadTasks().execute();



					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилттай", info, AlertDialogFragment.ALERT_TYPE_SUCCESS);
					alertDialogFragment.show(getChildFragmentManager(), "");



				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Алдаа", info, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onAgree() {

		if (selectedInspection!=null) {
			new LoadInspectTask(selectedInspection).execute();
			selectedInspection = null;
		}
	}
}
