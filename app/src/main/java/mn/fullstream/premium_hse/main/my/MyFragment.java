package mn.fullstream.premium_hse.main.my;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Calendar;

import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.CustomPermissionDialog;
import mn.fullstream.premium_hse.interfaces.OnAgree;
import mn.fullstream.premium_hse.login.LoginActivity;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyFragment extends Fragment implements OnAgree , SlyCalendarDialog.Callback {

    public MyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyFragment newInstance(String param1, String param2) {
        MyFragment fragment = new MyFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my, container, false);
    }

    TextView companyText,positionText,emailText,userNameText,phoneText;

    FrameLayout logoutButton;

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Realm realm = Realm.getInstance(MyApplication.realmConfig);
		realm.refresh();

		User user = realm.where(User.class).findFirst();
		companyText = view.findViewById(R.id.companyText);
		positionText = view.findViewById(R.id.positionText);
		emailText = view.findViewById(R.id.emailText);
		userNameText = view.findViewById(R.id.userNameText);
		phoneText = view.findViewById(R.id.phoneText);
		logoutButton = view.findViewById(R.id.logoutButton);

		if (user!=null){

			companyText.setText(user.getCompany());
			positionText.setText(user.getPosition());
			emailText.setText(user.getEmail());
			userNameText.setText(user.getLastName()+" "+user.getFirstName());
			phoneText.setText(user.getPhoneNumber());

		}



		logoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				CustomPermissionDialog customPermissionDialog = new CustomPermissionDialog(CustomPermissionDialog.DIALOG_WARNING,"Анхааруулга","Та Central Inspection Апп-аас гарах гэж байна.",MyFragment.this);

				customPermissionDialog.show(getChildFragmentManager(),"");
			}
		});

	}

	@Override
	public void onAgree() {

		new LogoutTask().execute();
		Realm realm = Realm.getInstance(MyApplication.realmConfig);
		realm.executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm bgRealm) {
				bgRealm.where(User.class).findAll().deleteAllFromRealm();
			}
		}, new Realm.Transaction.OnSuccess() {
			@Override
			public void onSuccess() {
				try {

					getActivity().finish();

					Intent intent = new Intent(getActivity(), LoginActivity
							.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onCancelled() {

	}

	@Override
	public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {

	}

	class LogoutTask extends AsyncTask<String, String, String> {



		@Override
		protected String doInBackground(String... strings) {
			try {
				RequestController requestController = new RequestController();


				JSONObject paramJsonObject = new JSONObject();


				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				realm.refresh();
				User user = realm.where(User.class).findFirst();

				Log.e("user info",user.toString());

				requestController.getHttpRequest(Constants.API_LOGOUT.replace("{userId}",user.getId()));

			} catch (
					Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

		}
	}
}
