package mn.fullstream.premium_hse.main.task;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import lv.chi.photopicker.PhotoPickerFragment;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogCloseFragment;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.LoadingCounterDialogFragment;
import mn.fullstream.premium_hse.dialogs.LoadingDialogFragment;
import mn.fullstream.premium_hse.dialogs.SelectDialogFragment;
import mn.fullstream.premium_hse.dialogs.SelectMultiDialogFragment;
import mn.fullstream.premium_hse.interfaces.OnAgree;
import mn.fullstream.premium_hse.interfaces.OnSelect;
import mn.fullstream.premium_hse.interfaces.OnSelectImageTypeListener;
import mn.fullstream.premium_hse.interfaces.OnSelectMulti;
import mn.fullstream.premium_hse.model.Attribute;
import mn.fullstream.premium_hse.model.InspectAttribue;
import mn.fullstream.premium_hse.model.Inspection;
import mn.fullstream.premium_hse.model.Option;
import mn.fullstream.premium_hse.model.TempAttributeImage;
import mn.fullstream.premium_hse.util.RequestController;

public class InspectActivity extends AppCompatActivity implements OnSelectImageTypeListener, OnSelect, OnAgree, PhotoPickerFragment.Callback, OnSelectMulti, TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {


	LinearLayout container;
	LottieAnimationView lottieAnimationView;

	FrameLayout saveButton;

	Inspection inspection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inspect);


		container = findViewById(R.id.container);
		lottieAnimationView = findViewById(R.id.lottieAnimationView);
		saveButton = findViewById(R.id.saveButton);

		inspection = getIntent().getParcelableExtra(Constants.INTENT_UNINSPECTION);


		if (inspection==null){


			String inspectId = getIntent().getStringExtra(Constants.INTENT_PARAM_INSPECT_ID);
			String inspectName = getIntent().getStringExtra(Constants.INTENT_PARAM_INSPECT_NAME);
			String inspectCode = getIntent().getStringExtra(Constants.INTENT_PARAM_INSPECT_CODE);



			if (inspectId==null || inspectId.isEmpty()) {
				try {
					inspectId = getIntent().getExtras().getString("inspectId");
					inspectName = getIntent().getExtras().getString("inspectName");
					inspectCode = getIntent().getExtras().getString("inspectCode");
				}catch (Exception e){
					e.printStackTrace();
				}
			}

			inspection = new Inspection();

			inspection.setId(inspectId);
			inspection.setName(inspectName);
			inspection.setCode(inspectCode);

		}


		setToolbar(inspection.getName());

		new LoadTemplateTask().execute();


		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				for (InspectAttribue inspectAttribue : inspectAttribues) {

					Log.e("inspectAttribue", inspectAttribue.getAttribueId() + "");

					if (inspectAttribue.getValue() != null) {
						Log.e("value", inspectAttribue.getValue());
					} else {
						for (String value : inspectAttribue.getValues()) {

							Log.e("value", value);
						}
					}
				}

				new LoadInspectTask().execute();
			}
		});

	}

	ArrayList<TempAttributeImage> tempAttributeImages = new ArrayList<>();

	@Override
	public void onSelectImageType(InspectAttribue inspectAttribue, boolean isGallery, RecyclerViewAdapter recyclerViewAdapter, RecyclerView recyclerView) {

		TempAttributeImage tempAttributeImage = new TempAttributeImage();


		tempAttributeImage.setAttributeId(inspectAttribue.getAttribueId());
		tempAttributeImage.setRecyclerView(recyclerView);
		tempAttributeImage.setRecyclerViewAdapter(recyclerViewAdapter);

	}


	@Override
	public void onSelect(String attributeId, String selectedValue, String selectedId, TextView selectedTextView) {

		InspectAttribue inspectAttribue = new InspectAttribue();
		inspectAttribue.setAttribueId(attributeId);
		inspectAttribue.setValue(selectedValue);
		selectedTextView.setText(selectedValue);
		inspectAttribue.setFormType(Attribute.FORM_SELECT);
		addInspectAttribue(inspectAttribue);

	}


	@Override
	public void onSelectMulti(String attributeId, ArrayList<Option> selectedOptions, TextView selectedTextView) {
		InspectAttribue inspectAttribue = new InspectAttribue();
		inspectAttribue.setAttribueId(attributeId);


		ArrayList<String> selectedValue = new ArrayList<>();
		StringBuilder stringBuilder = new StringBuilder();
		for (Option option:selectedOptions) {
			selectedValue.add(option.getName());
			stringBuilder.append(option.getName()+" ");
		}

		selectedTextView.setText(stringBuilder.toString());

		inspectAttribue.setValues(selectedValue);
		inspectAttribue.setFormType(Attribute.FORM_MULTI_SELECT);
		addInspectAttribue(inspectAttribue);
	}


	TempAttributeImage selectedAttributeImage = null;
	@Override
	public void onImagesPicked(@NotNull ArrayList<Uri> arrayList) {


		if (selectedAttributeImage!=null) {

			uploadFile(arrayList);

		}
	}

	@Override
	public void onAgree() {
		InspectActivity.this.finish();
	}

	@Override
	public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

	}


	class LoadTemplateTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		ArrayList<Attribute> attributes = new ArrayList<>();

		public LoadTemplateTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			container.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				response = requestController.getHttpRequest(Constants.TEMPLATI_API + "" + inspection.getId());

				try {

					JSONObject jsonObject = new JSONObject(response);

					JSONArray attributesJsonArray = jsonObject.getJSONArray("templateAttributes");


					for (int i = 0; i < attributesJsonArray.length(); i++) {

						JSONObject attributeJsonObject = attributesJsonArray.getJSONObject(i).getJSONObject("attribute");


						Attribute attribute = new Attribute();


						attribute.setId(attributeJsonObject.has("id") ? attributeJsonObject.getString("id") : "");
						attribute.setName(attributeJsonObject.has("name") ? attributeJsonObject.getString("name") : "");
						attribute.setFormType(attributeJsonObject.has("formType") ? attributeJsonObject.getString("formType") : "");
						attribute.setGrade(attributeJsonObject.has("grade") ? attributeJsonObject.getString("grade") : "");
						attribute.setIsRequired(attributeJsonObject.has("isRequired") ? attributeJsonObject.getString("isRequired") : "");
						JSONArray optionsJsonArray = attributeJsonObject.getJSONArray("options");

						ArrayList<Option> options = new ArrayList<>();

						for (int k = 0; k < optionsJsonArray.length(); k++) {

							JSONObject optionJsonObject = optionsJsonArray.getJSONObject(k);

							Option option = new Option();


							option.setId(optionJsonObject.has("id") ? optionJsonObject.getString("id") : "");
							option.setName(optionJsonObject.has("optionValue") ? optionJsonObject.getString("optionValue") : "");

							options.add(option);

						}


						attribute.setOptions(options);

						attributes.add(attribute);

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {


				initUiAttribute(attributes);
				container.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);


			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	void initUiAttribute(ArrayList<Attribute> attributes) {
		for (Attribute attribute : attributes) {

			if (attribute.getFormType().equals(Attribute.FORM_CHECKBOX)) {


				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_checkbox, null, false);
				CheckBox checkBox = inflatedLayout.findViewById(R.id.checkbox);
				checkBox.setText(attribute.getName());


				checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						InspectAttribue inspectAttribue = new InspectAttribue();

						inspectAttribue.setAttribueId(attribute.getId());
						inspectAttribue.setValue(isChecked + "");
						inspectAttribue.setFormType(attribute.getFormType());

						addInspectAttribue(inspectAttribue);
					}
				});

				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_CURRENT_NUMBER)) {

				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_edittext, null, false);
				TextInputLayout textInputLayout = inflatedLayout.findViewById(R.id.textInputLayout);
				textInputLayout.setHint(attribute.getName());
				EditText editText = inflatedLayout.findViewById(R.id.editText);
//				editText.setHint(attribute.getName());
				editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);

				editText.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {

					}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable s) {
						InspectAttribue inspectAttribue = new InspectAttribue();

						inspectAttribue.setAttribueId(attribute.getId());
						inspectAttribue.setValue(s + "");
						inspectAttribue.setFormType(attribute.getFormType());

						addInspectAttribue(inspectAttribue);
					}
				});

				container.addView(inflatedLayout);
			} else if (attribute.getFormType().equals(Attribute.FORM_EMAIL)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_edittext, null, false);
				TextInputLayout textInputLayout = inflatedLayout.findViewById(R.id.textInputLayout);
				textInputLayout.setHint(attribute.getName());
				EditText editText = inflatedLayout.findViewById(R.id.editText);
//				editText.setHint(attribute.getName());
				editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
				editText.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {

					}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable s) {
						InspectAttribue inspectAttribue = new InspectAttribue();

						inspectAttribue.setAttribueId(attribute.getId());
						inspectAttribue.setValue(s + "");
						inspectAttribue.setFormType(attribute.getFormType());

						addInspectAttribue(inspectAttribue);
					}
				});

				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_TEXT)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_edittext, null, false);
				TextInputEditText editText = inflatedLayout.findViewById(R.id.editText);
				TextInputLayout textInputLayout = inflatedLayout.findViewById(R.id.textInputLayout);
				textInputLayout.setHint(attribute.getName());
				editText.setInputType(InputType.TYPE_CLASS_TEXT);


				editText.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {

					}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable s) {
						InspectAttribue inspectAttribue = new InspectAttribue();

						inspectAttribue.setAttribueId(attribute.getId());
						inspectAttribue.setValue(s + "");
						inspectAttribue.setFormType(attribute.getFormType());

						addInspectAttribue(inspectAttribue);
					}
				});

				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_TEXTAREA)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_edittext_area, null, false);
				EditText editText = inflatedLayout.findViewById(R.id.editText);
				TextInputLayout textInputLayout = inflatedLayout.findViewById(R.id.textInputLayout);
				textInputLayout.setHint(attribute.getName());
				editText.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);


				editText.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {

					}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable s) {
						InspectAttribue inspectAttribue = new InspectAttribue();

						inspectAttribue.setAttribueId(attribute.getId());
						inspectAttribue.setValue(s + "");
						inspectAttribue.setFormType(attribute.getFormType());

						addInspectAttribue(inspectAttribue);
					}
				});

				container.addView(inflatedLayout);

			}else if (attribute.getFormType().equals(Attribute.FORM_IMAGE)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_image, null, false);
				carbon.widget.FrameLayout imageAddButton = inflatedLayout.findViewById(R.id.addImageButton);
				RecyclerView imageRecyclerView = inflatedLayout.findViewById(R.id.imageRecyclerView);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);


				nameText.setText(attribute.getName());
				RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(InspectActivity.this);
				imageRecyclerView.setLayoutManager(new LinearLayoutManager(InspectActivity.this, LinearLayoutManager.HORIZONTAL, false));
				imageRecyclerView.setAdapter(recyclerViewAdapter);
				imageAddButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						selectedAttributeImage = new TempAttributeImage();

						selectedAttributeImage.setRecyclerViewAdapter(recyclerViewAdapter);
						selectedAttributeImage.setRecyclerView(imageRecyclerView);
						selectedAttributeImage.setAttributeId(attribute.getId());
						openPicker();
					}
				});


				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_SELECT)) {


				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_select, null, false);
				TextView buttonText = inflatedLayout.findViewById(R.id.buttonText);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);
				FrameLayout selectButton = inflatedLayout.findViewById(R.id.selectButton);
				nameText.setText(attribute.getName());


				selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						SelectDialogFragment selectDialogFragment = new SelectDialogFragment(attribute.getId(),attribute.getOptions(),InspectActivity.this::onSelect,buttonText);
						selectDialogFragment.show(getSupportFragmentManager(),"");
					}
				});


				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_MULTI_SELECT)) {


				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_select, null, false);
				TextView buttonText = inflatedLayout.findViewById(R.id.buttonText);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);
				FrameLayout selectButton = inflatedLayout.findViewById(R.id.selectButton);
				nameText.setText(attribute.getName());


				selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						SelectMultiDialogFragment selectDialogFragment = new SelectMultiDialogFragment(attribute.getId(),attribute.getOptions(),InspectActivity.this::onSelectMulti,buttonText);
						selectDialogFragment.show(getSupportFragmentManager(),"");
					}
				});


				container.addView(inflatedLayout);

			}else if (attribute.getFormType().equals(Attribute.FORM_TIME_PICK)) {


				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_date, null, false);
				TextView buttonText = inflatedLayout.findViewById(R.id.buttonText);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);
				FrameLayout selectButton = inflatedLayout.findViewById(R.id.selectButton);
				nameText.setText(attribute.getName());


				selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Calendar now = Calendar.getInstance();

						TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(InspectActivity.this,now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),now.get(Calendar.SECOND),true);

						timePickerDialog.show(getSupportFragmentManager(),"");

						timePickerDialog.setOnTimeSetListener(new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

								InspectAttribue inspectAttribue = new InspectAttribue();


								String timeValue = hourOfDay+":"+minute+":"+second;
								inspectAttribue.setAttribueId(attribute.getId());
								inspectAttribue.setValue(timeValue);
								inspectAttribue.setFormType(attribute.getFormType());

								addInspectAttribue(inspectAttribue);


								buttonText.setText(timeValue);
								buttonText.setTextColor(getResources().getColor(R.color.textColor));


							}
						});

					}
				});


				container.addView(inflatedLayout);

			}else if (attribute.getFormType().equals(Attribute.FORM_CALENDAR)) {


				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_date, null, false);
				TextView buttonText = inflatedLayout.findViewById(R.id.buttonText);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);
				FrameLayout selectButton = inflatedLayout.findViewById(R.id.selectButton);
				nameText.setText(attribute.getName());

				selectButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						Calendar now = Calendar.getInstance();

						DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(InspectActivity.this,now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DAY_OF_MONTH));
						datePickerDialog.show(getSupportFragmentManager(),"");

						datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


								InspectAttribue inspectAttribue = new InspectAttribue();


								String dateValue = year+"-"+monthOfYear+"-"+dayOfMonth;
								inspectAttribue.setAttribueId(attribute.getId());
								inspectAttribue.setValue(dateValue);
								inspectAttribue.setFormType(attribute.getFormType());

								addInspectAttribue(inspectAttribue);

								buttonText.setText(dateValue);
								buttonText.setTextColor(getResources().getColor(R.color.textColor));


							}
						});

					}
				});


				container.addView(inflatedLayout);

			}
		}
	}

	ArrayList<InspectAttribue> inspectAttribues = new ArrayList<>();

	void addInspectAttribue(InspectAttribue inspectAttribue) {

		boolean addInspectAttribue = true;

		for (InspectAttribue tempInspectAttribue : inspectAttribues) {

			if (tempInspectAttribue.getAttribueId().equals(inspectAttribue.getAttribueId())) {

				tempInspectAttribue.setValue(inspectAttribue.getValue());

				addInspectAttribue = false;
			}
		}

		if (addInspectAttribue) {
			inspectAttribues.add(inspectAttribue);
		}

	}

	private void openPicker() {
		PhotoPickerFragment.Companion.newInstance(true,true,10,R.style.SamplePhotoPicker).show(getSupportFragmentManager(),"picker");
	}

	void uploadFile(ArrayList<Uri> galleryDatas) {


		LoadingCounterDialogFragment loadingDialogFragment = new LoadingCounterDialogFragment("Зураг хуулж байна түр хүлээнэ үү.");

		loadingDialogFragment.show(getSupportFragmentManager(), "");



		for (int i=0;i<galleryDatas.size();i++) {

			Uri galleryData = galleryDatas.get(i);

			Random r = new Random();
			int i1 = r.nextInt(1000000000);

			FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
			StorageReference mountainsRef = firebaseStorage.getReference().child(i1 + ".jpg");


			UploadTask uploadTask = mountainsRef.putFile(galleryData);

			int finalI = i;
			int finalI1 = i;

			int k = i+1;

			loadingDialogFragment.uploadImageDesc("Зураг "+"("+galleryDatas.size()+"/"+k+")");

			uploadTask.addOnFailureListener(new OnFailureListener() {
				@Override
				public void onFailure(@NonNull Exception exception) {
					// Handle unsuccessful uploads

					Toast.makeText(InspectActivity.this, "Зураг хуулалт амжилтгүй боллоо.", Toast.LENGTH_SHORT).show();
				}
			}).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

					Log.e("tmep", taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());


					Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();


					result.addOnSuccessListener(new OnSuccessListener<Uri>() {
						@Override
						public void onSuccess(Uri uri) {
							String imageUrl = uri.toString();
							selectedAttributeImage.getImages().add(imageUrl);
							selectedAttributeImage.getRecyclerViewAdapter().addItem(imageUrl);
						}
					});



					if (finalI ==(galleryDatas.size()-1)){

						InspectAttribue inspectAttribue = new InspectAttribue();
						inspectAttribue.setAttribueId(selectedAttributeImage.getAttributeId());
						inspectAttribue.setFormType(Attribute.FORM_IMAGE);
						inspectAttribue.setValues(selectedAttributeImage.getImages());

						addInspectAttribue(inspectAttribue);


						loadingDialogFragment.dismiss();
					}


				}
			}).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
					double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
				}
			});
		}



	}

	public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;

		ArrayList<String> imageUrls = new ArrayList<>();

		public RecyclerViewAdapter(Context context) {
			this.context = context;
		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_image, parent, false);
			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {

			final String data = imageUrls.get(position);


			Log.e("data", data.toString());
			if (data != null) {


				Picasso.get().load(data).into(holder.image);

				holder.deleteButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						LoadingDialogFragment loadingDialogFragment = new LoadingDialogFragment("Устгаж байна..");

						loadingDialogFragment.show(getSupportFragmentManager(),"");

						StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(data);
						storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
							@Override
							public void onSuccess(Void aVoid) {
								// File deleted successfully

								imageUrls.remove(data);
								notifyDataSetChanged();

								loadingDialogFragment.dismiss();
							}
						}).addOnFailureListener(new OnFailureListener() {
							@Override
							public void onFailure(@NonNull Exception exception) {
								// Uh-oh, an error occurred!

								loadingDialogFragment.dismiss();
								Log.e("firebasestorage", "onFailure: did not delete file");
							}
						});
					}
				});

			}
		}

		@Override
		public int getItemCount() {

			return imageUrls.size();
		}

		public void addItem(String image) {

			Log.e("image", image);
			imageUrls.add(image);

			notifyDataSetChanged();
		}

	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

		ImageView image;
		FrameLayout deleteButton;

		public RecyclerViewHolders(View itemView) {
			super(itemView);
			image = itemView.findViewById(R.id.image);
			deleteButton = itemView.findViewById(R.id.deleteButton);

		}

		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}

	protected void setToolbar(String title) {
		setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		TextView titleTextView = findViewById(R.id.title);
		titleTextView.setText(title);


		((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main)).setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	class LoadInspectTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;
		String info = "";

		LoadingDialogFragment loadingDialogFragment;

		public LoadInspectTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			loadingDialogFragment = new LoadingDialogFragment("Хадгалж байна...");
			loadingDialogFragment.show(getSupportFragmentManager(), "");
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				JSONObject paramJsonObject = new JSONObject();


				JSONArray attributes = new JSONArray();


				for (InspectAttribue inspectAttribue : inspectAttribues) {
					JSONObject attribute = new JSONObject();

					attribute.put("id", inspectAttribue.getAttribueId());
					attribute.put("formType", inspectAttribue.getFormType());

					if (inspectAttribue.getValue() != null && !inspectAttribue.getValue().isEmpty()) {
						attribute.put("value", inspectAttribue.getValue());
					} else {

						JSONArray valuesJsonArray = new JSONArray();

						for (String value : inspectAttribue.getValues()) {
							valuesJsonArray.put(value);
						}


						attribute.put("value", valuesJsonArray);
					}

					attributes.put(attribute);
				}


				paramJsonObject.put("attribute", attributes);

				Log.e("paramJsonObject.", paramJsonObject.toString());

				response = requestController.postHttpJsonParamRequest(Constants.USER_INSPECT_API + "/" + inspection.getId(), paramJsonObject);

				try {

					JSONObject jsonObject = new JSONObject(response);


					if (jsonObject.getString("code").equals("0")) {
						isSuccess = true;
						info = jsonObject.getString("message");
					} else {
						info = jsonObject.getString("message");
					}


					Log.e("LoadInspectTask", response);


				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				loadingDialogFragment.dismiss();
				if (isSuccess) {
					AlertDialogCloseFragment alertDialogFragment = new AlertDialogCloseFragment("Амжилттай", info, AlertDialogFragment.ALERT_TYPE_SUCCESS, InspectActivity.this);
					alertDialogFragment.show(getSupportFragmentManager(), "");

				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Алдаа", info, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getSupportFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
