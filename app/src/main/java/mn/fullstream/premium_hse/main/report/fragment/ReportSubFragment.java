package mn.fullstream.premium_hse.main.report.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.truizlop.sectionedrecyclerview.SectionedRecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.main.report.InspectedActivity;
import mn.fullstream.premium_hse.main.task.holders.CountFooterViewHolder;
import mn.fullstream.premium_hse.main.task.holders.CountHeaderViewHolder;
import mn.fullstream.premium_hse.model.Inspection;
import mn.fullstream.premium_hse.model.InspectionHistory;
import mn.fullstream.premium_hse.model.Template;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;

public class ReportSubFragment extends Fragment {

	RecyclerView recyclerView;

	ArrayList<Inspection> inspections = new ArrayList<>();

	CountSectionAdapter countSectionAdapter;

	LottieAnimationView lottieAnimationView;

    public ReportSubFragment() {
        // Required empty public constructor
    }


    Template template;

	String currentStartDate = "";
	String currentEndDate = "";


	public ReportSubFragment(Template template,String currentStartDate,String currentEndDate) {
		this.template = template;
		this.currentStartDate = currentStartDate;
		this.currentEndDate = currentEndDate;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_sub, container, false);
    }

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);


		recyclerView = view.findViewById(R.id.recyclerView);
		lottieAnimationView = view.findViewById(R.id.lottieAnimationView);
		countSectionAdapter = new CountSectionAdapter(getActivity());
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		recyclerView.setAdapter(countSectionAdapter);


		new LoadTasks().execute();
	}

	class LoadTasks extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;


		public LoadTasks() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			inspections.clear();
			inspectionHistories.clear();
			recyclerView.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.USER_INSPECTED_API.replace("{beginDate}",currentStartDate).replace("{endDate}",currentEndDate).replace("{code}",template.getCode()));


				Log.e("report",response);

				try {

					isSuccess = true;

					JSONObject jsonObject = new JSONObject(response);

					JSONArray jsonArray = jsonObject.getJSONArray("inspections");


					for (int i=0;i<jsonArray.length();i++){
						JSONObject inspectionJsonObject = jsonArray.getJSONObject(i);


						Inspection inspection = new Inspection();

						inspection.setId(inspectionJsonObject.getString("id"));
						inspection.setOrderCompanyName(inspectionJsonObject.has("orderCompanyName")?inspectionJsonObject.getString("orderCompanyName"):"");
						inspection.setQuantity(inspectionJsonObject.has("quantity")?inspectionJsonObject.getString("quantity"):"");
						inspection.setCarRegNumber(inspectionJsonObject.has("carRegNumber")?inspectionJsonObject.getString("carRegNumber"):"");
						inspection.setSite(inspectionJsonObject.has("site")?inspectionJsonObject.getString("site"):"");
						inspection.setSiteNumber(inspectionJsonObject.has("siteNumber")?inspectionJsonObject.getString("siteNumber"):"");
						inspection.setProductName(inspectionJsonObject.has("productName")?inspectionJsonObject.getString("productName"):"");


						try {
							inspection.setCreatedAt(inspectionJsonObject.getJSONObject("createdAt").getString("date").substring(0,10));
						}catch (Exception e){
							e.printStackTrace();
						}

						inspection.setName(inspectionJsonObject.getString("generatedName"));
//						inspection.setDesc(inspectionJsonObject.getString("description"));

						inspections.add(inspection);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				recyclerView.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {

					ArrayList<InspectionHistory> tempInspectionHistories = new ArrayList<>();

					String date = "";

					for (Inspection inspection:inspections){


						Log.e("date",date+"");

						if(!date.equals(inspection.getCreatedAt())){
							InspectionHistory inspectionHistory = new InspectionHistory();
							inspectionHistory.setCreatedAt(inspection.getCreatedAt());

							tempInspectionHistories.add(inspectionHistory);
							date = inspection.getCreatedAt();

						}

					}


					for (InspectionHistory inspectionHistory:tempInspectionHistories){


						ArrayList<Inspection> tempInspections = new ArrayList<>();

						for (Inspection inspection:inspections){

							if (inspectionHistory.getCreatedAt().equals(inspection.getCreatedAt())){

								tempInspections.add(inspection);
							}

						}

						inspectionHistory.setInspections(tempInspections);
					}


					inspectionHistories.addAll(tempInspectionHistories);

					if (inspectionHistories.size()>0) {
						countSectionAdapter.notifyDataSetChanged();
					}else {

					}
				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getChildFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	ArrayList<InspectionHistory> inspectionHistories = new ArrayList<>();

	public class CountSectionAdapter extends SectionedRecyclerViewAdapter<CountHeaderViewHolder,
			CountItemViewHolder,
			CountFooterViewHolder> {

		protected Context context = null;


		public CountSectionAdapter(Context context) {
			this.context = context;
		}

		@Override
		protected int getItemCountForSection(int section) {

			return inspectionHistories.get(section).getInspections().size();
		}

		@Override
		protected int getSectionCount() {
			return inspectionHistories.size();
		}

		@Override
		protected boolean hasFooterInSection(int section) {
			return true;
		}

		protected LayoutInflater getLayoutInflater(){
			return LayoutInflater.from(context);
		}

		@Override
		protected CountHeaderViewHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_parent_header, parent, false);
			return new CountHeaderViewHolder(view);
		}

		@Override
		protected CountFooterViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_empty, parent, false);
			return new CountFooterViewHolder(view);
		}

		@Override
		protected CountItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
			View view = getLayoutInflater().inflate(R.layout.cell_uninspected, parent, false);
			return new CountItemViewHolder(view);
		}

		@Override
		protected void onBindSectionHeaderViewHolder(CountHeaderViewHolder holder, int section) {

			InspectionHistory inspectionHistory = inspectionHistories.get(section);
			holder.render(inspectionHistory);
		}

		@Override
		protected void onBindSectionFooterViewHolder(CountFooterViewHolder holder, int section) {

		}

		@Override
		protected void onBindItemViewHolder(CountItemViewHolder holder, int section, int position) {

			InspectionHistory notificationHistory = inspectionHistories.get(section);

			Inspection inspection = notificationHistory.getInspections().get(position);

			holder.render(inspection,context);
		}
	}

	public class CountItemViewHolder extends RecyclerView.ViewHolder {

		TextView title,descText,moreText;
		ImageView circleImageView;

		FrameLayout checkButton, checkCircle;

		carbon.widget.LinearLayout container;

		TextView locationText,companyText,carNumberText,deliveryQuantity,productNameText,siteNumberText;
		LinearLayout siteContainer,companyContainer,carContainer,quantityContainer,productContainer,siteNumberContainer;


		FrameLayout expandButton;
		ImageView arrowImage;
		LinearLayout descriptionContainer;


		public CountItemViewHolder(View itemView) {
			super(itemView);

			title = (TextView) itemView.findViewById(R.id.title);
			circleImageView = itemView.findViewById(R.id.circleImageView);
			checkButton = itemView.findViewById(R.id.checkButton);
			container = itemView.findViewById(R.id.container);
			descText = itemView.findViewById(R.id.descText);


			siteContainer = itemView.findViewById(R.id.siteContainer);
			companyContainer = itemView.findViewById(R.id.companyContainer);
			carContainer = itemView.findViewById(R.id.carContainer);
			quantityContainer = itemView.findViewById(R.id.quantityContainer);



			locationText = itemView.findViewById(R.id.locationText);
			companyText = itemView.findViewById(R.id.companyText);
			carNumberText = itemView.findViewById(R.id.carNumberText);
			deliveryQuantity = itemView.findViewById(R.id.deliveryQuantity);


			descriptionContainer = itemView.findViewById(R.id.descriptionContainer);
			productNameText = itemView.findViewById(R.id.productNameText);
			siteNumberText = itemView.findViewById(R.id.siteNumberText);
			productContainer = itemView.findViewById(R.id.productContainer);
			siteNumberContainer = itemView.findViewById(R.id.siteNumberContainer);
			arrowImage = itemView.findViewById(R.id.arrowImage);
			expandButton = itemView.findViewById(R.id.expandButton);
			moreText = itemView.findViewById(R.id.moreText);
		}


		public void render(Inspection inspection, Context context) {
			try {

				final Inspection data = inspection;


				if (data != null) {

					title.setText("#"+data.getName());
//					descText.setText(data.getDesc());


					moreText.setTextColor(getResources().getColor(R.color.colorPrimaryWhite));

					checkButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

					container.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(getActivity(), InspectedActivity.class);

							intent.putExtra(Constants.INTENT_UNINSPECTION,inspection);
							startActivity(intent);
						}
					});


					if (data.getSite()!=null && data.getSite().isEmpty() && !data.getSite().equals("null")){
						siteContainer.setVisibility(View.VISIBLE);
						locationText.setText(data.getSite());
					}else {
						siteContainer.setVisibility(View.GONE);
					}

					if (data.getCarRegNumber()!=null && data.getCarRegNumber().isEmpty() && !data.getCarRegNumber().equals("null")){
						carContainer.setVisibility(View.VISIBLE);
						carNumberText.setText(data.getSite());
					}else {
						carContainer.setVisibility(View.GONE);
					}

					if (data.getOrderCompanyName()!=null && data.getOrderCompanyName().isEmpty() && !data.getOrderCompanyName().equals("null")){
						companyContainer.setVisibility(View.VISIBLE);
						companyText.setText(data.getSite());
					}else {
						companyContainer.setVisibility(View.GONE);
					}

					if (data.getQuantity()!=null && data.getQuantity().isEmpty() && !data.getQuantity().equals("null")){
						quantityContainer.setVisibility(View.VISIBLE);
						deliveryQuantity.setText(data.getSite());
					}else {
						quantityContainer.setVisibility(View.GONE);
					}

					if (data.getSite()!=null && !data.getSite().isEmpty() && !data.getSite().equals("null")){
						siteContainer.setVisibility(View.VISIBLE);
						locationText.setText(data.getSite());
					}else {
						siteContainer.setVisibility(View.GONE);
					}

					if (data.getCarRegNumber()!=null && !data.getCarRegNumber().isEmpty() && !data.getCarRegNumber().equals("null")){
						carContainer.setVisibility(View.VISIBLE);
						carNumberText.setText(data.getCarRegNumber());
					}else {
						carContainer.setVisibility(View.GONE);
					}

					if (data.getOrderCompanyName()!=null && !data.getOrderCompanyName().isEmpty() && !data.getOrderCompanyName().equals("null")){
						companyContainer.setVisibility(View.VISIBLE);
						companyText.setText(data.getOrderCompanyName());
					}else {
						companyContainer.setVisibility(View.GONE);
					}

					if (data.getQuantity()!=null && !data.getQuantity().isEmpty() && !data.getQuantity().equals("null")){
						quantityContainer.setVisibility(View.VISIBLE);
						deliveryQuantity.setText(data.getQuantity()+" М3");
					}else {
						quantityContainer.setVisibility(View.GONE);
					}

					if (data.getSiteNumber()!=null && !data.getSiteNumber().isEmpty() && !data.getSiteNumber().equals("null")){
						siteNumberContainer.setVisibility(View.VISIBLE);
						siteNumberText.setText(data.getSiteNumber());
					}else {
						siteNumberContainer.setVisibility(View.GONE);
					}

					if (data.getProductName()!=null && !data.getProductName().isEmpty() && !data.getProductName().equals("null")){
						productContainer.setVisibility(View.VISIBLE);
						productNameText.setText(data.getProductName());
					}else {
						productContainer.setVisibility(View.GONE);
					}

					moreText.setTextColor(getResources().getColor(R.color.colorPrimaryWhite));

					if (descriptionContainer.getVisibility() == View.VISIBLE){
						arrowImage.setRotation(180f);
						moreText.setText("Хураах");
					}else {
						arrowImage.setRotation(0f);
						moreText.setText("Сунгах");
					}

					expandButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							if (descriptionContainer.getVisibility() == View.VISIBLE){
								arrowImage.setRotation(0f);
								descriptionContainer.setVisibility(View.GONE);
								moreText.setText("Сунгах");
							}else {
								arrowImage.setRotation(180f);
								descriptionContainer.setVisibility(View.VISIBLE);
								moreText.setText("Хураах");
							}
						}
					});
				}
//
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
