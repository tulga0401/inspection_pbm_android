package mn.fullstream.premium_hse.main.report;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.ImagesBottomFragment;
import mn.fullstream.premium_hse.model.Attribute;
import mn.fullstream.premium_hse.model.InspectAttribue;
import mn.fullstream.premium_hse.model.Inspection;
import mn.fullstream.premium_hse.model.Template;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;

public class InspectedActivity extends AppCompatActivity {

	LinearLayout container;
	LottieAnimationView lottieAnimationView;
	Inspection inspection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inspected);

		container = findViewById(R.id.container);
		lottieAnimationView = findViewById(R.id.lottieAnimationView);

		inspection = getIntent().getParcelableExtra(Constants.INTENT_UNINSPECTION);

		setToolbar(inspection.getName());


		new LoadTasks().execute();


	}

	class LoadTasks extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		String beginDate;
		String endDate;

		ArrayList<Template> templates = new ArrayList<Template>();

		String inspectionAttributeResponse = "";

		JSONArray attributesJsonArray;


		public LoadTasks() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			container.setVisibility(View.GONE);
			lottieAnimationView.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.USER_UNINSPECTED_DETAIL+inspection.getId());

				Log.e("report",response);

				try {



					isSuccess = true;
					Template template = new Template();



					JSONObject jsonObject = new JSONObject(response);

					attributesJsonArray = jsonObject.getJSONArray("templateAttributes");




				} catch (Exception e) {
					e.printStackTrace();

					isSuccess = false;
				}
			} catch (Exception e) {
				e.printStackTrace();

				isSuccess = false;
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {

				container.setVisibility(View.VISIBLE);
				lottieAnimationView.setVisibility(View.GONE);

				if (isSuccess) {
					try {

						ArrayList<InspectAttribue> inspectAttribues = new ArrayList<>();


						if (attributesJsonArray!=null) {

							for (int i = 0; i < attributesJsonArray.length(); i++) {

								JSONObject attributeJsonObject = attributesJsonArray.getJSONObject(i).getJSONObject("attribute");
								InspectAttribue inspectAttribue = new InspectAttribue();

								inspectAttribue.setAttribueId(attributeJsonObject.getString("name"));


								if (attributeJsonObject.has("value") && attributeJsonObject.getString("value").startsWith("[")) {

									JSONArray valueJsonArray = new JSONArray(attributeJsonObject.getString("value"));

									ArrayList<String> values = new ArrayList<>();
									for (int k = 0; k < valueJsonArray.length(); k++) {

										String value = valueJsonArray.getString(k);
										values.add(value);
									}

									inspectAttribue.setValues(values);


								} else {
									inspectAttribue.setValue(attributeJsonObject.has("value") ? attributeJsonObject.getString("value"):"");

								}

								inspectAttribue.setFormType(attributeJsonObject.getString("formType"));
								inspectAttribues.add(inspectAttribue);
							}


							initUiAttribute(inspectAttribues);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}


				} else {
					AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
					alertDialogFragment.show(getSupportFragmentManager(), "");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}


	void initUiAttribute(ArrayList<InspectAttribue> attributes) {
		for (InspectAttribue attribute : attributes) {

			if (attribute.getFormType().equals(Attribute.FORM_IMAGE)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_image_result, null, false);
				RecyclerView imageRecyclerView = inflatedLayout.findViewById(R.id.imageRecyclerView);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);

				nameText.setText(attribute.getAttribueId());

				if (attribute.getValues() != null && attribute.getValues().size() > 0) {
					RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(InspectedActivity.this, attribute.getValues());
					imageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
					imageRecyclerView.setAdapter(recyclerViewAdapter);
				}

				container.addView(inflatedLayout);

			} else if (attribute.getFormType().equals(Attribute.FORM_MULTI_SELECT)) {
				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_multi_select_result, null, false);
				RecyclerView selectsRecyclerView = inflatedLayout.findViewById(R.id.selectsRecyclerView);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);


				nameText.setText(attribute.getAttribueId());

				if (attribute.getValues() != null && attribute.getValues().size() > 0) {
					RecyclerTextViewAdapter recyclerViewAdapter = new RecyclerTextViewAdapter(InspectedActivity.this, attribute.getValues());
					selectsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
					selectsRecyclerView.setAdapter(recyclerViewAdapter);
				}

				container.addView(inflatedLayout);

			} else {

				View inflatedLayout = getLayoutInflater().inflate(R.layout.form_text_result, null, false);
				TextView nameText = inflatedLayout.findViewById(R.id.nameText);
				TextView resultText = inflatedLayout.findViewById(R.id.resultText);
				nameText.setText(attribute.getAttribueId());
				resultText.setText(attribute.getValue());

				container.addView(inflatedLayout);
			}
		}
	}

	protected void setToolbar(String title) {
		setSupportActionBar((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		TextView titleTextView = findViewById(R.id.title);
		titleTextView.setText(title);


		((androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar_main)).setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;

		ArrayList<String> imageUrls = new ArrayList<>();

		public RecyclerViewAdapter(Context context, ArrayList<String> imageUrls) {
			this.context = context;
			this.imageUrls = imageUrls;
		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_image, parent, false);
			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {

			final String data = imageUrls.get(position);

			if (data != null) {

				Picasso.get().load(data).into(holder.image);

				holder.deleteButton.setVisibility(View.GONE);

			}

			holder.selectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					ImagesBottomFragment imagesBottomFragment = new ImagesBottomFragment(position, imageUrls);
					imagesBottomFragment.show(getSupportFragmentManager(), "");
				}
			});


		}

		@Override
		public int getItemCount() {

			return imageUrls.size();
		}

	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

		ImageView image;

		FrameLayout deleteButton, selectButton;

		public RecyclerViewHolders(View itemView) {
			super(itemView);
			image = itemView.findViewById(R.id.image);
			deleteButton = itemView.findViewById(R.id.deleteButton);
			selectButton = itemView.findViewById(R.id.selectButton);

		}

		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}

	public class RecyclerTextViewAdapter extends RecyclerView.Adapter<RecyclerTextViewHolders> {

		private Context context;

		ArrayList<String> values = new ArrayList<>();

		public RecyclerTextViewAdapter(Context context, ArrayList<String> values) {
			this.context = context;
			this.values = values;
		}

		@Override
		public RecyclerTextViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_text, parent, false);
			RecyclerTextViewHolders rcv = new RecyclerTextViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerTextViewHolders holder, int position) {

			final String data = values.get(position);


			holder.optionText.setText(data);
		}

		@Override
		public int getItemCount() {

			return values.size();
		}

	}

	public class RecyclerTextViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

		TextView optionText;

		public RecyclerTextViewHolders(View itemView) {
			super(itemView);
			optionText = itemView.findViewById(R.id.optionText);

		}

		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}

}
