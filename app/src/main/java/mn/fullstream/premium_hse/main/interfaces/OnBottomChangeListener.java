package mn.fullstream.premium_hse.main.interfaces;

public interface OnBottomChangeListener {

	void onChangePage(int position);
}
