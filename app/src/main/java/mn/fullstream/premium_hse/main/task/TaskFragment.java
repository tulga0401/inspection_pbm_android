package mn.fullstream.premium_hse.main.task;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.SearchTaskDialogFragment;
import mn.fullstream.premium_hse.main.task.fragment.TaskSubFragment;
import mn.fullstream.premium_hse.model.Template;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;
import mn.fullstream.premium_hse.util.Util;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskFragment extends Fragment implements SlyCalendarDialog.Callback {
    LottieAnimationView lottieAnimationView;
    LinearLayout container;
    TextView dateText;

    FrameLayout calendarButton, searchButton;

    String currentStartDate = "";
    String currentEndDate = "";

    SmartTabLayout viewPagerTab;
    ViewPager viewPager;

    public TaskFragment() {
        // Required empty public constructor
    }


    public static TaskFragment newInstance(String param1, String param2) {
        TaskFragment fragment = new TaskFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lottieAnimationView = view.findViewById(R.id.lottieAnimationView);
        container = view.findViewById(R.id.container);
        dateText = view.findViewById(R.id.dateText);
        calendarButton = view.findViewById(R.id.calendarButton);
        searchButton = view.findViewById(R.id.searchButton);
        viewPagerTab = view.findViewById(R.id.viewPagerTab);
        viewPager = view.findViewById(R.id.viewPager);


        Calendar startCalendar = Calendar.getInstance();
        startCalendar.add(Calendar.DAY_OF_MONTH, -7);
        Date startDate = startCalendar.getTime();

        long start = startDate.getTime();
        Calendar endCalendar = Calendar.getInstance();
        Date endDate = endCalendar.getTime();
        long end = endDate.getTime();


        currentStartDate = Util.getDateFromUnix(end + "");
        currentEndDate = Util.getDateFromUnix(end + "");
        dateText.setText(currentStartDate + " - " + currentEndDate);

        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SlyCalendarDialog slyCalendarDialog = new SlyCalendarDialog()
                        .setSingle(false);

                slyCalendarDialog.setCallback(TaskFragment.this);
                slyCalendarDialog.show(getChildFragmentManager(), "");

            }
        });

    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
        try {
            Date startDate = firstDate.getTime();
            Date endDate = secondDate.getTime();


            currentStartDate = Util.getDateFromUnix(startDate.getTime() + "");
            currentEndDate = Util.getDateFromUnix(endDate.getTime() + "");

            dateText.setText(Util.getDateFromUnix(startDate.getTime() + "") + " - " + Util.getDateFromUnix(endDate.getTime() + ""));
            new LoadTasks().execute();

        } catch (Exception e) {
            AlertDialogFragment customResultDialog = new AlertDialogFragment(getString(R.string.warning), getString(R.string.wrong_date_selection), AlertDialogFragment.ALERT_TYPE_FAIL);
            customResultDialog.show(getChildFragmentManager(), "");
        }
    }

    class LoadTasks extends AsyncTask<String, String, String> {

        boolean isSuccess = false;
        String responseMessage = "";

        RequestController requestController = new RequestController();
        String response;

        String beginDate;
        String endDate;

        ArrayList<Template> templates = new ArrayList<Template>();

        public LoadTasks() {

            this.beginDate = currentStartDate;
            this.endDate = currentEndDate;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            container.setVisibility(View.GONE);
            lottieAnimationView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                Realm realm = Realm.getInstance(MyApplication.realmConfig);
                User user = realm.where(User.class).findFirst();

                response = requestController.getHttpRequest(Constants.USER_UNINSPECTED_TEMPLATE + "uninspected/" + beginDate + "/" + endDate);

                Log.e("report", response);

                try {


                    isSuccess = true;


                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Template template = new Template();

                        template.setCode(jsonObject.has("code") ? jsonObject.getString("code") : "");
                        template.setName(jsonObject.has("name") ? jsonObject.getString("name") : "");

                        templates.add(template);

                    }


                } catch (Exception e) {
                    e.printStackTrace();

                    isSuccess = false;
                }
            } catch (Exception e) {
                e.printStackTrace();

                isSuccess = false;
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                container.setVisibility(View.VISIBLE);
                lottieAnimationView.setVisibility(View.GONE);

                if (isSuccess) {
                    Log.e("templates", templates.size() + "");
                    SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), templates);


                    viewPager.setAdapter(sectionsPagerAdapter);

                    viewPager.setScrollbarFadingEnabled(true);


                    final LayoutInflater inflater = LayoutInflater.from(viewPagerTab.getContext());

                    viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {

                        @Override
                        public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {

                            android.widget.FrameLayout view = (android.widget.FrameLayout) inflater.inflate(R.layout.cell_tab_text_view, container, false);

                            TextView tabText = (TextView) view.findViewById(R.id.tabText);

                            Template template = templates.get(position);

                            Log.e("template name", template.getName());
                            Log.e("position", position + "");

                            tabText.setText(template.getName());

                            return view;
                        }
                    });

                    viewPagerTab.setViewPager(viewPager);
                } else {
                    AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
                    alertDialogFragment.show(getChildFragmentManager(), "");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadTasks().execute();

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        ArrayList<Template> templates;

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<Template> templates) {
            super(fm);

            this.templates = templates;

        }

        @Override
        public TaskSubFragment getItem(int position) {

            Template template = templates.get(position);

            Log.e("template", template.getName());

            return new TaskSubFragment(template, currentStartDate, currentEndDate);
        }


        @Override
        public int getCount() {
            return templates.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return "";

        }

    }


}
