package mn.fullstream.premium_hse;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.FrameLayout;
import carbon.widget.LinearLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.CalendarDialogFragment;
import mn.fullstream.premium_hse.dialogs.LoadingDialogFragment;
import mn.fullstream.premium_hse.interfaces.OnSelectDateListener;
import mn.fullstream.premium_hse.login.LoginActivity;
import mn.fullstream.premium_hse.model.ActiveOrder;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.util.RequestController;

public class HistoryOrderActivity extends AppCompatActivity implements OnSelectDateListener {

    LinearLayout chooseDateButton;
    RecyclerView ordersRecyclerView;
    android.widget.TextView currentDate;
    FrameLayout logoutButton;
    ImageView notfoundImage;

    String currentFormattedDate = "";
    SwipeRefreshLayout swipeRefreshLayout;
    TextView userName;
    carbon.widget.FrameLayout actionBarProfileButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_order);
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c);

        SimpleDateFormat currentDf = new SimpleDateFormat("MM сарын dd");
        final String currentDateValue = currentDf.format(c);

        chooseDateButton = (LinearLayout) findViewById(R.id.chooseDateButton);
        ordersRecyclerView = findViewById(R.id.ordersRecyclerView);
        currentDate = findViewById(R.id.currentDate);
        logoutButton = findViewById(R.id.logoutButton);
        notfoundImage = findViewById(R.id.notfoundImage);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        userName = findViewById(R.id.userName);
        actionBarProfileButton = findViewById(R.id.actionBarProfileButton);

        actionBarProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HistoryOrderActivity.this.finish();
            }
        });

        try{
            Realm realm = Realm.getInstance(MyApplication.realmConfig);
            User user = realm.where(User.class).findFirst();
            userName.setText("Сайн байна уу? Ажилтан "+user.getFirstName());
        }catch (Exception e){
            e.printStackTrace();
        }

        currentDate.setText(currentDateValue);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HistoryOrderActivity.this);

                prefs.edit().putBoolean(Constants.SHARED_PREFERENCE_IS_LOGIN, false).apply();

                Realm realm = Realm.getInstance(MyApplication.realmConfig);
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm bgRealm) {
                        bgRealm.where(User.class).findAll().deleteAllFromRealm();

                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        try {
                            HistoryOrderActivity.this.finish();
                            startActivity(new Intent(HistoryOrderActivity.this, LoginActivity.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        chooseDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarDialogFragment calendarDialogFragment = new CalendarDialogFragment(HistoryOrderActivity.this, currentDateValue, formattedDate);

                calendarDialogFragment.show(getSupportFragmentManager(), "");
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(false);
                new LoadActiveOrdersTask("Баталгаажуулах захиалга ачааллаж байна...",currentFormattedDate).execute();
            }
        });

        currentFormattedDate = formattedDate;
        new LoadActiveOrdersTask("Баталгаажуулах захиалга ачааллаж байна...",formattedDate).execute();
    }

    @Override
    public void onSelectDate(String date, String formatDate) {
        currentDate.setText(date);
        currentFormattedDate = formatDate;
        new LoadActiveOrdersTask("Идэвхитэй захиалга ачааллаж байна...",formatDate).execute();
    }

    class LoadActiveOrdersTask extends AsyncTask<String, String, String> {

        boolean isSuccess = false;
        String responseMessage = "";

        RequestController requestController = new RequestController();
        String response;

        LoadingDialogFragment loadingDialogFragment;

        String loadingTitle;
        String formatDate;

        ArrayList<ActiveOrder> activeOrders;

        public LoadActiveOrdersTask(String loadingTitle,String formatDate) {
            this.loadingTitle = loadingTitle;
            this.formatDate = formatDate;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            loadingDialogFragment = new LoadingDialogFragment(loadingTitle);
            loadingDialogFragment.show(getSupportFragmentManager(), "");
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                response = requestController.getHttpRequest(Constants.ORDERS_API_LIST+"?date="+formatDate);
//            if (response != null) {
                try {

                    isSuccess = true;

                    activeOrders = new ArrayList<>();


                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject resultJsonObject = jsonObject.getJSONObject("result");


                    JSONArray ordersJsonArray = resultJsonObject.getJSONArray("orders");


                    for (int i = 0; i < ordersJsonArray.length(); i++) {

                        JSONObject orderJsonObject = ordersJsonArray.getJSONObject(i);

                        ActiveOrder activeOrder = new ActiveOrder();

                        activeOrder.setId(orderJsonObject.has("id") ? orderJsonObject.getString("id") : "");
                        activeOrder.setOrderNumber(orderJsonObject.has("orderNumber") ? orderJsonObject.getString("orderNumber") : "");



                        String siteName = orderJsonObject.getJSONObject("site").getString("name");

                        if (siteName != null && !siteName.isEmpty()) {
                            activeOrder.setCastingLocation(siteName);
                        }

                        activeOrder.setTotalQuantity(orderJsonObject.has("orderQualityOfConcrete") ? orderJsonObject.getString("orderQualityOfConcrete") : "");
                        activeOrder.setCreatedDate(orderJsonObject.has("createdAt") ? orderJsonObject.getString("createdAt") : "");
                        activeOrder.setStatus(orderJsonObject.has("status") ? orderJsonObject.getString("status") : "");


                        activeOrder.setNameWhoGives(orderJsonObject.has("nameWhoGives") ? orderJsonObject.getString("nameWhoGives") : "");
                        activeOrder.setStatusWhoGives(orderJsonObject.has("statusWhoGives") ? orderJsonObject.getString("statusWhoGives") : "");
                        activeOrder.setContactNumber(orderJsonObject.has("contactNumber") ? orderJsonObject.getString("contactNumber") : "");
                        activeOrder.setStructureName(orderJsonObject.has("structureName") ? orderJsonObject.getString("structureName") : "");
                        activeOrder.setFloor(orderJsonObject.has("floor") ? orderJsonObject.getString("floor") : "");
                        activeOrder.setSiteEngineer(orderJsonObject.has("siteEngineer") ? orderJsonObject.getString("siteEngineer") : "");
                        activeOrder.setOrderQualityOfConcrete(orderJsonObject.has("orderQualityOfConcrete") ? orderJsonObject.getString("orderQualityOfConcrete") : "");
                        activeOrder.setDateOfCasting(orderJsonObject.has("dateOfCasting") ? orderJsonObject.getString("dateOfCasting") : "");
                        activeOrder.setStartTime(orderJsonObject.has("startTime") ? orderJsonObject.getString("startTime") : "");
                        activeOrder.setSlump(orderJsonObject.has("slump") ? orderJsonObject.getString("slump") : "");
                        activeOrder.setCastingMethod(orderJsonObject.has("castingMethod") ? orderJsonObject.getString("castingMethod") : "");
                        activeOrder.setNameOfCastingLocation(orderJsonObject.has("nameOfCastingLocation") ? orderJsonObject.getString("nameOfCastingLocation") : "");
                        activeOrder.setAdditionalInfo(orderJsonObject.has("comment") ? orderJsonObject.getString("comment") : "");
                        activeOrder.setHseAdvice(orderJsonObject.has("hseAdvice") ? orderJsonObject.getString("hseAdvice") : "");

                        activeOrder.setApprovedAt(orderJsonObject.has("approvedAt") ? orderJsonObject.getString("approvedAt") : "");
                        activeOrder.setCanceledAt(orderJsonObject.has("canceledAt") ? orderJsonObject.getString("canceledAt") : "");



                        activeOrder.setCanceledReason(orderJsonObject.has("canceledReason") ? orderJsonObject.getString("canceledReason") : "");



                        try {

                            String concreteMark = orderJsonObject.getJSONObject("concreteMixDesign").getJSONObject("mixDesign").getString("name");

                            activeOrder.setConcreteClass(concreteMark);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        try {

                            String siteLat = orderJsonObject.getJSONObject("site").getString("addressLat");
                            String siteLon = orderJsonObject.getJSONObject("site").getString("addressLng");

                            activeOrder.setSiteLat(siteLat);
                            activeOrder.setSiteLon(siteLon);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try{
                            String clientName = orderJsonObject.getJSONObject("contract").getJSONObject("company").getString("displayName");
                            activeOrder.setClientName(clientName);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try{
                            String projectName = orderJsonObject.getJSONObject("contract").getJSONObject("project").getString("nameMn");
                            activeOrder.setProjectName(projectName);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        activeOrders.add(activeOrder);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                loadingDialogFragment.dismiss();


                ArrayList<ActiveOrder> waitingOrders = new ArrayList<>();
                ArrayList<ActiveOrder> approvedOrders = new ArrayList<>();
                ArrayList<ActiveOrder> canceledOrders = new ArrayList<>();
                ArrayList<ActiveOrder> doneActiveOrders = new ArrayList<>();
                ArrayList<ActiveOrder> ongoingActiveOrders = new ArrayList<>();


                if (isSuccess) {

                    if (activeOrders.size() > 0) {


                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistoryOrderActivity.this);
                        ordersRecyclerView.setLayoutManager(layoutManager);
                        notfoundImage.setVisibility(View.GONE);
                    }else {
                        notfoundImage.setVisibility(View.VISIBLE);
                    }

                    ordersRecyclerView.setAdapter(new RecyclerViewAdapter(HistoryOrderActivity.this, activeOrders));


                } else {
                    AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Амжилтгүй", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
                    alertDialogFragment.show(getSupportFragmentManager(), "");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        private Context context;
        private ArrayList<ActiveOrder> activeOrders;

        public RecyclerViewAdapter(Context context, ArrayList<ActiveOrder> activeOrders) {
            this.activeOrders = activeOrders;
            this.context = context;
        }

        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_order_history_value, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolders holder, int position) {

            final ActiveOrder data = activeOrders.get(position);

            if (data != null) {

                holder.orderIdText.setText("Захиалга #" + data.getOrderNumber());
                holder.quantityText.setText("(" + data.getTotalQuantity() + " М3)");
                holder.siteText.setText(data.getCastingLocation());

                try {

                    String date = data.getCreatedDate();
                    String startDate = data.getStartTime();
                    SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


                    Date newDate = spf.parse(date);
                    Date newStartDate = spf.parse(startDate);
                    spf = new SimpleDateFormat("yyyy-MM-dd");
                    date = spf.format(newDate);


                    startDate = spf.format(newStartDate);

                    holder.createdDate.setText(date);
                    holder.createdDate.setVisibility(View.GONE);
                    holder.createdDateText.setText("Захиалга үүсгэсэн огноо : "+date);
                    holder.startDateText.setText("Захиалга эхлэх хугацаа : "+startDate);



                    if (data.getStatus().equals("approving")) {
                        holder.statusText.setText("Батлагдаж байгаа");
                    } else if (data.getStatus().equals("approved")){
                        holder.statusText.setText("Батлагдсан");
                    }else if (data.getStatus().equals("delivering")){
                        holder.statusText.setText("Хүргэгдэж байгаа");
                    }else if (data.getStatus().equals("delivered")){
                        holder.statusText.setText("Хүргэгдсэн");
                    }else if (data.getStatus().equals("finished")){
                        holder.statusText.setText("Дууссан");
                    }else if (data.getStatus().equals("canceled")){
                        holder.statusText.setText("Цуцлагдсан");
                    }else if (data.getStatus().equals("hold")){
                        holder.statusText.setText("Хойшлогдсон");
                    }else if (data.getStatus().equals("waiting")){
                        holder.statusText.setText("Хүлээгдэж байгаа");
                    }


                    holder.deliveryButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(HistoryOrderActivity.this,OrderResultActivity.class);

                            intent.putExtra(Constants.INTENT_ORDER_ID,data.getId());


                            String advice = "";


                            if(!data.getCanceledReason().isEmpty() && !data.getCanceledReason().equals("null")){
                                advice = data.getCanceledReason();
                            }else if(!data.getHseAdvice().isEmpty() && !data.getHseAdvice().equals("null")){
                                advice = data.getHseAdvice();
                            }

                            Log.e("advice",advice);

                            intent.putExtra(Constants.INTENT_ORDER_ADVICE,advice);
                            intent.putExtra(Constants.INTENT_ORDER_APPROVEDAT,data.getApprovedAt());
                            intent.putExtra(Constants.INTENT_ORDER_CANCALEDAT,data.getCanceledAt());



                            startActivity(intent);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {

            return activeOrders.size();
        }


    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView orderIdText, quantityText, createdDate, siteText,statusText,startDateText,createdDateText;

        carbon.widget.FrameLayout deliveryButton;

        ImageView circleImageView;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            orderIdText = itemView.findViewById(R.id.orderIdText);
            quantityText = itemView.findViewById(R.id.quantityText);
            createdDate = itemView.findViewById(R.id.createdDate);
            siteText = itemView.findViewById(R.id.siteText);
            deliveryButton = itemView.findViewById(R.id.deliveryButton);
            circleImageView = itemView.findViewById(R.id.circleImageView);
            statusText = itemView.findViewById(R.id.statusText);
            startDateText = itemView.findViewById(R.id.startDateText);
            createdDateText = itemView.findViewById(R.id.createdDateText);

        }

        @Override
        public void onClick(View view) {

            Log.e("click", "click");
        }
    }

}
