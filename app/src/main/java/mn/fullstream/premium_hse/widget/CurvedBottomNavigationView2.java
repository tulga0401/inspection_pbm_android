package mn.fullstream.premium_hse.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.core.content.ContextCompat;
import mn.fullstream.premium_hse.R;

public class CurvedBottomNavigationView2 extends BottomNavigationView {
    private Path mPath;
    private Paint mPaint;

    /** the CURVE_CIRCLE_RADIUS represent the radius of the fab button */




    // the coordinates of the first curve
    private Point mFirstCurveStartPoint = new Point();
    private Point mFirstCurveEndPoint = new Point();
    private Point mFirstCurveControlPoint1 = new Point();
    private Point mFirstCurveControlPoint2 = new Point();

    //the coordinates of the second curve
    @SuppressWarnings("FieldCanBeLocal")
    private Point mSecondCurveStartPoint = new Point();
    private Point mSecondCurveEndPoint = new Point();
    private Point mSecondCurveControlPoint1 = new Point();
    private Point mSecondCurveControlPoint2 = new Point();


	private Point mThirdCurveStartPoint = new Point();
	private Point mThirdCurveEndPoint = new Point();
	private Point mThirdCurveControlPoint1 = new Point();
	private Point mThirdCurveControlPoint2 = new Point();


	private Point mFourthCurveEndPoint = new Point();
	private Point mFourthCurveControlPoint1 = new Point();
	private Point mFourthCurveControlPoint2 = new Point();


	private Point mFifthCurveEndPoint = new Point();
	private Point mFifthCurveControlPoint1 = new Point();
	private Point mFifthCurveControlPoint2 = new Point();

	private Point mSixthCurveStartPoint = new Point();
	private Point mSixthCurveEndPoint = new Point();
	private Point mSixthCurveControlPoint1 = new Point();
	private Point mSixthCurveControlPoint2 = new Point();





	private int mNavigationBarWidth;
    private int mNavigationBarHeight;

    public CurvedBottomNavigationView2(Context context) {
        super(context);
        init();
    }

    public CurvedBottomNavigationView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CurvedBottomNavigationView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryWhite));


        setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // get width and height of navigation bar
        // Navigation bar bounds (width & height)
        mNavigationBarWidth = getWidth();
        mNavigationBarHeight = getHeight();


		int CURVE_CIRCLE_RADIUS = getResources().getDimensionPixelOffset(R.dimen.carbon_fabSize);




		int firstPoint1X = getResources().getDimensionPixelOffset(R.dimen.firstPointX);
		int firstPoint1Y = getResources().getDimensionPixelOffset(R.dimen.firstPointY);
		int firstControl1X = getResources().getDimensionPixelOffset(R.dimen.firstControlx);
		int firstControl1Y = getResources().getDimensionPixelOffset(R.dimen.firstControly);

		int firstPoint2X = getResources().getDimensionPixelOffset(R.dimen.firstPoint2X);
		int firstPoint2Y = getResources().getDimensionPixelOffset(R.dimen.firstPoint2Y);
		int firstControl2X = getResources().getDimensionPixelOffset(R.dimen.firstControl2X);
		int firstControl2Y = getResources().getDimensionPixelOffset(R.dimen.firstControl2Y);




		mFirstCurveStartPoint.set(firstPoint1X,firstPoint1Y);
		mFirstCurveEndPoint.set(firstPoint2X,firstPoint2Y);

		mFirstCurveControlPoint1.set(firstControl1X,firstControl1Y);
		mFirstCurveControlPoint2.set(firstControl2X,firstControl2Y);




		int secondPoint1X = getWidth()/2-getResources().getDimensionPixelOffset(R.dimen.seconPoint1X_center_half);
		int secondPoint1Y = getResources().getDimensionPixelOffset(R.dimen.seconPoint1Y_center_half);
		int secondControl1X = getWidth()/2-getResources().getDimensionPixelOffset(R.dimen.secondControl1X_center_half);
		int secondControl1Y = getResources().getDimensionPixelOffset(R.dimen.secondControl1Y_center_half);

		int secondPoint2X =getWidth()/2- getResources().getDimensionPixelOffset(R.dimen.seconPoint2X_center_half);
		int secondPoint2Y = getResources().getDimensionPixelOffset(R.dimen.seconPoint2Y_center_half);

		int secondControl2X = getWidth()/2 - getResources().getDimensionPixelOffset(R.dimen.secondControl2X_center_half);
		int secondControl2Y = getResources().getDimensionPixelOffset(R.dimen.secondControl2Y_center_half);


		mSecondCurveStartPoint.set(secondPoint1X,secondPoint1Y);
		mSecondCurveEndPoint.set(secondPoint2X,secondPoint2Y);

		mSecondCurveControlPoint1.set(secondControl1X,secondControl1Y);
		mSecondCurveControlPoint2.set(secondControl2X,secondControl2Y);




		int thirdControl1X = getWidth()/2-getResources().getDimensionPixelOffset(R.dimen.thirdControl1X);
		int thirdControl1Y = getResources().getDimensionPixelOffset(R.dimen.thirdControl1Y);

		int thirdControl2X = getWidth()/2 - getResources().getDimensionPixelOffset(R.dimen.thirdControl2X);
		int thirdControl2Y = getResources().getDimensionPixelOffset(R.dimen.thirdControl2Y);

		int thirdPoint2X =getWidth()/2-getResources().getDimensionPixelOffset(R.dimen.thirdPoint2X);
		int thirdPoint2Y = getResources().getDimensionPixelOffset(R.dimen.thirdPoint2Y);


		mThirdCurveControlPoint1.set(thirdControl1X,thirdControl1Y);
		mThirdCurveControlPoint2.set(thirdControl2X,thirdControl2Y);
		mThirdCurveEndPoint.set(thirdPoint2X,thirdPoint2Y);




		int fourthControl2X = getWidth()/2+getResources().getDimensionPixelOffset(R.dimen.thirdControl1X);
		int fourthControl2Y = getResources().getDimensionPixelOffset(R.dimen.thirdControl1Y);

		int fourthControl1X = getWidth()/2 + getResources().getDimensionPixelOffset(R.dimen.thirdControl2X);
		int fourthControl1Y = getResources().getDimensionPixelOffset(R.dimen.thirdControl2Y);

		int fourthPoint2X = getWidth()/2 + getResources().getDimensionPixelOffset(R.dimen.secondControl2X_center_half);
		int fourthPoint2Y = getResources().getDimensionPixelOffset(R.dimen.secondControl2Y_center_half);

		mFourthCurveControlPoint1.set(fourthControl1X,fourthControl1Y);
		mFourthCurveControlPoint2.set(fourthControl2X,fourthControl2Y);
		mFourthCurveEndPoint.set(fourthPoint2X,fourthPoint2Y);






		int fifthPoint1X = getWidth()/2+getResources().getDimensionPixelOffset(R.dimen.seconPoint1X_center_half);
		int fifthPoint1Y = getResources().getDimensionPixelOffset(R.dimen.seconPoint1Y_center_half);

		int fifthControl2X = getWidth()/2+getResources().getDimensionPixelOffset(R.dimen.secondControl1X_center_half);
		int fifthControl2Y = getResources().getDimensionPixelOffset(R.dimen.secondControl1Y_center_half);

		int fifthControl1X = getWidth()/2 + getResources().getDimensionPixelOffset(R.dimen.secondControl2X_center_half);
		int fifthControl1Y = getResources().getDimensionPixelOffset(R.dimen.secondControl2Y_center_half);


		mFifthCurveControlPoint1.set(fifthControl1X,fifthControl1Y);
		mFifthCurveControlPoint2.set(fifthControl2X,fifthControl2Y);
		mFifthCurveEndPoint.set(fifthPoint1X,fifthPoint1Y);





		int sixthPoint2X = getWidth()-getResources().getDimensionPixelOffset(R.dimen.firstPointX);
		int sixthPoint2Y = getResources().getDimensionPixelOffset(R.dimen.firstPointY);
		int sixthControl2X = getWidth()-getResources().getDimensionPixelOffset(R.dimen.firstControlx);
		int sixthControl2Y = getResources().getDimensionPixelOffset(R.dimen.firstControly);

		int sixthPoint1X = getWidth()- getResources().getDimensionPixelOffset(R.dimen.firstPoint2X);
		int sixthPoint1Y = getResources().getDimensionPixelOffset(R.dimen.firstPoint2Y);
		int sixthControl1X = getWidth()-getResources().getDimensionPixelOffset(R.dimen.firstControl2X);
		int sixthControl1Y = getResources().getDimensionPixelOffset(R.dimen.firstControl2Y);




		mSixthCurveStartPoint.set(sixthPoint1X,sixthPoint1Y);
		mSixthCurveEndPoint.set(sixthPoint2X,sixthPoint2Y);

		mSixthCurveControlPoint1.set(sixthControl1X,sixthControl1Y);
		mSixthCurveControlPoint2.set(sixthControl2X,sixthControl2Y);




		mPath.reset();



        mPath.moveTo(0, mFirstCurveStartPoint.y);
        mPath.lineTo(mFirstCurveStartPoint.x, mFirstCurveStartPoint.y);
        mPath.cubicTo(mFirstCurveControlPoint1.x, mFirstCurveControlPoint1.y,
                mFirstCurveControlPoint2.x, mFirstCurveControlPoint2.y,
                mFirstCurveEndPoint.x, mFirstCurveEndPoint.y);

		mPath.lineTo(mSecondCurveStartPoint.x, mSecondCurveStartPoint.y);

        mPath.cubicTo(mSecondCurveControlPoint1.x, mSecondCurveControlPoint1.y,
                mSecondCurveControlPoint2.x, mSecondCurveControlPoint2.y,
                mSecondCurveEndPoint.x, mSecondCurveEndPoint.y);

		mPath.cubicTo(mThirdCurveControlPoint1.x, mThirdCurveControlPoint1.y,
			mThirdCurveControlPoint2.x, mThirdCurveControlPoint2.y,
			mThirdCurveEndPoint.x, mThirdCurveEndPoint.y);

		mPath.cubicTo(mFourthCurveControlPoint1.x, mFourthCurveControlPoint1.y,
			mFourthCurveControlPoint2.x, mFourthCurveControlPoint2.y,
			mFourthCurveEndPoint.x, mFourthCurveEndPoint.y);

		mPath.cubicTo(mFifthCurveControlPoint1.x, mFifthCurveControlPoint1.y,
			mFifthCurveControlPoint2.x, mFifthCurveControlPoint2.y,
			mFifthCurveEndPoint.x, mFifthCurveEndPoint.y);

		mPath.lineTo(mSixthCurveStartPoint.x, mSixthCurveStartPoint.y);

		mPath.cubicTo(mSixthCurveControlPoint1.x, mSixthCurveControlPoint1.y,
			mSixthCurveControlPoint2.x, mSixthCurveControlPoint2.y,
			mSixthCurveEndPoint.x, mSixthCurveEndPoint.y);

		mPath.lineTo(mNavigationBarWidth, 0);
        mPath.lineTo(mNavigationBarWidth, mNavigationBarHeight);
        mPath.lineTo(0, mNavigationBarHeight);
        mPath.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(mPath, mPaint);
    }


}
