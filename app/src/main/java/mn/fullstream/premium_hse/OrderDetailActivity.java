package mn.fullstream.premium_hse;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;


import mn.fullstream.premium_hse.dialogs.AlertDialogFragment;
import mn.fullstream.premium_hse.dialogs.LoadingDialogFragment;
import mn.fullstream.premium_hse.interfaces.OnAdviceListener;
import mn.fullstream.premium_hse.model.ActiveOrder;
import mn.fullstream.premium_hse.model.ConfirmOrder;
import mn.fullstream.premium_hse.util.RequestController;

public class OrderDetailActivity extends AppCompatActivity implements OnAdviceListener {

	RecyclerView confirmRecyclerView;

	FrameLayout okbutton, cancelButton;
	TextView buttonText;

	ActiveOrder order;

	LinearLayout canceledContainer;

	carbon.widget.FrameLayout backButton;

	TextView cancelReasonText, orderNumberText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_detail);


		backButton = findViewById(R.id.backButton);

		orderNumberText = findViewById(R.id.orderNumberText);


		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				OrderDetailActivity.this.finish();
			}
		});


		confirmRecyclerView = findViewById(R.id.confirmRecyclerView);
		okbutton = findViewById(R.id.okbutton);
		cancelButton = findViewById(R.id.cancelButton);
		buttonText = findViewById(R.id.buttonText);

		order = getIntent().getParcelableExtra(Constants.INTENT_ORDER_HISTORY_DETAIL);

		if (order != null) {
			wrapData();

			orderNumberText.setText("#" + order.getOrderNumber());


		}

	}

	@Override
	public void onAdviceAdded(String advice) {
		new ApproveOrderTask("Баталгаажуулж байна...", order.getId(), advice).execute();
	}


	class ApproveOrderTask extends AsyncTask<String, String, String> {

		LoadingDialogFragment loadingDialogFragment;


		String loadingTitle = "";

		boolean isSuccess = false;
		String responseMessage = "";

		String advice = "";
		String orderId = "";

		public ApproveOrderTask(String loadingTitle, String orderId, String advice) {
			this.loadingTitle = loadingTitle;
			this.orderId = orderId;
			this.advice = advice;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			loadingDialogFragment = new LoadingDialogFragment(loadingTitle);
			loadingDialogFragment.show(getSupportFragmentManager(), "");

		}

		@Override
		protected String doInBackground(String... strings) {


			RequestController requestController = new RequestController();


			try {
				String adviceEncoded = URLEncoder.encode(advice, "UTF-8");

				String response = requestController.postHttpRequest(Constants.APPROVE_ORDER_API.replace("{id}", orderId) + "?advice=" + adviceEncoded);


				Log.e("response", response);


				JSONObject resultJson = new JSONObject(response);

				if (resultJson != null) {
					try {
						String status = "";
						status = resultJson.getString("status");

						if (status.equals("success")) {

							isSuccess = true;
						}


						String message = resultJson.getString("message");

						responseMessage = message;

					} catch (Exception e) {
						e.printStackTrace();

						isSuccess = false;

						responseMessage = e.getMessage();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			loadingDialogFragment.dismiss();


			if (isSuccess) {


				OrderDetailActivity.this.finish();

				Intent i = new Intent(OrderDetailActivity.this, MainActivity.class); // Your list's Intent
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Adds the FLAG_ACTIVITY_NO_HISTORY flag

				startActivity(i);

			} else {
				AlertDialogFragment alertDialogFragment = new AlertDialogFragment("Баталгаажуулахад алдаа гарлаа", responseMessage, AlertDialogFragment.ALERT_TYPE_FAIL);
				alertDialogFragment.show(getSupportFragmentManager(), "");
			}
		}
	}

	void wrapData() {
		ArrayList<ConfirmOrder> confirmOrders = new ArrayList<>();

		confirmOrders.add(confirmOrder(getString(R.string.client_name), order.getClientName()));
		confirmOrders.add(confirmOrder(getString(R.string.project_name), order.getProjectName()));
		confirmOrders.add(confirmOrder(getString(R.string.name_who_gives), order.getNameWhoGives()));
		confirmOrders.add(confirmOrder(getString(R.string.status_who_gives), order.getStatusWhoGives()));
		confirmOrders.add(confirmOrder(getString(R.string.contact_number), order.getContactNumber()));
		confirmOrders.add(confirmOrder(getString(R.string.name_of_casting_location), order.getCastingLocation()));
		confirmOrders.add(confirmOrder(getString(R.string.structure_name), order.getStructureName()));
		confirmOrders.add(confirmOrder(getString(R.string.site_engineer), order.getSiteEngineer()));


		try {
			String date = order.getStartTime();
			String time = order.getStartTime();
			SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			SimpleDateFormat spfTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


			Date newDate = spf.parse(date);
			spf = new SimpleDateFormat("yyyy-MM-dd");
			date = spf.format(newDate);


			confirmOrders.add(confirmOrder(getString(R.string.date_of_casting), date));


			Date newTime = spfTime.parse(time);
			spfTime = new SimpleDateFormat("HH:mm");
			time = spfTime.format(newTime);

			confirmOrders.add(confirmOrder(getString(R.string.start_time), time));

		} catch (Exception e) {
			e.printStackTrace();
		}


		confirmOrders.add(confirmOrder(getString(R.string.order_quality_of_concrete), order.getOrderQualityOfConcrete()));
		confirmOrders.add(confirmOrder(getString(R.string.concrete_class), order.getConcreteClass()));
		confirmOrders.add(confirmOrder(getString(R.string.slump), order.getSlump()));


		confirmOrders.add(confirmOrder(getString(R.string.casting_method), order.getCastingMethod()));


		if (order.getMorter() != null && !order.getMorter().isEmpty()) {
			confirmOrders.add(confirmOrder(getString(R.string.value_of_morter), order.getMorter()));
		}
		confirmOrders.add(confirmOrder(getString(R.string.additional_info), order.getAdditionalInfo()));

		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderDetailActivity.this);
		confirmRecyclerView.setHasFixedSize(false);
		confirmRecyclerView.setLayoutManager(layoutManager);
		confirmRecyclerView.setAdapter(new RecyclerViewAdapter(OrderDetailActivity.this, confirmOrders));

	}

	ConfirmOrder confirmOrder(String title, String value) {
		ConfirmOrder confirmOrder = new ConfirmOrder();
		confirmOrder.setTitle(title);
		confirmOrder.setValue(value);

		return confirmOrder;

	}

	class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

		private Context context;
		private ArrayList<ConfirmOrder> confirmOrders;

		public RecyclerViewAdapter(Context context, ArrayList<ConfirmOrder> confirmOrders) {
			this.confirmOrders = confirmOrders;
			this.context = context;
		}

		@Override
		public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
			View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_order_confirm, parent, false);
			RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
			return rcv;
		}

		@Override
		public void onBindViewHolder(RecyclerViewHolders holder, int position) {

			final ConfirmOrder data = confirmOrders.get(position);

			if (data != null) {
				try {

					holder.title.setText(data.getTitle());

					if (data.getValue() == null || data.getValue().isEmpty()) {
						holder.value.setText(getString(R.string.not_inserted));
						holder.value.setTextColor(getResources().getColor(R.color.colorDeepGrey));
					} else {
						holder.value.setText(data.getValue());
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public int getItemCount() {

			return confirmOrders.size();
		}


	}

	public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
		TextView value, title;


		public RecyclerViewHolders(View itemView) {
			super(itemView);

			value = (TextView) itemView.findViewById(R.id.value);
			title = (TextView) itemView.findViewById(R.id.title);


		}

		@Override
		public void onClick(View view) {

			Log.e("click", "click");
		}
	}


}
