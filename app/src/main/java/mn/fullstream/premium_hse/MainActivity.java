package mn.fullstream.premium_hse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.firebase.messaging.FirebaseMessaging;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import carbon.widget.FrameLayout;
import io.realm.Realm;
import mn.fullstream.premium_hse.interfaces.OnSelectDateListener;
import mn.fullstream.premium_hse.main.home.HomeFragment;
import mn.fullstream.premium_hse.main.interfaces.OnBottomChangeListener;
import mn.fullstream.premium_hse.main.my.MyFragment;
import mn.fullstream.premium_hse.main.report.ReportFragment;
import mn.fullstream.premium_hse.main.task.TaskFragment;
import mn.fullstream.premium_hse.model.Attribute;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.notification.NotificationsActivity;
import mn.fullstream.premium_hse.util.RequestController;

public class MainActivity extends AppCompatActivity implements OnSelectDateListener, OnBottomChangeListener {


	ViewPager viewPager;

	FrameLayout homeButton,reportButton,taskButton,myButton;

	TextView title;

	FrameLayout notificationButton;

	FrameLayout countNotificationContainer;
	TextView countNotificationText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		FirebaseMessaging.getInstance().getToken()
				.addOnCompleteListener(task -> {
					if (!task.isSuccessful()) {
						return;
					}
					sendRegistrationToServer(task.getResult());
				});

		homeButton = findViewById(R.id.homeButton);
		reportButton = findViewById(R.id.reportButton);
		taskButton = findViewById(R.id.taskButton);
		myButton = findViewById(R.id.myButton);
		title = findViewById(R.id.title);
		viewPager = findViewById(R.id.viewPager);
		notificationButton = findViewById(R.id.notificationButton);
		countNotificationContainer = findViewById(R.id.countNotificationContainer);
		countNotificationText = findViewById(R.id.countNotificationText);


		Date c = Calendar.getInstance().getTime();

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		final String formattedDate = df.format(c);


		Log.e("formatted", formattedDate);


		SimpleDateFormat currentDf = new SimpleDateFormat("MM сарын dd");
		final String currentDateValue = currentDf.format(c);


		loadUi();
	}

	void loadUi(){


		SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(sectionsPagerAdapter);
		viewPager.setOffscreenPageLimit(5);
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {

				selectMenu(position);

			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});


		notificationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,NotificationsActivity.class);
				startActivity(intent);

			}
		});

		setNavigationInit();
	}

	@Override
	public void onChangePage(int position) {
		viewPager.setCurrentItem(position);
	}


	public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {

			Fragment fragment = null;


			if (position == 0){
				fragment = new HomeFragment(MainActivity.this::onChangePage);
			}else if (position == 1){
				fragment = new ReportFragment();
			}else if (position == 2){
				fragment = new TaskFragment();
			}else if (position == 3){
				fragment = new MyFragment();
			}

			return fragment;

		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "";
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {

		}
	}



	void setNavigationInit() {



		myButton = findViewById(R.id.myButton);
		homeButton = findViewById(R.id.homeButton);
		reportButton = findViewById(R.id.reportButton);
		myButton = findViewById(R.id.myButton);


		myButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadFragment(3);
			}
		});

		homeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadFragment(0);
			}
		});

		reportButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadFragment(1);
			}
		});

		taskButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadFragment(2);
			}
		});

		selectMenu(0);


	}

	void selectMenu(int position) {


		ImageView myIconImageView, promoIconImageView, searchIconImageView, moreIconImageView;
		TextView myTextView, promoText, searchText, moreText;

		myIconImageView = findViewById(R.id.myIconImageView);
		promoIconImageView = findViewById(R.id.promoIconImageView);
		searchIconImageView = findViewById(R.id.searchIconImageView);
		moreIconImageView = findViewById(R.id.moreIconImageView);


		myTextView = findViewById(R.id.myTextView);
		promoText = findViewById(R.id.promoText);
		searchText = findViewById(R.id.searchText);
		moreText = findViewById(R.id.moreText);


		homeButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryWhite));
		reportButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryWhite));
		taskButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryWhite));
		myButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryWhite));


		myTextView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		promoText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		searchText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		moreText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColor));

		myIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		promoIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		searchIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textColor));
		moreIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.textColor));

		if (position==0) {
			title.setText("Эхлэл");

			homeButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.lightGrey));
			myTextView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
			myIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
		} else if (position==1) {
			title.setText("Тайлан");
			reportButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.lightGrey));
			promoText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
			promoIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
		} else if (position==2) {
			title.setText("Ажил");
			taskButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.lightGrey));
			searchText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
			searchIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));

		} else if (position==3) {
			title.setText("Миний");
			myButton.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.lightGrey));
			moreText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
			moreIconImageView.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
		}

	}



	private boolean loadFragment(int position) {
		viewPager.setCurrentItem(position,true);

		return true;
	}

	@Override
	public void onSelectDate(String date, String formatDate) {

//		new LoadActiveOrdersTask("Идэвхитэй захиалга ачааллаж байна...", formatDate).execute();
	}

	@Override
	protected void onResume() {
		super.onResume();

		new LoadNotificationsTask().execute();
	}

	class LoadNotificationsTask extends AsyncTask<String, String, String> {

		boolean isSuccess = false;
		String responseMessage = "";

		RequestController requestController = new RequestController();
		String response;

		String count = "";

		ArrayList<Attribute> attributes = new ArrayList<>();

		public LoadNotificationsTask() {

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();


		}

		@Override
		protected String doInBackground(String... params) {

			try {

				Realm realm = Realm.getInstance(MyApplication.realmConfig);
				realm.refresh();
				User user = realm.where(User.class).findFirst();

				response = requestController.getHttpRequest(Constants.API_UNSEEN_NOTIFICATIONS.replace("{userId}",user.getId()));



				try {

					JSONObject jsonObject = new JSONObject(response);


					count = jsonObject.has("count")?jsonObject.getString("count"):"0";



				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {


				if (count.isEmpty() || count.equals("0")){
					countNotificationContainer.setVisibility(View.GONE);
				}else {
					countNotificationContainer.setVisibility(View.VISIBLE);
					countNotificationText.setText(count);
				}


			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void sendRegistrationToServer(String token) {
		String appVersion = String.valueOf(getAppVersion(getApplicationContext()));
		String deviceName = Build.MODEL;
		String deviceVersion = Build.VERSION.SDK_INT + "";

		new RegisterNotificationTask(token, deviceName, deviceVersion, appVersion).execute();
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo =
					context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	@SuppressLint("StaticFieldLeak")
	public static class RegisterNotificationTask extends AsyncTask<String, String, String> {
		String deviceToken;
		String deviceName;
		String deviceVersion;
		String appVersion;
		boolean isSuccess = false;
		RequestController requestController = new RequestController();

		public RegisterNotificationTask(String deviceToken, String deviceName, String deviceVersion, String appVersion) {
			this.deviceToken = deviceToken;
			this.deviceName = deviceName;
			this.deviceVersion = deviceVersion;
			this.appVersion = appVersion;
		}

		@Override
		protected String doInBackground(String... strings) {
			String response = requestController.postRegisterHttpRequest(deviceToken, deviceName, deviceVersion, appVersion);

			try {
				JSONObject data = new JSONObject(Objects.requireNonNull(response));
				Log.e("DR Result:", data.toString());
				if (data.getString("status").equals("success")) {
					isSuccess = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);
			if (isSuccess) {
				Log.e("success", "success");
			} else {
				Log.e("error", "error");
			}
		}
	}
}
