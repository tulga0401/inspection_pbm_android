package mn.fullstream.premium_hse.interfaces;

import java.util.ArrayList;

import mn.fullstream.premium_hse.model.Issue;

public interface OnAddIssueDescListener {
    void onAddDescIssue(ArrayList<Issue> issues);
}
