package mn.fullstream.premium_hse.interfaces;

public interface OnChangePageListener {

	void onChangePage(int position);
}
