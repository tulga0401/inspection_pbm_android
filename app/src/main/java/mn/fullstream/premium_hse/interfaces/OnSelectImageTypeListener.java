package mn.fullstream.premium_hse.interfaces;

import androidx.recyclerview.widget.RecyclerView;
import mn.fullstream.premium_hse.main.task.InspectActivity;
import mn.fullstream.premium_hse.model.InspectAttribue;

public interface OnSelectImageTypeListener {
    void onSelectImageType(InspectAttribue inspectAttribue, boolean isGallery, InspectActivity.RecyclerViewAdapter recyclerViewAdapter, RecyclerView recyclerView);
}
