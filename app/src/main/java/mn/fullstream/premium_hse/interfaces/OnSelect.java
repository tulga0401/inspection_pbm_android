package mn.fullstream.premium_hse.interfaces;

import android.widget.TextView;

public interface OnSelect {
    void onSelect(String attributeId, String selectedValue,String selectedId, TextView selectedTextView);
}
