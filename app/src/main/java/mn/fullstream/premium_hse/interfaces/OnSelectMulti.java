package mn.fullstream.premium_hse.interfaces;

import android.widget.TextView;

import java.util.ArrayList;

import mn.fullstream.premium_hse.model.Option;

public interface OnSelectMulti {
    void onSelectMulti(String attributeId, ArrayList<Option> selectedOptions, TextView selectedTextView);
}
