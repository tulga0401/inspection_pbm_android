package mn.fullstream.premium_hse.interfaces;

public interface OnAdviceListener {
    void onAdviceAdded(String advice);
}
