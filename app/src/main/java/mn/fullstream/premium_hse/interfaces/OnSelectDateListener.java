package mn.fullstream.premium_hse.interfaces;

public interface OnSelectDateListener {
    void onSelectDate(String date,String formatDate);
}
