package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnSelectDateListener;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class CalendarDialogFragment extends DialogFragment {

    TextView title;

    FrameLayout closeButton;
    FrameLayout okButton;

//    MonthCalendarView mcvCalendar;

    OnSelectDateListener onSelectDateListener;
    String currentDate;
    String formatCurrentDate;

    @SuppressLint("ValidFragment")
    public CalendarDialogFragment(OnSelectDateListener onSelectDateListener,String currentDate,String formatCurrentDate) {
        // Required empty public constructor

        this.onSelectDateListener = onSelectDateListener;
        this.currentDate = currentDate;
        this.formatCurrentDate = formatCurrentDate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_calendar_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        title  = view.findViewById(R.id.title);
//        mcvCalendar  = view.findViewById(R.id.mcvCalendar);
//
//        closeButton = view.findViewById(R.id.closeButton);
//        okButton = view.findViewById(R.id.okButton);
//
//        title.setText(currentDate);
//
//
//        mcvCalendar.setOnCalendarClickListener(new OnCalendarClickListener() {
//            @Override
//            public void onClickDate(int year, int month, int day) {
//
//
////                title.setText(year+"-"+month+"-"+day);
//
//                currentDate = (++month) +" сарын "+day;
//
//                formatCurrentDate = year+"-"+(month)+"-"+day;
//
//                title.setText(currentDate);
//            }
//
//            @Override
//            public void onPageChange(int year, int month, int day) {
////                title.setText(year+"-"+month+"-"+day);
//                currentDate = (++month) +" сарын "+day;
//
//                formatCurrentDate = year+"-"+(month)+"-"+day;
//
//                title.setText(currentDate);
//            }
//        });
//
//
//        okButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onSelectDateListener.onSelectDate(currentDate,formatCurrentDate);
//                CalendarDialogFragment.this.dismiss();
//            }
//        });
//
//        dialog.setContentView(view);
//
//
//        closeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CalendarDialogFragment.this.dismiss();
//            }
//        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);


    }


}
