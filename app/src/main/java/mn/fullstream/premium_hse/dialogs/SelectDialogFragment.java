package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import carbon.widget.LinearLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnSelect;
import mn.fullstream.premium_hse.interfaces.OnSelectIssuesListener;
import mn.fullstream.premium_hse.model.Option;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SelectDialogFragment extends DialogFragment {

    TextView title;

    FrameLayout closeButton;

    String titleValue;
    int alertType;


    RecyclerView issueRecyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    OnSelectIssuesListener onSelectIssuesListener;

    int position;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {




        return super.onCreateView(inflater, container, savedInstanceState);
    }


	String attributeId;
    ArrayList<Option> options;
    OnSelect onSelect;
	TextView selectedTextView;

    public SelectDialogFragment(String attributeId, ArrayList<Option> options, OnSelect onSelect, TextView selectedTextView) {
        this.attributeId = attributeId;
        this.options = options;
        this.onSelect = onSelect;
        this.selectedTextView = selectedTextView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_select_issue_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
        dialog.getWindow().setLayout(width,px);


        dialog.setContentView(view);

        closeButton = view.findViewById(R.id.closeButton);


        issueRecyclerView = view.findViewById(R.id.issueRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        issueRecyclerView.setLayoutManager(layoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity());
        issueRecyclerView.setAdapter(recyclerViewAdapter);


        recyclerViewAdapter.notifyDataSetChanged();

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDialogFragment.this.dismiss();
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);
        getActivity().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        private Context context;

        public RecyclerViewAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_issue, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolders holder, @SuppressLint("RecyclerView") final int position) {


        	Option data = options.get(position);
            holder.optionText.setText(data.getName());
            holder.selectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					onSelect.onSelect(attributeId,data.getName(),data.getId(),selectedTextView);
					SelectDialogFragment.this.dismiss();

				}
			});
        }

        @Override
        public int getItemCount() {

            return options.size();
        }
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

		LinearLayout selectButton;
        TextView optionText;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

			selectButton = itemView.findViewById(R.id.selectButton);
			optionText = itemView.findViewById(R.id.optionText);
        }

        @Override
        public void onClick(View view) {

            Log.e("click", "click");
        }
    }


}
