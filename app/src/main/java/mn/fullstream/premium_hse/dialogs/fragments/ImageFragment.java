package mn.fullstream.premium_hse.dialogs.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import mn.fullstream.premium_hse.R;

public class ImageFragment extends Fragment {


	ImageView image;

	String img;

	public ImageFragment(String img) {
		// Required empty public constructor
		this.img = img;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_image, container, false);
	}


	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		image = view.findViewById(R.id.image);


		try {
			Picasso.get().load(img).into(image);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
