package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import mn.fullstream.premium_hse.R;


/**
 * A simple {@link Fragment} subclass.
 */


@SuppressLint("ValidFragment")
public class LoadingCounterDialogFragment extends DialogFragment {

    String titleValue;

    TextView title;

    @SuppressLint("ValidFragment")
    public LoadingCounterDialogFragment(String titleValue) {
        // Required empty public constructor
        this.titleValue = titleValue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_counter_loading_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        title  = view.findViewById(R.id.title);

        title.setText(titleValue);

        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);


        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);

    }

    public void uploadImageDesc(String desc){

		Log.e("title",title+" is null");
    	if (title!=null) {
			title.setText(desc);
			title.requestLayout();
		}
	}


}
