package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnSelectIssuesListener;
import mn.fullstream.premium_hse.model.Issue;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SearchTaskDialogFragment extends DialogFragment {

    FrameLayout closeButton,searchButton;

    LottieAnimationView image;

    EditText editText;

	OnSearchTaskListener onSearchTaskListener;


	public interface OnSearchTaskListener {
		void onSearch(String searchValue);
	}

	public void setOnSearchTaskListener(OnSearchTaskListener onSearchTaskListener) {

		this.onSearchTaskListener = onSearchTaskListener;
	}



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {




        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public SearchTaskDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_search_task_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
        dialog.getWindow().setLayout(width,px);


        dialog.setContentView(view);
        image = view.findViewById(R.id.image);
		editText = view.findViewById(R.id.editText);
        closeButton = view.findViewById(R.id.closeButton);
		searchButton = view.findViewById(R.id.searchButton);



		searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (onSearchTaskListener!=null){

					String searchText = editText.getText().toString();

					if (!searchText.isEmpty()){
						onSearchTaskListener.onSearch(searchText);

						SearchTaskDialogFragment.this.dismiss();
					}else {
						editText.setError("Захиалгын дугаар хоосон байна.");
					}
				}
			}
		});


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchTaskDialogFragment.this.dismiss();
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);
        getActivity().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


    }


}
