package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnSelectIssuesListener;
import mn.fullstream.premium_hse.model.Issue;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SelectIssueDialogFragment extends DialogFragment {

    FrameLayout closeButton;

    String titleValue;
    int alertType;

    LottieAnimationView image;

    ArrayList<Issue> issues;


    RecyclerView issueRecyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    OnSelectIssuesListener onSelectIssuesListener;

    int position;

    @SuppressLint("ValidFragment")
    public SelectIssueDialogFragment(ArrayList<Issue> issues, OnSelectIssuesListener onSelectIssuesListener) {
        // Required empty public constructor
        this.issues = issues;
        this.onSelectIssuesListener = onSelectIssuesListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {




        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public SelectIssueDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_select_issue_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
        dialog.getWindow().setLayout(width,px);


        dialog.setContentView(view);
        image = view.findViewById(R.id.image);
        closeButton = view.findViewById(R.id.closeButton);


        issueRecyclerView = view.findViewById(R.id.issueRecyclerView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        issueRecyclerView.setLayoutManager(layoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity());
        issueRecyclerView.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.notifyDataSetChanged();

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectIssueDialogFragment.this.dismiss();
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);
        getActivity().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


    }

    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        private Context context;

        public RecyclerViewAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_issue, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolders holder, @SuppressLint("RecyclerView") final int position) {

            Log.e(issues.get(position)+"",issues.get(position).isChild()+"");

            holder.title.setText(issues.get(position).getName());

            if (issues.get(position).isChecked()) {
                holder.checkCircle.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            } else {
                holder.checkCircle.setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
            }

            if (issues.get(position).isChild()) {

                holder.checkButton.setEnabled(true);
                holder.checkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (issues.get(position).isChecked()) {
                            issues.get(position).setChecked(false);
                        } else {
                            issues.get(position).setChecked(true);
                        }

                        onSelectIssuesListener.onSelectIssue(issues);

                        Log.e("checkButton",issues.get(position).getName());
                        RecyclerViewAdapter.this.notifyItemChanged(position);

                    }
                });

                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.checkButton.setVisibility(View.VISIBLE);

                holder.checkCircle.setVisibility(View.VISIBLE);
                holder.title.setTextColor(getResources().getColor(R.color.colorPrimary));
                holder.title.setTypeface(null);
            } else {

                holder.checkCircle.setVisibility(View.GONE);
                holder.checkButton.setEnabled(false);
                holder.circleImageView.setVisibility(View.INVISIBLE);
                holder.title.setTextColor(getResources().getColor(R.color.colorAccent));
                holder.title.setTypeface(holder.title.getTypeface(), Typeface.BOLD);

            }

        }

        @Override
        public int getItemCount() {

            return issues.size();
        }


    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView circleImageView;

        FrameLayout checkButton, checkCircle;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            circleImageView = itemView.findViewById(R.id.circleImageView);
            checkButton = itemView.findViewById(R.id.checkButton);


        }

        @Override
        public void onClick(View view) {

            Log.e("click", "click");
        }
    }


}
