package mn.fullstream.premium_hse.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;

/**
 * Simple fragment with blur effect behind.
 */

public class CustomResultDialog extends DialogFragment {


	LottieAnimationView lottieAnimationView;
	TextView title, desc;
	FrameLayout okButton;

	FrameLayout resultContainer;


	public static final int DIALOG_INFO = 0;
	public static final int DIALOG_ERROR = 1;
	public static final int DIALOG_WARNING = 2;
	public static final int DIALOG_SUCCESS_ = 3;


	int dialogType;

	String titleValue;
	String descValue;


	@SuppressLint("ValidFragment")
	public CustomResultDialog(int dialogType, String titleValue, String descValue) {
		this.dialogType = dialogType;
		this.titleValue = titleValue;
		this.descValue = descValue;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
		View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_result_custom, null);
		dialog.setContentView(view);

		lottieAnimationView = (LottieAnimationView) view.findViewById(R.id.lottieAnimationView);
		resultContainer = view.findViewById(R.id.resultContainer);


		if (dialogType == DIALOG_ERROR) {
			lottieAnimationView.setAnimation("alert_error.json");
		} else if (dialogType == DIALOG_WARNING) {
			lottieAnimationView.setAnimation("alert_warning.json");
		} else if (dialogType == DIALOG_SUCCESS_) {
			lottieAnimationView.setAnimation("alert_success.json");
		}


		title = view.findViewById(R.id.title);
		desc = view.findViewById(R.id.desc);
		okButton = view.findViewById(R.id.okButton);


		title.setText(titleValue);
		desc.setText(descValue);


		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CustomResultDialog.this.dismiss();
			}
		});

		return dialog;
	}

	@Override
	public void onPause() {
		super.onPause();
		this.dismiss();
	}
}

