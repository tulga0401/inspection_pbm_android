package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnAdviceListener;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AdviceDialogFragment extends DialogFragment {

    TextView title;

    FrameLayout closeButton,okButton;

    String titleValue;

    EditText adviceEditText;

    OnAdviceListener onAdviceListener;

    @SuppressLint("ValidFragment")
    public AdviceDialogFragment(OnAdviceListener onAdviceListener) {
        // Required empty public constructor

        this.onAdviceListener = onAdviceListener;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_advice_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        title  = view.findViewById(R.id.title);
        closeButton = view.findViewById(R.id.closeButton);
        adviceEditText = view.findViewById(R.id.adviceEditText);
        okButton = view.findViewById(R.id.okButton);

        title.setText("Зѳвлѳмж");





        dialog.setContentView(view);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdviceDialogFragment.this.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String reasonValue = adviceEditText.getText().toString();


                if (reasonValue.isEmpty()){

                    onAdviceListener.onAdviceAdded("Зѳвлѳмж байхгүй");

                }else {

                    onAdviceListener.onAdviceAdded(reasonValue);
                }
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);


    }


}
