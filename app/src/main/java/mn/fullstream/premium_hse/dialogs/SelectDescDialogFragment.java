package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnAddIssueDescListener;
import mn.fullstream.premium_hse.model.Issue;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SelectDescDialogFragment extends DialogFragment {

    TextView title;

    FrameLayout closeButton,okButton;

    String titleValue;
    int alertType;

    LottieAnimationView image;

    ArrayList<Issue> issues;
    int index;




    EditText descEditText;
    OnAddIssueDescListener onAddIssueDescListener;

    @SuppressLint("ValidFragment")
    public SelectDescDialogFragment(ArrayList<Issue> issues, int index,OnAddIssueDescListener onAddIssueDescListener) {
        // Required empty public constructor
        this.issues = issues;
        this.index = index;
        this.onAddIssueDescListener = onAddIssueDescListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {




        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public SelectDescDialogFragment(TextView title) {
        this.title = title;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_select_desc_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
        dialog.getWindow().setLayout(width,px);


        dialog.setContentView(view);

        title = view.findViewById(R.id.title);
        image = view.findViewById(R.id.image);
        closeButton = view.findViewById(R.id.closeButton);
        okButton = view.findViewById(R.id.okButton);



        title.setText("Тайлбар оруулах");

        descEditText = view.findViewById(R.id.descEditText);

        descEditText.setText("");

        Log.e("position",index+"");


        if (issues.get(index).getDesc()!=null && !issues.get(index).getDesc().isEmpty()){

            Log.e("issue",issues.get(index).getDesc());
            descEditText.setText(issues.get(index).getDesc());

        }else {
            Log.e("nodesc","desc");
        }

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDescDialogFragment.this.dismiss();
            }
        });


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String desc = descEditText.getText().toString();

                issues.get(index).setDesc(desc);

                onAddIssueDescListener.onAddDescIssue(issues);

                SelectDescDialogFragment.this.dismiss();

            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);
        getActivity().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


    }


}
