package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import androidx.fragment.app.Fragment;
import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AlertDialogFragment extends DialogFragment {

    TextView title;
    TextView desc;

    FrameLayout closeButton;

    String descValue;
    String titleValue;
    int alertType;

    LottieAnimationView image;

    public static int ALERT_TYPE_SUCCESS = 1;
    public static int ALERT_TYPE_FAIL = 0;

    @SuppressLint("ValidFragment")
    public AlertDialogFragment(String titleValue, String descValue, int alertType) {
        // Required empty public constructor
        this.titleValue = titleValue;
        this.descValue = descValue;
        this.alertType = alertType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_alert_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        title  = view.findViewById(R.id.title);
        desc  = view.findViewById(R.id.desc);
        image = view.findViewById(R.id.image);
        closeButton = view.findViewById(R.id.closeButton);

        title.setText(titleValue);
        desc.setText(descValue);


        if (alertType == ALERT_TYPE_FAIL){
            image.setAnimation("lottie_failed.json");
        }else if(alertType == ALERT_TYPE_SUCCESS){
            image.setAnimation("lottie_success.json");
        }

        dialog.setContentView(view);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogFragment.this.dismiss();
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);


    }


}
