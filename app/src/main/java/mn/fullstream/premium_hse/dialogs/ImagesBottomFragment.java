package mn.fullstream.premium_hse.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import carbon.widget.FrameLayout;
import me.relex.circleindicator.CircleIndicator;
import me.relex.circleindicator.Config;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.dialogs.fragments.ImageFragment;


@SuppressLint("ValidFragment")
public class ImagesBottomFragment extends BottomSheetDialogFragment {

	FrameLayout closeButton;

	CircleIndicator indicator;
	ViewPager viewPager;

	int indicatorWidth = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
			Resources.getSystem().getDisplayMetrics()) + 0.5f);
	int indicatorHeight = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4,
			Resources.getSystem().getDisplayMetrics()) + 0.5f);
	int indicatorMargin = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,
			Resources.getSystem().getDisplayMetrics()) + 0.5f);


	ArrayList<String> imgs;
	int position;

	@SuppressLint("ValidFragment")
	public ImagesBottomFragment(int position, ArrayList<String> imgs) {

		this.imgs = imgs;
		this.position = position;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return super.onCreateDialog(savedInstanceState);
	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
							 @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_images_bottom, parent, false);
		setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme);

		closeButton = view.findViewById(R.id.closeButton);
		indicator = view.findViewById(R.id.indicator);
		viewPager = view.findViewById(R.id.viewPager);


		SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), imgs);
		viewPager.setAdapter(sectionsPagerAdapter);



		Config config = new Config.Builder().width(indicatorWidth)
				.height(indicatorHeight)
				.margin(indicatorMargin)
				.drawable(R.drawable.select_indicator)
				.build();

		indicator.initialize(config);

		indicator.setViewPager(viewPager);

		viewPager.setCurrentItem(position);

		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ImagesBottomFragment.this.dismiss();
			}
		});

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

		ArrayList<String> imgs;

		public SectionsPagerAdapter(FragmentManager fm, ArrayList<String> imgs) {
			super(fm);
			this.imgs = imgs;
		}

		@Override
		public Fragment getItem(int position) {

			Fragment fragment = null;

			String img = imgs.get(position);

			fragment = new ImageFragment(img);

			return fragment;
		}

		@Override
		public int getCount() {
			return imgs.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "";
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {

		}
	}

}
