package mn.fullstream.premium_hse.dialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import carbon.widget.FrameLayout;
import mn.fullstream.premium_hse.R;
import mn.fullstream.premium_hse.interfaces.OnSelectImageTypeListener;
import mn.fullstream.premium_hse.main.task.InspectActivity;
import mn.fullstream.premium_hse.model.InspectAttribue;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SelectImageDialogFragment extends DialogFragment {

    TextView title;

    FrameLayout closeButton;
    FrameLayout fromGalleryButton,fromCameraButton;

    String titleValue;
    int alertType;

    LottieAnimationView image;

    OnSelectImageTypeListener onSelectImageTypeListener;

	InspectActivity.RecyclerViewAdapter recyclerViewAdapter;
	String attributeId;

	RecyclerView recyclerView;



    @SuppressLint("ValidFragment")
    public SelectImageDialogFragment(OnSelectImageTypeListener onSelectImageTypeListener, InspectActivity.RecyclerViewAdapter recyclerViewAdapter,RecyclerView recyclerView,String attributeId) {
        // Required empty public constructor
        this.onSelectImageTypeListener = onSelectImageTypeListener;
        this.attributeId = attributeId;
        this.recyclerViewAdapter = recyclerViewAdapter;
        this.recyclerView = recyclerView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_select_type_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        title  = view.findViewById(R.id.title);
        image = view.findViewById(R.id.image);
        closeButton = view.findViewById(R.id.closeButton);
        fromGalleryButton = view.findViewById(R.id.fromGalleryButton);
        fromCameraButton = view.findViewById(R.id.fromCameraButton);



        title.setText("Зѳрчлийн зураг оруулах");


        fromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

				InspectAttribue inspectAttribue = new InspectAttribue();
				inspectAttribue.setValue("");
				inspectAttribue.setAttribueId(attributeId);

                onSelectImageTypeListener.onSelectImageType(inspectAttribue,true,recyclerViewAdapter,recyclerView);
                SelectImageDialogFragment.this.dismiss();
            }
        });


        fromCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

				InspectAttribue inspectAttribue = new InspectAttribue();
				inspectAttribue.setValue("");
				inspectAttribue.setAttribueId(attributeId);

                onSelectImageTypeListener.onSelectImageType(inspectAttribue,false,recyclerViewAdapter,recyclerView);
                SelectImageDialogFragment.this.dismiss();


            }
        });



        dialog.setContentView(view);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImageDialogFragment.this.dismiss();
            }
        });

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().getDecorView().setBackgroundResource(R.color.colorTrans);


    }


}
