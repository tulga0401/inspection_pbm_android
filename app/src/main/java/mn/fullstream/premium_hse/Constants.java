package mn.fullstream.premium_hse;

public class Constants {

    public static final String INTENT_TRUCK_DRIVER = "INTENT_TRUCK_DRIVER";
    public static final String INTENT_ORDER_ID = "INTENT_ORDER_ID";
    public static final String API_HSE_ISSUES = "api/hse/issue/list";
    public static final String INTENT_ORDER_ADVICE = "INTENT_ORDER_ADVICE";
    public static final String INTENT_ORDER_APPROVEDAT = "INTENT_ORDER_APPROVEDAT";
    public static final String INTENT_ORDER_CANCALEDAT = "INTENT_ORDER_CANCALEDAT";
	public static final String INTENT_UNINSPECTION = "INTENT_UNINSPECTION";
    public static final String INTENT_WORLEY_MENU = "INTENT_WORLEY_MENU";
    public static final String INTENT_PUSH_NOTIFICATION = "INTENT_PUSH_NOTIFICATION";
	public static final String INTENT_PARAM_INSPECT_ID = "INTENT_PARAM_INSPECT_ID";
	public static final String INTENT_PARAM_INSPECT_NAME = "INTENT_PARAM_INSPECT_NAME";
	public static final String INTENT_PARAM_INSPECT_CODE = "INTENT_PARAM_INSPECT_CODE";
    public static String SHARED_PREFERENCE_IS_LOGIN = "SHARED_PREFERENCE_IS_LOGIN";
    public static String INTENT_ORDER_RESULT_DESC = "INTENT_ORDER_RESULT_DESC";
    public static String INTENT_ORDER_RESULT_TITLE = "INTENT_ORDER_RESULT_TITLE";
    public static String INTENT_ORDER_RESULT = "INTENT_ORDER_RESULT";
    public static String INTENT_TEMPLATE_ORDER = "INTENT_TEMPLATE_ORDER";


	public static final String INTENT_GROUP_INSPECTION_CODE = "INTENT_GROUP_INSPECTION_CODE";
	public static final String INTENT_GROUP_INSPECTION_NAME = "INTENT_GROUP_INSPECTION_NAME";
	public static final String INTENT_GROUP_INSPECTION_GENERATED_NAME = "INTENT_GROUP_INSPECTION_GENERATED_NAME";
	public static final String INTENT_GROUP_INSPECTION_BEGINDATE = "INTENT_GROUP_INSPECTION_BEGINDATE";
	public static final String INTENT_GROUP_INSPECTION_ENDDATE = "INTENT_GROUP_INSPECTION_ENDDATE";
	public static final String INTENT_GROUP_INSPECTION_GROUPID = "INTENT_GROUP_INSPECTION_GROUPID";

    //	API
//    public static String BASE_API = "http://test.centrals.mn/";
    public static String BASE_API = "http://test2.centrals.mn/";
//    public static String BASE_API = "http://192.168.0.116/";
	public static String BASE_CENTRALS_API = "http://test2.centrals.mn";
    public static String USER_INFO_API = BASE_API + "api/login";

	public static String API_LOGOUT = BASE_API+"/security/logout/{userId}";
    public static String USER_INFO_MAIN_REPORT = BASE_API + "inspection/api/report";
    public static String USER_UNINSPECTED_TEMPLATE = BASE_API + "inspection/api/inspection/templates/";
    public static String USER_INSPECTED_TEMPLATE = BASE_API + "inspection/api/templates/inspected/";
    public static String USER_UNINSPECTED_DETAIL = BASE_API + "inspection/api/";


    public static String USER_INSPECTED_API = BASE_API + "inspection/api/inspections/{beginDate}/{endDate}/inspected/{code}";
    public static String USER_UNINSPECTED_API_2 = BASE_API + "/inspection/api/inspection/index/{beginDate}/{endDate}/{code}?_group_id={groupId}";
    public static String USER_SEARCH_INSPECSTION_API = BASE_API + "inspection/api/order/{orderNumber}/inspections";
    public static String TEMPLATI_API = BASE_API + "inspection/api/";
    public static String API_NOTIFICATIONS = BASE_API + "inspection/api/notification/{userId}/page/{page}";
    public static String API_UNSEEN_NOTIFICATIONS = BASE_API + "inspection/api/notification/count/{userId}";
    public static String USER_INSPECT_API = BASE_API + "inspection/api/inspect";
    public static String USER_INFO = BASE_API + "api/cms_users";
    public static String ORDERS_API_LIST = BASE_API + "inspection/api/hse/orders/list";
    public static String ORDERS_API_HSE_ISSUE = BASE_API + "inspection/api/hse/orders/{id}/issue-list";

    public static String APPROVE_ORDER_API = BASE_API + "api/hse/orders/{id}/approve";
    public static final String INTENT_ORDER_HISTORY_DETAIL = "INTENT_ORDER_HISTORY_DETAIL";
    public static final String REGISTER_DEVICE_API = BASE_API + "papi/user/register-device";
    
}
