package mn.fullstream.premium_hse;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;

import io.realm.Realm;
import mn.fullstream.premium_hse.login.LoginActivity;
import mn.fullstream.premium_hse.model.User;
import mn.fullstream.premium_hse.worley.WorleyActivity;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (MyApplication.checkLocale(getBaseContext()).equals("") || MyApplication.checkLocale(getBaseContext()).equals("mn")) {
            MyApplication.setLocale("mn", getBaseContext());
        }


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);

                Intent intent;
                Realm realm = Realm.getInstance(MyApplication.realmConfig);

                User user = realm.where(User.class).findFirst();

                if (user!=null) {

                	if(user.getIsWorley()!=null && user.getIsWorley().equals("true")) {
						intent = new Intent(SplashActivity.this, WorleyActivity.class);
					}else {
						intent = new Intent(SplashActivity.this, MainActivity.class);
					}
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }

                startActivity(intent);
                SplashActivity.this.finish();

            }
        }, SPLASH_TIME_OUT);

    }
}
