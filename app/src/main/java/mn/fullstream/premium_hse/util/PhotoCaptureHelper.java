package mn.fullstream.premium_hse.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;


public class PhotoCaptureHelper {
  private String currentPhotoPath;
  private String imageFileName;
  private String timeStamp;
  private File imageFile;
  private Context context;

  public PhotoCaptureHelper(Context context) {
    this.context = context;
  }

  public void dispatchTakePictureIntent(final int position) {
    String[] permissions = { Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO };
    Permissions.check(context, permissions, null, null, new PermissionHandler() {
      @Override public void onGranted() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
          File photoFile = null;
          try {
            photoFile = createImageFile();
          } catch (IOException ex) {
            ex.printStackTrace();
          }
          if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(context, "mn.iotech.inspection.fileprovider",
                photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            ((Activity) context).startActivityForResult(takePictureIntent, position);
          }
        }
      }
    });
  }

  @SuppressLint("SimpleDateFormat") private File createImageFile() throws IOException {
    File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date());
    imageFileName = "Photo_" + timeStamp + "_";
    imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
    currentPhotoPath = imageFile.getAbsolutePath();
    return imageFile;
  }

  Bitmap addWatermark(Bitmap src) {
    int textSize = 70;

    String line1 = "МОНГОЛ ДАХЬ";
    String line2 = "АЛБАН ЁСНЫ НЭЭЛТ";

    int w = src.getWidth();
    int h = src.getHeight();

    Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
    Canvas canvas = new Canvas(result);
    canvas.drawBitmap(src, 0, 0, null);
    Rect line1bound = new Rect();
    Rect line2bound = new Rect();

    Paint paint = new Paint();
    paint.setColor(Color.WHITE);
    paint.setFakeBoldText(true);
    paint.setTextSize(textSize);
    paint.setAntiAlias(true);

    paint.getTextBounds(line1, 0, line1.length(), line1bound);
    paint.getTextBounds(line2, 0, line2.length(), line2bound);

    int text1Width = line1bound.width();
    int text2Width = line2bound.width();
    int text1Height = line1bound.height();
    int text2Height = line2bound.height();

    int x = w - text1Width - 50;
    int x2 = w - text2Width - 50;
    int y = h - (text1Height + text2Height + 80);
    int y2 = h - text2Height;

    canvas.drawText(line1, x, y, paint);
    canvas.drawText(line2, x2, y + 80, paint);

    return result;
  }

  public String getCurrentPhotoPath() {
    return currentPhotoPath;
  }

  String getPhotoName() {
    return imageFileName;
  }

  String getTimeStamp() {
    return timeStamp;
  }

  File getImageFile() {
    return imageFile;
  }
}
