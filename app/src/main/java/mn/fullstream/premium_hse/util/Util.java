package mn.fullstream.premium_hse.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {

	public static String getDateFromUnix(String timeStamp) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);

			Date result = new Date(Long.parseLong(timeStamp));

			return dateFormat.format(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String getTimeFromUnix(String timeStamp) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);

		Date result = new Date(Long.parseLong(timeStamp));

		return dateFormat.format(result);
	}

	//    barcode
	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0x00000000;

	private static String guessAppropriateEncoding(CharSequence contents) {
		// Very crude at the moment
		for (int i = 0; i < contents.length(); i++) {
			if (contents.charAt(i) > 0xFF) {
				return "UTF-8";
			}
		}
		return null;
	}

	public static Drawable getDrawable(String name, Context context) {

		try {
			int resourceId =
					context.getResources().getIdentifier(name, "mipmap", context.getPackageName());
			return context.getResources().getDrawable(resourceId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo =
					context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}


	public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);

		// "RECREATE" THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(
				bm, 0, 0, width, height, matrix, false);
		bm.recycle();
		return resizedBitmap;
	}

}
