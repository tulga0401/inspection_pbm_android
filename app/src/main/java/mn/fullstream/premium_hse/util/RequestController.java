package mn.fullstream.premium_hse.util;

import android.graphics.Bitmap;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import mn.fullstream.premium_hse.Constants;
import mn.fullstream.premium_hse.MyApplication;
import mn.fullstream.premium_hse.model.Config;
import mn.fullstream.premium_hse.model.HseImage;
import mn.fullstream.premium_hse.model.Issue;
import mn.fullstream.premium_hse.model.User;


public class RequestController {

    public String loginHttpRequest(String language, String userName, String password) {

        String url = Constants.USER_INFO_API;


        Log.e("url", url);

        try {


            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse response;
            String responseString;

            HttpPost httpPost = new HttpPost(url);

//            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("username", userName));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            try {
                Realm realm = Realm.getInstance(MyApplication.realmConfig);
                Config config = realm.where(Config.class).findFirst();
                nameValuePairs.add(new BasicNameValuePair("deviceToken", config.getFcmToken()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


            Log.e("nameValuePairs", nameValuePairs.toString());

            response = httpclient.execute(httpPost);

            StatusLine statusLine = response.getStatusLine();

            Log.e("status", statusLine.getReasonPhrase());
            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                Log.e("responseString", responseString + " s");

                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    String data = jsonObject.toString();

                    out.close();

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //Closes the connection.
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();

                return responseString;
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }

        return null;
    }

    public String postCancelHttpRequest(String url, ArrayList<HseImage> hseImages, ArrayList<Issue> issues, String advice) {
        try {

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();


            HttpPost httpPost = new HttpPost(url);

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            Log.e("url", url);

            String responseString = null;


            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();


            httpPost.addHeader("X-AUTH-TOKEN", user.getToken());
            httpPost.addHeader("App", "HSE");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


            for (int i = 0; i < hseImages.size(); i++) {

                try {

                    HseImage labImage = hseImages.get(i);

                    Bitmap imageBitmap = labImage.getPhoto();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    byte[] data = bos.toByteArray();


                    ByteArrayBody bab = new ByteArrayBody(data, labImage.getFileName());
                    multipartEntity.addPart("images[]", bab);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            for (int i = 0; i < issues.size(); i++) {


                try {
                    String issueId = issues.get(i).getId();

                    String desc = issues.get(i).getDesc() == null ? "" : issues.get(i).getDesc();

                    multipartEntity.addTextBody("issues[" + i + "]", issueId);
                    multipartEntity.addTextBody("descs[" + i + "]", desc);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            multipartEntity.addTextBody("advice", advice, ContentType.create("text/plain", MIME.UTF8_CHARSET));


            httpPost.setEntity(multipartEntity.build());

            response = httpclient.execute(httpPost);


            StatusLine statusLine = response.getStatusLine();


            Log.e(".getStatusCode()", statusLine.getStatusCode() + "");

            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObj = new JSONObject(responseString);
                    String data = jsonObj.toString();

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.


                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String postApproveHttpRequest(String url, ArrayList<HseImage> hseImages, ArrayList<Issue> issues, String advice) {
        try {

            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();

            HttpPost httpPost = new HttpPost(url);

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            Log.e("url", url);

            String responseString = null;


            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();


            httpPost.addHeader("X-AUTH-TOKEN", user.getToken());

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


            for (int i = 0; i < hseImages.size(); i++) {

                try {

                    HseImage labImage = hseImages.get(i);

                    Bitmap imageBitmap = labImage.getPhoto();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    byte[] data = bos.toByteArray();


                    ByteArrayBody bab = new ByteArrayBody(data, labImage.getFileName());
                    multipartEntity.addPart("images[]", bab);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            for (int i = 0; i < issues.size(); i++) {


                try {
                    String issueId = issues.get(i).getId();
                    multipartEntity.addTextBody("issues[" + i + "]", issueId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            multipartEntity.addTextBody("advice", advice, ContentType.create("text/plain", MIME.UTF8_CHARSET));


            httpPost.setEntity(multipartEntity.build());

            response = httpclient.execute(httpPost);


            StatusLine statusLine = response.getStatusLine();


            Log.e(".getStatusCode()", statusLine.getStatusCode() + "");

            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObj = new JSONObject(responseString);
                    String data = jsonObj.toString();

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.


                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String getHttpRequest(String url) {
        try {

            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();

            HttpGet httpGet = new HttpGet(url);

            Log.e("url", url);

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            String responseString = null;


            httpGet.addHeader("X-AUTH-TOKEN", user.getToken());
            httpGet.setHeader("Accept", "application/json");
            httpGet.addHeader("Content-Type", "application/json");

            response = httpclient.execute(httpGet);


            Log.e("url", url);

            StatusLine statusLine = response.getStatusLine();


            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString().replace("string(7) \"success\"", "");

                Log.e("responseString", responseString);

                try {

                    return responseString;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String postHttpRequest(String url) {
        try {


            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();


            HttpPost httpPost = new HttpPost(url);

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            String responseString = null;


            Log.e("url", url + "");
            httpPost.addHeader("X-AUTH-TOKEN", user.getToken());


            response = httpclient.execute(httpPost);


            StatusLine statusLine = response.getStatusLine();


            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObj = new JSONObject(responseString);
                    String data = jsonObj.toString();

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String postHttpJsonParamRequest(String url, JSONObject paramJsonObject) {
        try {

            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();

            HttpPost httpPost = new HttpPost(url);


            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            String responseString = null;


            Log.e("url", url + "");
            Log.e("X-AUTH-TOKEN", user.getToken() + "");
            Log.e("paramJsonObject", paramJsonObject.toString() + "");
            httpPost.addHeader("X-AUTH-TOKEN", user.getToken());
            httpPost.setEntity(new StringEntity(paramJsonObject.toString(), HTTP.UTF_8));

            response = httpclient.execute(httpPost);


            StatusLine statusLine = response.getStatusLine();


            if (statusLine.getStatusCode() == HttpStatus.SC_OK || statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObj = new JSONObject(responseString);
                    String data = jsonObj.toString();

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String getHttpWeatherRequest(String url) {
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;

            String responseString = null;


            response = httpclient.execute(new HttpGet(url));
            Log.e("url", url);

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObj = new JSONObject(responseString);
                    String data = jsonObj.toString();


                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();
        }
        return null;
    }

    public String postRegisterHttpRequest(String deviceToken, String deviceName, String deviceVersion, String appVersion) {
        String url = Constants.REGISTER_DEVICE_API;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString;

            HttpPost httpPost = new HttpPost(url);
            Realm realm = Realm.getInstance(MyApplication.realmConfig);

            User user = realm.where(User.class).findFirst();
            if (user != null) {
                httpPost.addHeader("X-AUTH-TOKEN", user.getToken());
            }

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("deviceId", ""));
            nameValuePairs.add(new BasicNameValuePair("deviceToken", deviceToken));
            nameValuePairs.add(new BasicNameValuePair("deviceName", deviceName));
            nameValuePairs.add(new BasicNameValuePair("deviceModel", deviceName));
            nameValuePairs.add(new BasicNameValuePair("osVersion", deviceVersion));
            nameValuePairs.add(new BasicNameValuePair("os", "android"));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            response = httpclient.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();

                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    String data = jsonObject.toString();

                    Log.e("response", data);

                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.close();
            } else {
                //Closes the connection.
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();

                return responseString;
            }
        } catch (Exception e) {
            //TODO Handle problems..
            e.printStackTrace();

//            Toast.makeText(context,context.getResources().getString(R.string.check_internet),Toast.LENGTH_SHORT).show();

        }

        return null;
    }
}
